# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 13:36:55 2016

@author: holzbecher
"""

# =============================================================================
# This script contains some of the plotting functions
# and some analysis functions for the plots.
# =============================================================================


#%% import...
import matplotlib
import SimulationWrapperPypetPub as sw
import gapAnalyticsTempPub as gt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from itertools import product
from matplotlib import patheffects
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import os
from scipy.ndimage.filters import gaussian_filter
from math import ceil, isnan
import ColorMapsAndAxisTitleParasPub as cmt
import matplotlib.gridspec as gridspec
from scipy.signal import spectrogram

# do the 2d plots of the manuscript 

def twoDplotFig2(df, datalabelZ, datalabelX, plotNumbers = 0, cLimits = [], figLetter = '', interploation = 'nearest', identifier = '', saveDir = '', linColorCode = False, ax = False):

    
#    plt.rcParams.update({'font.size': 16})
    # different codes denote different axes labels and colors
    if datalabelZ == 2:
        colorBarlabel = 'Network frequency (Hz)'
        title = 'Network frequency (Hz)'
        colorCode = cmt.cmapNetworkFrequency
    elif datalabelZ == 3:
        colorBarlabel = 'Neuron firing rate (1/s)'
        title = 'Neuron firing rate (1/s)'
        colorCode = cmt.cmapFiringRate
    elif datalabelZ == 1:
        colorBarlabel = 'Synchrony index'
        title = 'Synchrony index'
        colorCode = cmt.cmapSynchrony #'Greens'
    elif datalabelZ == 4:
        colorBarlabel = 'Quality Factor ' + identifier
        title = colorBarlabel
        colorCode = 'viridis'        
    elif datalabelZ == 5:
        colorBarlabel = 'Saturation'
        title = colorBarlabel
        colorCode = 'Greys'
    else:
        colorBarlabel = 'I really dont know'

    if datalabelX == 1:
        xlabel = 'Passive conductance (nS)'
        ylabel = 'Active spike (mV)'
    elif datalabelX == 2:
        xlabel = 'Connection probabilty'
        ylabel = 'Cluster size (#neurons)'
    elif datalabelX == 3:
        xlabel = 'Sigma pa. cond. (gammma0)'
        ylabel = 'Sigma of act. spike (beta0)'
    elif datalabelX == 4:
        xlabel = 'Connection probabilty'
        ylabel = 'Possion Drive (APs/s)'
    elif datalabelX == 5:
        xlabel = 'Delay (ms)'
        ylabel = 'Connection probability'
    else:
        xlabel = 'WAAAAAAAA: enter x label here'
        ylabel = 'enter y label here'

    #df = GJParameterMap.res.summary.syncIndexCorr.syncIndexCorr_frame
    dfFloat = df.astype(float)

    m = len(df.columns)
    n = len(df.index)
    if linColorCode: colorCode = 'viridis'
    
#    if ax == False:
#        fig = plt.figure(figsize = (7,7))
#        ax = fig.add_subplot(1, 1, 1)
#    else:
#    fig = ax.get_figure()
        

    minData = min(df.min())
    maxData = max(df.max())

    diff = maxData - minData

    if not cLimits:
        lowTick = float(gt.magicRound(minData, diff, 0))
        highTick = float(gt.magicRound(maxData, diff, 1))

    else:
        [lowTick, highTick] = cLimits
#    extent = [df.columns.min(), df.columns.max(), df.index.min(), df.index.max()]
    plot = ax.imshow(dfFloat, origin = 'lower', interpolation = 'nearest', cmap = colorCode,
                     filterrad = 10.0, vmin = lowTick, vmax = highTick)#, aspect = 3)#, extent = extent)

    yTicks = np.linspace(0, n-1, n)[::3]
    yLabels = df.index.tolist()[::3]

    xTicks = np.linspace(0, m-1, m)[::3]
    xLabels = df.columns.tolist()[::3]

    _ = ax.set_xticks(xTicks)
    _ = ax.set_xticklabels(xLabels)
    _ = ax.set_yticks(yTicks)
    _ = ax.set_yticklabels(yLabels)


    _ = ax.yaxis.set_ticks_position('left')
    _ = ax.xaxis.set_ticks_position('bottom')


    if figLetter:
        ax.annotate(figLetter,(0.0,0.9), textcoords='figure fraction', fontsize = 25)

    divider = make_axes_locatable(ax)
    
    if datalabelX == 5:
        cax = divider.append_axes("top", size="8%", pad=0.07)
        cb = plt.colorbar(plot, cax=cax, ticks=[lowTick,highTick],ticklocation = 'top', orientation = 'horizontal' )
        cax.set_title(title)
    else:
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cb = plt.colorbar(plot, cax=cax, ticks=[lowTick,highTick])
        ax.set_title(title)
#    plt.clim(lowTick,highTick)
#    cb.set_label(colorBarlabel)
    #path_effects = [patheffects.withSimplePatchShadow(shadow_rgbFace=(1,1,1))]

    

    if plotNumbers:
        for i, j in product(range(m), range(n)):
            _ = ax.text(j, i, '{0:.2f}'.format(df.iloc[i, j]),
                        size='medium', ha='center', va='center')

# plot the network activity 
            
def rasterPlotSingle(spikes, simtime, number_neuron = 199.0, ccode = gt.colNetFreq, markersize = 0.1, showTime = 0, ax = False, rasterized = False):


    N1 = np.where(spikes == number_neuron) #gives the number spike count

    if len(N1[0]) == 0 : return 'Sorry, we do not have so many neurons'
    else: N = N1[0][-1] + 1

    x = np.array([0.0]*N)
    y = np.array([0.0]*N)

    for each in range(N): #extremly costly way to generate the wanted spike train
        x[each] = spikes[each][0]*1000
        y[each] = spikes[each][1]
        each+=1

    #Scatter plot for the spikes
    #plt.scatter(x,y, marker='|', c = colNetFreq)
    scatterPlot = ax.scatter(x,y, marker = '.', s = markersize, facecolor = ccode, lw = 0)
    if rasterized: scatterPlot.set_rasterized(True)
    ax.set_xlim([1000*showTime, 1000*simtime])
    ax.set_ylim([(spikes[0][1] - 1), spikes[-1][1] + 1])

    ax.locator_params(axis = 'x', nbins = 3)



#    gt.remove_box_cueAll(ax1, keepx = True)
    #plt.plot(np.array(range(len(output.smooth_rate(sigma_krnl=0.0))))/100.0,output.smooth_rate(sigma_krnl=0.0))

    ax.set_xlim([showTime*1000, simtime*1000])

# calculates the oscillation strength
def getOscillationStrengthFirstPos(frqaux, yaux, windowSize = 5):

    interpolationRangeFreq = 20
    yaux = gaussian_filter(yaux, windowSize)


    zero_crossings = np.where(np.diff(np.sign(np.gradient(yaux))))[0]
    
    if len(zero_crossings) != 0:
        
        print zero_crossings
        
        freqIdx = zero_crossings.min() + 1
        if ((frqaux[freqIdx] < 500) & (len(zero_crossings) == 1)):
            lowerBorderMax = frqaux[freqIdx]
            yaux = yaux[frqaux > lowerBorderMax]
            frqaux = frqaux[frqaux > lowerBorderMax]
            
            
            
        elif frqaux[freqIdx] < 500:
            
            lowerBorderMax = frqaux[freqIdx]
            freqStep = frqaux[1] - frqaux[0]
            chooseMinFromThis = zero_crossings[zero_crossings > (zero_crossings[0] + interpolationRangeFreq/freqStep)]
            if len(chooseMinFromThis) != 0:
                
                upperBorderMax = frqaux[(chooseMinFromThis.min() + 1)]
            else:
                upperBorderMax = frqaux[-1]
                
            yaux = yaux[frqaux > lowerBorderMax]            
            frqaux = frqaux[frqaux > lowerBorderMax]
            
            yaux = yaux[frqaux < upperBorderMax]
            frqaux = frqaux[frqaux < upperBorderMax]
            
            

        else: return 0, 0
    else: return 0, 0


    interpolationRange = ceil(interpolationRangeFreq/(frqaux[1]-frqaux[0]))
    
    maxIndex = yaux.argmax()
    
    #print maxIndex, interpolationRange

    if (maxIndex > 0) & (maxIndex > interpolationRange):

        xRange = frqaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]
        yRange = yaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]

        #print xRange, yRange

        #x = gt.moving_average(xRange, windowSize)
        #y = gt.moving_average(yRange, windowSize)
        x = xRange

#        if windowSize != 0:
#            y = gaussian_filter(yRange, windowSize)
#        else:
        y = yRange

        #print y
        halfPowerValue = y.max()/2.0

#        print halfPowerValue
        maxIndexNew = y.argmax()

        if maxIndexNew > 0:

            halfPowerIndexLow = (abs(y[:maxIndexNew] - halfPowerValue)).argmin()
            halfPowerIndexUp = (abs(y[maxIndexNew:] - halfPowerValue)).argmin()
            HalfPowerFLow = x[:maxIndexNew][halfPowerIndexLow]
            HalfPowerFUp = x[maxIndexNew:][halfPowerIndexUp]
            FWHM = HalfPowerFUp - HalfPowerFLow

            oscillationStrength = y.max()*FWHM

            return oscillationStrength, x[y.argmax()]

        else:
            print "New max idx found at 0"
#            fig, ax = plt.subplots(1)
#            ax.plot(frqauxOld, yauxOld)
#            ax.plot(frqauxOld[zero_crossings], yauxOld[zero_crossings], 'o')
#            ax.plot(frqaux, yaux)
#            ax.plot(frqaux[maxIndex], yaux[maxIndex], 'o')
#            ax.plot(x, y)
#            ax.plot(x[maxIndexNew], y[maxIndexNew], 'o')
##            plt.show()
            return 0, 0
    else:
#        fig, ax = plt.subplots(1)
#        ax.plot(frqauxOld, yauxOld)
#        ax.plot(frqauxOld[zero_crossings], yauxOld[zero_crossings], 'o')
#        ax.plot(frqaux, yaux)
#        ax.plot(frqaux[maxIndex], yaux[maxIndex], 'o')
##        plt.show()
        print "Max idx found at 0"
        return 0, 0



#
#
# Oscillatin onset is defined as being above the threshold twice in a row
# This functions tells at which data point this is the case.
        
def findOscillationOnset(oscillationData):
#==============================================================================
#   define oscillation onset as two times a not NaN entry
#==============================================================================

    for excitedNeurons in range(len(oscillationData.index)):

        if not isnan(oscillationData.iloc[excitedNeurons]) and not isnan(oscillationData.iloc[excitedNeurons + 1]):
            return excitedNeurons
    return 1000

# This function generated Fig 2 of the paper
    
def rasterPlotSWR(output, simtime, number_neuron = 199.0,
                  sync = 1, figName = 'Test',
                  markersize = 0.1, returnFig = True,
                  showTime = 0, bins = 500, title = '',
                  spectrumFreqs = [], spectrumValues = [],
                  maxDict = {}, minMaxFreq = [50, 500],
                  leadingFreq = 0, 
                  averageFiring = 0):
    print maxDict
    spikes = output.spiketimes
    plt.rcParams.update({'font.size': 7})
   #    if len(N1[0]) == 0 : return 'Sorry, we do not have so many neurons'
#    else: N = N1[0][-1]


#    N = N1[0][-1]
    
    NInt = len(spikes)
    
    x = np.array([0.0]*NInt)
    y = np.array([0.0]*NInt)
    each = 0;
    for each in range(NInt): #extremly costly way to generate the wanted spike train
        x[each] = spikes[each][0]*1000
        y[each] = spikes[each][1] * 5.0
        each+=1

    fig = plt.figure(facecolor='white', figsize= cmt.halfhalfPageFigSize)
    
    #make outer gridspec

    
    gs1 = gridspec.GridSpec(2, 1, height_ratios = [5, 1], hspace = .25) 
    #make nested gridspecs
    gs11 = gridspec.GridSpecFromSubplotSpec(4, 1, subplot_spec = gs1[0], hspace = .1)
    #gs12 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = gs1[1])
    
    
    excitatory = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = gs11[0], hspace = .0)
    inhibitory = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = gs11[1], hspace = .0)
    
            
    gsList =[excitatory[1], inhibitory[0], inhibitory[1],
             gs11[2]]
    
    ax = []
        
    ax.append(fig.add_subplot(excitatory[0]))
    
    for i, gs in enumerate(gsList):    
        ax.append(fig.add_subplot(gs, sharex = ax[0]))
    for gs in [gs11[3], gs1[1]]:
        ax.append(fig.add_subplot(gs))


    #Scatter plot for the spikes

    scatterMarker = 'o'
    ax[2].scatter(x,y, marker = scatterMarker, s = markersize, c = cmt.inhibitionColor)
    ax[2].set_ylim([-50.0, (spikes[-1][1] + 4) * 5.0])
    ax[2].locator_params(axis = 'x', nbins = 3)
 

    
    # Scale by all interneurons to get the single interneuron firing rate
    timesFR, networkActivityInt = gt.returnTimeResolvedFiringRatesInMsWindows(spikes, 0.3, scaleFactor = 1/200.0, timeFactor = 10000)
    networkActivity = gt.smooth_rate(spikes, sigma_krnl=0, simTime = 0.3)
#    times = np.array(range(len(networkActivity)))/100.0

#    ax[3].plot(output.times_rate * 1000.0, gaussian_filter(output.rate_inh, 1.0), c = cmt.inhibitionColor)
    ax[3].plot(1000.0 * timesFR, gaussian_filter(networkActivityInt, 10.0), c = cmt.inhibitionColor)
    ax[3].set_ylabel(cmt.labelFiringRateShort)
    ax[3].set_xlabel('Time (ms)')
    
    if maxDict.has_key('inhibitionActivity'):
        ax[3].set_ylim(maxDict['inhibitionActivity'])

    else:
        maxDict['inhibitionActivity'] = ax[3].get_ylim()
        



    freq, times, Sxx = spectrogram(networkActivity.flatten(), fs = 100000, noverlap = 4096-50, nperseg = 4096)
    
    Sxx = Sxx[(freq > minMaxFreq[0]) & (freq < minMaxFreq[1]), :]
    freq = freq[(freq > minMaxFreq[0]) & (freq < minMaxFreq[1])]

    print Sxx.max()
    
    times = times * 1000.0 - 150.0    
    if maxDict.has_key('SxxMax'):
        Sxx = Sxx/maxDict['SxxMax']
        cax = ax[5].imshow(Sxx, extent=[times.min(), times.max(), freq.min(), freq.max()], vmin = 0.0, vmax = 1.0,
                interpolation='gaussian', origin='lower', aspect = 'auto', filterrad = 2.0)
    else:
        maxDict['SxxMax'] = Sxx.max() 
        Sxx = Sxx/maxDict['SxxMax']
        cax = ax[5].imshow(Sxx, extent=[times.min(), times.max(), freq.min(), freq.max()], vmin = 0.0,
                interpolation='gaussian', origin='lower', aspect = 'auto', filterrad = 2.0)
    
    
#    SxxSmooth = gaussian_filter(Sxx, [1.0, 2])
#    ax[6].pcolormesh(times * 1000.0, freq, Sxx)

#    ax[6].pcolormesh(times, freq, SxxSmooth)

#    ax[6] = fig.add_subplot(gs11[4])



    

    ax[5].set_xlabel('Time (ms)')
    ax[5].set_ylabel('Frequency (Hz)')
    maxScale = round(np.floor(100.0 * Sxx.max())/100.0, 2)
    
    # Same scale for both
   # cb = plt.colorbar(cax, ax = ax[5], aspect = 10, fraction = 0.1, ticks=[0.0, maxScale])
   
    cb = plt.colorbar(cax, ax = ax[5], aspect = 10, fraction = 0.1, ticks=[0, 1])
    cb.set_label('Power (a.u.)')
#    ax[6].axis([times.min(), times.max(), freq.min(), freq.max()])

#    if maxDict.has_key('spectrogram'):
#        ax[6].get_images()[0].set_clim(maxDict['spectrogram'])
#
#    else:
#        maxDict['spectrogram'] = ax[6].get_images()[0].get_clim()    

    
#    gaussianWidth = 5
    ax[6].plot(spectrumFreqs, gaussian_filter(spectrumValues, 3.0), label = 'Raw')
#    ax[6].plot(spectrumFreqs,gaussian_filter(spectrumValues,gaussianWidth), label = 'Filtered ')
    #plt.rcParams.update({'font.size': 22})
    ax[6].set_xlabel('Frequency (Hz)')
    ax[6].set_ylabel('Power (a.u)')
    ax[6].set_xlim(xmin = 10.0)
    
    if maxDict.has_key('periodogram'):
        ax[6].set_ylim(maxDict['periodogram'])

    else:
        maxDict['periodogram'] = ax[6].get_ylim()
    
#        ax[7].autoscale(enable=True, axis='y', tight=True)
    

    numberActiveNeurons = 30  
    fullResTime = np.arange(len(output.Vm[0])) * 0.01
    
    for membraneVoltage in output.Vm[:numberActiveNeurons]:
        ax[4].plot(fullResTime, membraneVoltage*1000.0, cmt.inhibitionColor, alpha = 0.05)
    
    ax[4].plot(fullResTime, output.Vm[:numberActiveNeurons, :].mean(axis = 0)*1000.0, cmt.inhibitionColor)
#    ax[4].plot(fullResTime, output.Vm[numberActiveNeurons:, :].mean(axis = 0)*1000.0, '#ff7f0e')
    ax[4].set_ylabel(cmt.labelPotential)
    
    
    spikesPyr = np.concatenate((output.spiketimes_burst, output.spiketimes_poi))
#    spikesBurst = out.spiketimes_burst

    print "Number of spikes", len(spikesPyr)


    N = len(spikesPyr)
    
    x = np.array([0.0]*N)
    y = np.array([0.0]*N)
    each = 0;
    for each in range(N): #extremly costly way to generate the wanted spike train
        x[each] = spikesPyr[each][0]*1000
        y[each] = spikesPyr[each][1] * 5.0
        each+=1


    ax[1].scatter(x,y, marker = scatterMarker, s = markersize, c = cmt.excitatoryColor, alpha = 0.2)
#    ax[1].set_ylabel('Neurons')

    ax[1].locator_params(axis = 'x', nbins = 3)


#    plt.subplots_adjust(left = 0.12, bottom = 0.13)
#    

    #networkActivity = gt.smooth_rate(spikesPyr, sigma_krnl=0.0, simTime = 0.3)
#    times = np.array(range(len(networkActivity)))/100.0
    

#    ax[0].plot(output.times_rate * 1000.0, gaussian_filter(output.rate_burst + output.rate_poi, 2.0), c = cmt.excitatoryColor)
#    ax[0].plot(np.arange(len(networkActivity)) / 100.0, gaussian_filter(networkActivity, 35.0) * 1000.0, c = cmt.excitatoryColor)
    
    # calc the firing rate in 0.1 ms bins
    timesFR, networkActivity = gt.returnTimeResolvedFiringRatesInMsWindows(spikesPyr, 0.3, timeFactor = 10000)
    
#    ax[0].plot(1000.0 * timesFR, gaussian_filter(networkActivity, 3.5), c = cmt.excitatoryColor)
    
    # filtering over 10 consecutive time points... with dt = 0.1 ms 1 ms smoothing.
    ax[0].plot(1000.0 * timesFR, gaussian_filter(networkActivity, 10.0), c = cmt.excitatoryColor)
    ax[0].set_ylabel(cmt.labelFiringRateShort)
    ax[0].set_xlabel('Time (ms)')
    ax[0].set_ylim(ymin = 0.0)
    
    if maxDict.has_key('excitationActivity'):
        ax[0].set_ylim(maxDict['excitationActivity'])

    else:
        maxDict['excitationActivity'] = ax[0].get_ylim()
        

    
    for axis in ax:
        
        axis.get_yaxis().set_label_coords(-0.12, 0.5)
        if axis in [ax[5], ax[6]]:
            gt.remove_box_cue(axis, keepx = True)
            axis.locator_params(axis = 'y', nbins=3)
        elif (axis == ax[2]) | (axis == ax[1]):
            gt.remove_box_cueAll(axis, keepx = False)
        else:
            gt.remove_box_cue(axis, keepx = False)
            axis.locator_params(axis = 'y', nbins=3)
    
        
#    fig.tight_layout()        
    
    ax[0].set_xlim([120,180])
    ax[5].set_xlim([-30, 30])
    diplayPoints = ax[5].transData.transform((30, 0))
    transformed = ax[4].transData.inverted().transform(diplayPoints)
#    print diplayPoints, transformed
    ax[5].set_xlim([-30, transformed[0] - 150.0])
    ax[5].locator_params(axis = 'x', nbins = 3)
    
    ax[2].set_ylim([-50.0, (spikes[-1][1] + 4) * 5.0])
#    ax[6].set_xlim([ax[0].get_xlim()[0], ax[0].get_xlim()[1] * 0.85])
    
    
    # we introduced a new measurement for synchrony recently where we compensate for the case that are no spikes at all.
    
    
    syncIndex2, syncIndexCorr = gt.SynchronyIndexMaxWindow(output, 0.0005, averageFiring, leadingFreq)
    
    spikesAroundSWR = np.array([spiketime for spiketime in output.spiketimes if (spiketime[0] > 0.12) & (spiketime[0] < 0.18)])
    
    print spikesAroundSWR
    syncIndex, histoTimes, histoValues, valuesNoZeros, spikeTimesNoZeros = gt.spikeTrainSynchrony(spikesAroundSWR, 0.0005, bins = bins)
    
    
    
    fig.suptitle(title + ' Sync. ind. = ' +  str(syncIndex)[:4] + ' or ' +  str(syncIndexCorr)[:4], fontsize = 10 )
    
#    gt.saveFig(fig, figName)
#    gt.saveFig(fig, figName, fileFormat='.png' , dpi=300)
    #plt.close()

    if returnFig == True: return fig, maxDict
    else: 
        plt.close()    
        return maxDict
