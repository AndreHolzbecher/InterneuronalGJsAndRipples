# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 09:30:02 2016


@author: andre
"""

# =============================================================================
# The multicompartment cell models are given in Neuron
# and we interact with Neuron using the python interface.
# Here is the file that does all of the multicompartment 
# simulations and deals with Neuron.
# =============================================================================

#%% imports
#from __future__ import unicode_literals
from neuron import h
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import neuronGapJunctionCouplingPub as ngj
import gapAnalyticsTempPub as gt
import cPickle as cp
from functools import partial
from math import isnan
import os
from random import randrange
plt.rcParams.update({'font.size': 16})



#%% main class
class configSimulation:

    def __init__(self, *args, **kwargs):

        self.dendriteType = "Standard"
        self.explorationType = "delay"
        self.resistanceGapJunction = 1000 # in MegaOhm...aka 1nS
        self.vInit = -69.8838
        self.vRest = -69.8838
        self.standardSEClamp = {'dur1':5.0, 'dur2':0.1, 'dur3': 50.0, 'amp1':self.vRest, 'amp2':0, 'amp3':self.vRest}
        self.Numberprocesses = 4
        self.nsegLeaveOut = 5
        self.frontCutOff = 5.0
        self.stimPos = ("somaa", 0.5)

        self.signalSource = ("somaa", 0.5)
        self.signalReceiver = ("somab", 0.5)
        self.gapPosPre = ("dend1a", 0.5)
        self.gapPosPost = ("dend1b", 0.5)


        self.stimulusProtocol = "SEClamp"
        self.SECAmplitude = -50.0
        self.delayMeasure = "MaxMax"
        self.fractionOfMax = 0.90
        self.stimDelay = 10.0
        self.assignMeasureDelayFunction()

        if 'cellModel' in kwargs:
            self.cellModel = kwargs['cellModel']
        else:
            self.cellModel = "Saraga"

        self.assignNeuronModelInits()
        self.dendDict = []

        self.calcAPAmp = True
        
        # parameters for the dual recording AP
        self.scaleDualAP = 1.0
        self.delayDualAP = 0.105

        self.simulationLength = 20.0
        
        
        self.pickDendritesRnd = True
        
    def getDendDic(self):

        if self.cellModel == "Saraga":
            self.dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}
            
            self.dendDictA = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        elif self.cellModel == "Lee":
            self.dendDict = {'cella.bcdendAP1{}': h.cella.bcdendAP1, 'cella.bcdendAP2{}': h.cella.bcdendAP2, 'cella.bcdendBAS1{}': h.cella.bcdendBAS1, 'cella.bcdendBAS2{}': h.cella.bcdendBAS2,
                             'cellb.bcdendAP1{}': h.cellb.bcdendAP1, 'cellb.bcdendAP2{}': h.cellb.bcdendAP2, 'cellb.bcdendBAS1{}': h.cellb.bcdendBAS1, 'cellb.bcdendBAS2{}': h.cellb.bcdendBAS2}

            self.dendDictA = {'cella.bcdendAP1{}': h.cella.bcdendAP1, 'cella.bcdendAP2{}': h.cella.bcdendAP2, 'cella.bcdendBAS1{}': h.cella.bcdendBAS1, 'cella.bcdendBAS2{}': h.cella.bcdendBAS2}

        elif self.cellModel == "Saudargiene":

            self.dendDict = {'cella.radT2{}': h.cella.radT2, 'cella.radM2{}': h.cella.radM2, 'cella.radt2{}': h.cella.radt2, 'cella.lmM2{}': h.cella.lmM2,
                            'cella.lmt2{}': h.cella.lmt2, 'cella.radT1{}': h.cella.radT1, 'cella.radM1{}': h.cella.radM1, 'cella.radt1{}': h.cella.radt1,
                            'cella.lmM1{}': h.cella.lmM1, 'cella.lmt1{}': h.cella.lmt1, 'cella.oriT1{}': h.cella.oriT1, 'cella.oriM1{}': h.cella.oriM1,
                            'cella.orit1{}': h.cella.orit1, 'cella.oriT2{}': h.cella.oriT2, 'cella.oriM2{}': h.cella.oriM2, 'cella.orit2{}': h.cella.orit2,
                            'cellb.radT2{}': h.cellb.radT2, 'cellb.radM2{}': h.cellb.radM2, 'cellb.radt2{}': h.cellb.radt2, 'cellb.lmM2{}': h.cellb.lmM2,
                            'cellb.lmt2{}': h.cellb.lmt2, 'cellb.radT1{}': h.cellb.radT1, 'cellb.radM1{}': h.cellb.radM1, 'cellb.radt1{}': h.cellb.radt1,
                            'cellb.lmM1{}': h.cellb.lmM1, 'cellb.lmt1{}': h.cellb.lmt1, 'cellb.oriT1{}': h.cellb.oriT1, 'cellb.oriM1{}': h.cellb.oriM1,
                            'cellb.orit1{}': h.cellb.orit1, 'cellb.oriT2{}': h.cellb.oriT2, 'cellb.oriM2{}': h.cellb.oriM2, 'cellb.orit2{}': h.cellb.orit2
                            }

            self.dendDictA = {'cella.radT2{}': h.cella.radT2, 'cella.radM2{}': h.cella.radM2, 'cella.radt2{}': h.cella.radt2, 'cella.lmM2{}': h.cella.lmM2,
                            'cella.lmt2{}': h.cella.lmt2, 'cella.radT1{}': h.cella.radT1, 'cella.radM1{}': h.cella.radM1, 'cella.radt1{}': h.cella.radt1,
                            'cella.lmM1{}': h.cella.lmM1, 'cella.lmt1{}': h.cella.lmt1, 'cella.oriT1{}': h.cella.oriT1, 'cella.oriM1{}': h.cella.oriM1,
                            'cella.orit1{}': h.cella.orit1, 'cella.oriT2{}': h.cella.oriT2, 'cella.oriM2{}': h.cella.oriM2, 'cella.orit2{}': h.cella.orit2,
                            }


    def testDendDicSaudargiene(self):
        print self.dendDictA['cella.radT2{}'] == h.cella.radT2
        print self.dendDictA['cella.radM2{}'] == h.cella.radM2
        print self.dendDictA['cella.radt2{}'] == h.cella.radt2
        print self.dendDictA['cella.lmM2{}'] == h.cella.lmM2

        print self.dendDictA['cella.lmt2{}'] == h.cella.lmt2
        print self.dendDictA['cella.radT1{}'] == h.cella.radT1
        print self.dendDictA['cella.radM1{}'] == h.cella.radM1
        print self.dendDictA['cella.radt1{}'] == h.cella.radt1

        print self.dendDictA['cella.lmM1{}'] == h.cella.lmM1
        print self.dendDictA['cella.lmt1{}'] == h.cella.lmt1
        print self.dendDictA['cella.oriT1{}'] == h.cella.oriT1
        print self.dendDictA['cella.oriM1{}'] == h.cella.oriM1

        print self.dendDictA['cella.orit1{}'] == h.cella.orit1
        print self.dendDictA['cella.oriT2{}'] == h.cella.oriT2
        print self.dendDictA['cella.oriM2{}'] == h.cella.oriM2
        print self.dendDictA['cella.orit2{}'] == h.cella.orit2


    def cutFirst(func):
        def func_wrapper(*args, **kwrds):
            return func(*args, **kwrds).loc[args[0].frontCutOff::]
        return func_wrapper

    def measureKink(self, data):
        dx = data.index[2] - data.index[1]
        data2derv = np.gradient(np.gradient(data, dx), dx)

        return data.index[np.argmax(data2derv)]


    def measureKinkPlot(self, data):
        dx = data.index[2] - data.index[1]
        data2derv = np.gradient(np.gradient(data, dx), dx)
#        plot(data.index, data2derv)
        xx = np.arange(0, len(data2derv)) * float(dx)
        yy = data2derv
        print len(xx), len(yy)
        fig = plt.figure()
        ax2 = fig.add_subplot(111)
        ax2.plot(xx, yy)
        return data.index[np.argmax(data2derv)]

    def measureMaxRise(self, data):
        
        dx = data.index[2] - data.index[1]
        data1derv = np.gradient(data, dx)
#        xx = np.arange(0, len(data1derv)) * float(dx)
        yy = data1derv
#        print len(xx), len(yy)
        fig = plt.figure()
        ax2 = fig.add_subplot(111)
        ax2.plot(data.index, yy)
        ax3 = ax2.twinx()
        dataMax = data.idxmax()
        data1dervDF = pd.DataFrame(data1derv, index = data.index)
        
        data.plot(ax = ax3)
        print (data1dervDF[data1dervDF.index < dataMax]).idxmax()
        print data1dervDF
        return (data1dervDF[data1dervDF.index < dataMax]).idxmax().iloc[0]


    def measureMaxMaxDelay(self, data):
        print "MaxMax"
        if (self.stimulusProtocol == "SEClamp") & (self.explorationType != "GJdelay"):
            return data.iloc[:,1].idxmax() - ngj.get_maxIdxBlock(data.iloc[:,0])
        else:
            return data.iloc[:,1].idxmax() - data.iloc[:,0].idxmax()

    def measureFracMaxDelay(self, data):
        print "FracMax"
        if (self.stimulusProtocol == "SEClamp") & (self.explorationType != "GJdelay"):
            return ngj.findFractionMaxium(data.iloc[:,1], fractionOfMax = self.fractionOfMax) - self.stimDelay
        else:
            return ngj.findFractionMaxium(data.iloc[:,1], fractionOfMax = self.fractionOfMax) - ngj.findFractionMaxium(data.iloc[:,0], fractionOfMax = self.fractionOfMax)

    def measureKinkMaxDelay(self, data):
        print "K"
        if (self.stimulusProtocol == "SEClamp") & (self.explorationType != "GJdelay"):
            print ngj.get_maxIdxBlock(data.iloc[:,0], maxPosition = 'right')
            delay = self.measureKink(data.iloc[:,1]) -  self.stimDelay
            return delay
        else:
            delay = self.measureKink(data.iloc[:,1]) - self.measureKink(data.iloc[:,0])
            return delay


    def measureMaxRiseDelay(self, data):
        if (self.stimulusProtocol == "SEClamp") & (self.explorationType != "GJdelay"):
            print ngj.get_maxIdxBlock(data.iloc[:,0], maxPosition = 'right')
            delay = self.measureMaxRise(data.iloc[:,1]) -  self.stimDelay
            return delay
        else:
            delay = self.measureMaxRise(data.iloc[:,1]) - self.measureMaxRise(data.iloc[:,0])
            return delay


    def measureMaxAttenuation(self, data):
        print "MaxMax"
        if (self.stimulusProtocol == "SEClamp") & (self.explorationType != "GJdelay"):
            raise NameError('Only defined for APIClamp')
        else:
            return data.iloc[:,1].max() / data.iloc[:,0].max()

    def assignMeasureDelayFunction(self):

        if self.delayMeasure == "MaxMax":
            print "Measuring MaxMax"
            self.measureDelayFunction = self.measureMaxMaxDelay
        elif self.delayMeasure == "FracMax":
            print "Measuring FracMax"
            self.measureDelayFunction = self.measureFracMaxDelay
        elif self.delayMeasure == "KinkMax":
            print "Measuring KinkMax"
            self.measureDelayFunction = self.measureKinkMaxDelay
        elif self.delayMeasure == "MaxRise":
            print "Measuring MaxRise"
            self.measureDelayFunction = self.measureMaxRiseDelay        
            
        else: print "whaaaat???"

    def calcDistance(self, compOne, coordinateOne, compTwo, coordinateTwo):
        # gets the distance for two points in dendritic tree
#        self.getDendDic()

        if compOne == compTwo: return abs(coordinateOne-coordinateTwo)*compOne.L
        else:
            children = compOne.children()
            if len(children) > 0:

                for child in compOne.children():
                    dist = compOne.L*(1.0-coordinateOne) + self.calcDistance(child, 0.0, compTwo, coordinateTwo)
                    if dist < 10000: return dist

            else: return 10000

        return 10000


    def getColorForDifferentDendrites(self, dend):
        for i in np.arange(4)+1:
            if str(i)+"a" in dend:
                if '[' in dend:
                    return tuple((30.0-float(dend[-2]))/30.0*np.array(ngj.tableau20[i*2]))
                else: return ngj.tableau20[i*2]

    def plotDataNew(self, 
                    resultsDelayVsPosition,
                    figname,
                    ylabel = "Delay (ms)",
                    ax = '',
                    fig = '',
                    plotstyle = "-x",
                    selector = [1,2,3,4],
                    **kwrds):
        
        from re import sub

        if ax == '':
            fig = plt.figure(figsize = (10,7))
            ax = fig.add_subplot(111)

        #ax.set_color_cycle(ngj.tableau20)
        for each in resultsDelayVsPosition.columns:
            if int(sub("\D","", each)[0]) in selector:
                resultsDelayVsPosition.loc[:,each][resultsDelayVsPosition.loc[:,each]>=-10].plot(axes = ax, style = plotstyle, c = self.getColorForDifferentDendrites(each), **kwrds)
#            resultsDelayVsPosition.loc[:,each][map(isnan, resultsDelayVsPosition.loc[:,each])].plot(axes = ax, style = plotstyle)#, c = self.getColorForDifferentDendrites(each))
        #ax.set_ylim([0,2.5])
        ax.set_ylabel(ylabel)
        ax.set_xlabel("Distance to soma (µm)")
        ax.set_title(figname)
        #ax.legend(loc = 'upper left', fontsize = 12)
        gt.saveFig(fig, figname)
        gt.saveFig(fig, figname, fileFormat = ".png")
        return fig, ax

    def plotDataAll(self, 
                    resultsDelayVsPosition,
                    figname = '',
                    ylabel = "Delay [ms]",
                    ax = '',
                    fig = '',
                    plotstyle = "-x",
                    color = 1,
                    **kwrds):


        if ax == '':
            fig = plt.figure(figsize = (10,7))
            ax = fig.add_subplot(111)
        colorCode = ngj.tableau20[color * 2]
        #ax.set_color_cycle(ngj.tableau20)
        for each in resultsDelayVsPosition.columns:
            resultsDelayVsPosition.loc[:,each][resultsDelayVsPosition.loc[:,each]>=-10].plot(ax = ax, style = plotstyle, c = colorCode, **kwrds)
#            resultsDelayVsPosition.loc[:,each][map(isnan, resultsDelayVsPosition.loc[:,each])].plot(axes = ax, style = plotstyle)#, c = self.getColorForDifferentDendrites(each))
        #ax.set_ylim([0,2.5])
        ax.set_ylabel(ylabel)
        ax.set_xlabel("Distance to soma (µm)")
        if figname != '':
            ax.set_title(figname)
            #ax.legend(loc = 'upper left', fontsize = 12)
            gt.saveFig(fig, figname)
            gt.saveFig(fig, figname, fileFormat = ".png")
            
        return fig, ax



    def plotAndClose(self, data, figname, plotMax = False):

        fig = plt.figure()
        ax = fig.add_subplot(111)
        data.plot(ax = ax)
        if plotMax == True: ax.plot(data.idxmax(), data.max(), 'o')
        gt.saveFig(fig, figname, setPath = 'Dendplots')
        gt.saveFig(fig, figname, fileFormat='.png', setPath = 'Dendplots')
        plt.show()
        plt.close(fig)



    def assignNeuronModelInits(self):
        # This function sets the different initializing functions for the neuron models

        if self.cellModel == "Saraga":
            self.initHocTwoNeurons = self.initHocTwoNeuronsSaraga
            self.initHoc = self.initHocSaraga
            self.initHocTwoNeuronsPassive = self.initHocTwoNeuronsPassiveSaraga
            self.generatePositionDelayData2Neurons = self.generatePositionDelayData2NeuronsSaraga
            self.insertStimulation = self.insertStimulationSaraga
            self.generateAPattentuation = self.generateAPattentuationSaraga

        elif self.cellModel == "Lee":
            self.initHocTwoNeurons = self.initHocTwoNeuronsLee
            self.insertStimulation = self.insertStimulationLee
            self.initHoc = self.initHocTwoNeuronsLee
            self.generatePositionDelayData2Neurons = self.generatePositionDelayData2NeuronsSaudargiene
            self.generateAPattentuation = self.generateAPattentuationSaudargiene
#            self.initHoc = self.initHocLee
            self.initHocTwoNeuronsPassive = self.initHocTwoNeuronsLeePassive

        elif self.cellModel == "Saudargiene":
            self.initHocTwoNeurons = self.initHocTwoNeuronsSaudargiene
            self.initHoc = self.initHocTwoNeuronsSaudargiene
            self.insertStimulation = self.insertStimulationSaudargiene
            self.generatePositionDelayData2Neurons = self.generatePositionDelayData2NeuronsSaudargiene
            self.generateAPattentuation = self.generateAPattentuationSaudargiene
#            self.initHoc = self.initHocSaudargiene
            self.initHocTwoNeuronsPassive = self.initHocTwoNeuronsSaudargienePassive
            self.initHocTwoNeuronsPassiveDenActiveSoma = self.initHocTwoNeuronsSaudargienePassiveDensActiveSoma

        else: print "whaaaat???"

        print "Using {} neuron model".format(self.cellModel)


    def initHocSaraga(self):
        h("activeDendrites = 0.0")
        h("passiveReversal = -70")
        h.load_file("nrngui.hoc")
        h.load_file("cella.hoc")
        h.load_file("progOnlyA.hoc")
        h.load_file("extras.hoc")

    def initHocTwoNeuronsSaraga(self):
        h("activeDendrites = 0.0")
        h("passiveReversal = -70")
        h.load_file("nrngui.hoc")
        h.load_file("cella.hoc")
        h.load_file("cellb.hoc")
        h.load_file("prog2.hoc")
        h.load_file("extras.hoc")

    def initHocTwoNeuronsPassiveSaraga(self):
        h("activeDendrites = 0.0")
        h("passiveReversal = -70")
        h.load_file("nrngui.hoc")
        h.load_file("cella.hoc")
        h.load_file("cellb.hoc")
        h.load_file("prog2Passive.hoc")
        h.load_file("extras.hoc")

    def initHocTwoNeuronsLee(self):

#        os.chdir("/home/holzbecher/PhD/Gap Junctions/Neuron/SaudargieneEtAl2015")
        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/superdeep/cells/class_pvbasketcellNew.hoc"
        h.load_file(cellpath)
#        h.load_file(cellpath)
        h("objref cella, cellb ")
        h("cella = new pvbasketcell()")
        h("cellb = new pvbasketcell()")
        
    def initHocTwoNeuronsLeePassive(self):

#        os.chdir("/home/holzbecher/PhD/Gap Junctions/Neuron/SaudargieneEtAl2015")
        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/superdeep/cells/class_pvbasketcellNew_Passive.hoc"
        h.load_file(cellpath)
#        h.load_file(cellpath)
        h("objref cella, cellb ")
        h("cella = new pvbasketcell()")
        h("cellb = new pvbasketcell()")
        print h.cella, h.cellb        
        
    def initHocNNeuronsLee(self, NumberNeurons):


        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/superdeep/cells/class_pvbasketcellNew.hoc"
        h.load_file(cellpath)
        #        h.load_file(cellpath)
        createRefs = "objref " +  "{}, " * NumberNeurons + "{}"
        cellList = map(lambda x: "cell" + x, map(str, range(NumberNeurons + 1)))
        createRefs = createRefs.format(*cellList)
        h(createRefs)
        
        
        for cell in cellList: 
            h("{} = new pvbasketcell()".format(cell))
            
         
        h("objref gapJunction[{}]".format((NumberNeurons * 2)))
        
        
        gapPosPre = ("cell0.bcdendAP2[3]", 0.9)
        
        couplingList = ['bcdendAP1', 'bcdendAP2', 'bcdendBAS1', 'bcdendBAS2']
        
        gapPosPre = ("cell0.bcdendAP2[3]", 0.9)
        
        for i, cell in enumerate(cellList[1:]):
            
            if self.pickDendritesRnd:
                if i == 0:
                    gapPosPre = ("cell0.bcdendAP1[3]", 0.1)
                    gapPosPost = ("cell1.bcdendAP1[3]", 0.1)
                else:
                    gapPosPre = ("cell0.{}[{}]".format(couplingList[randrange(0,4,1)], randrange(0,4,1)), randrange(0.0,100.0,1)/100.0)
                    gapPosPost = ("{}.{}[{}]".format(cell, couplingList[randrange(0,4,1)], randrange(0,4,1)), randrange(0.0,100.0,1)/100.0)
            else:
                gapPosPost = ("{}.bcdendAP2[3]".format(cell), 0.9)
                
            h("gapJunction[{}] = new gap()".format((2 * i)))
            h("gapJunction[{}] = new gap()".format((2 * i + 1)))
            
            h("{} gapJunction[{}].loc({})".format(gapPosPre[0], (2 * i), gapPosPre[1]))
            h("{} gapJunction[{}].loc({})".format(gapPosPost[0], (2 * i + 1), gapPosPost[1]))
            
            h("{1} gapJunction[{0}].del=0".format((2 * i), gapPosPre[0]))
            h("{1} gapJunction[{0}].del=0".format((2 * i + 1), gapPosPost[0]))
            
            h("setpointer gapJunction[{1}].vgap, {0}.v({2})".format(gapPosPre[0], (2 * i + 1), gapPosPre[1]))
            h("setpointer gapJunction[{1}].vgap, {0}.v({2})".format(gapPosPost[0], (2 * i), gapPosPost[1]))
        
        
            h("gapJunction[{}].r={}".format((2 * i + 1), self.resistanceGapJunction))
            h("gapJunction[{}].r={}".format((2 * i), self.resistanceGapJunction))
            
            
        stimPos = ("cell0.soma", 0.5)    
        h("objref stim")
        h("stim = new IClamp()")
        h("{} stim.loc({})".format(*stimPos))
        h("stim.dur=4.0")
        #h("stim.amp=0.95")
        h("stim.amp=1.5")
        h("stim.del={}".format(self.stimDelay))
         
        
        
        createRefsRecord = "objref timeVec, " +  "{}, " * NumberNeurons + "{}"
        cellListRecord = map(lambda x: "recordCell" + x, map(str, range(NumberNeurons + 1)))
        createRefsRecord = createRefsRecord.format(*cellListRecord)
        
        h(createRefsRecord)
           
        
        h("timeVec = new Vector()")
        
        h.dt = 0.001
        for i, record in enumerate(cellListRecord):
            
            h("{} = new Vector()".format(record))
            gapPosPost = ("{}.soma".format(cellList[i]), 0.5)
            h("{}.record(&{}.v({}),dt)".format(record, gapPosPost[0], gapPosPost[1]))
        
        
        h("timeVec.record(&t,dt)")
        h.tstop = 30
        h.t = 0
    #    h.dt = 0.001
        h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        
        
        results = pd.DataFrame(index = np.array(h.timeVec))
        
        # fill the data in the panda frame
        # never use exec
        for record in cellListRecord:
            exec("results['{0}'] = h.{0}".format(record))  
        

        return results



    def multipleGJsExperimentBuildup(self, NumberNeurons):


        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/superdeep/cells/class_pvbasketcellNew.hoc"
        h.load_file(cellpath)
        #        h.load_file(cellpath)
        createRefs = "objref " +  "{}, " * NumberNeurons + "{}"
        cellList = map(lambda x: "cell" + x, map(str, range(NumberNeurons + 1)))
        createRefs = createRefs.format(*cellList)
        h(createRefs)
        resultsDifferentNumberofGJs = pd.DataFrame()
        
        for cell in cellList: 
            h("{} = new pvbasketcell()".format(cell))
                
             
        h("objref gapJunction[{}]".format((NumberNeurons * 2)))
        
        for i in range(NumberNeurons):
            
    
            gapPosPre = ("cell0.bcdendAP2[3]", 0.9)
            
            couplingList = ['bcdendAP1', 'bcdendAP2', 'bcdendBAS1', 'bcdendBAS2']
            
            gapPosPre = ("cell0.bcdendAP2[3]", 0.9)
            

                
            if self.pickDendritesRnd:
                if i == 0:
                    gapPosPre = ("cell0.bcdendAP1[3]", 0.1)
                    gapPosPost = ("cell1.bcdendAP1[3]", 0.1)
                else:
                    gapPosPre = ("cell0.{}[{}]".format(couplingList[randrange(0,4,1)], randrange(0,4,1)), randrange(0.0,100.0,1)/100.0)
#                    gapPosPre = ("cell0.{}[{}]".format("bcdendAP1", randrange(0,4,1)), randrange(0.0,100.0,1)/100.0)
                    gapPosPost = ("{}.{}[{}]".format(cellList[i + 1], couplingList[randrange(0,4,1)], randrange(0,4,1)), randrange(0.0,100.0,1)/100.0)
            else:
                gapPosPost = ("{}.bcdendAP2[3]".format(cellList[i + 1]), 0.9)
                
                
            print i, gapPosPre, gapPosPost
            h("gapJunction[{}] = new gap()".format((2 * i)))
            h("gapJunction[{}] = new gap()".format((2 * i + 1)))
            
            h("{} gapJunction[{}].loc({})".format(gapPosPre[0], (2 * i), gapPosPre[1]))
            h("{} gapJunction[{}].loc({})".format(gapPosPost[0], (2 * i + 1), gapPosPost[1]))
            
            h("{1} gapJunction[{0}].del=0".format((2 * i), gapPosPre[0]))
            h("{1} gapJunction[{0}].del=0".format((2 * i + 1), gapPosPost[0]))
            
            h("setpointer gapJunction[{1}].vgap, {0}.v({2})".format(gapPosPre[0], (2 * i + 1), gapPosPre[1]))
            h("setpointer gapJunction[{1}].vgap, {0}.v({2})".format(gapPosPost[0], (2 * i), gapPosPost[1]))
        
        
            h("gapJunction[{}].r={}".format((2 * i + 1), self.resistanceGapJunction))
            h("gapJunction[{}].r={}".format((2 * i), self.resistanceGapJunction))
                
                
            stimPos = ("cell0.soma", 0.5)    
            
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*stimPos))
            h("stim.dur=4.0")
            #h("stim.amp=0.95")
            h("stim.amp=1.5")
            h("stim.del={}".format(self.stimDelay))
             
            
            
            createRefsRecord = "objref timeVec, " +  "{}, " * (i + 1) + "{}"
            cellListRecord = map(lambda x: "recordCell" + x, map(str, range(i + 2)))
            createRefsRecord = createRefsRecord.format(*cellListRecord)
            
            h(createRefsRecord)
               
            
            h("timeVec = new Vector()")
            
            h.dt = 0.001
            
            for j, record in enumerate(cellListRecord):
                
                h("{} = new Vector()".format(record))
                gapPosPost = ("{}.soma".format(cellList[j]), 0.5)
                h("{}.record(&{}.v({}),dt)".format(record, gapPosPost[0], gapPosPost[1]))
            
            
            h("timeVec.record(&t,dt)")
            h.tstop = 30
            h.t = 0
        #    h.dt = 0.001
            h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
            h("init()")
            h("stdinit()")
            h.run()
            
            
            results = pd.DataFrame(index = np.array(h.timeVec))
            
            # fill the data in the panda frame
            # never use exec
            for record in cellListRecord:
                exec("results['{0}'] = h.{0}".format(record))  
            
            results.iloc[:,1:].plot()
            
            resultsDifferentNumberofGJs["N = " + str(i + 1)] = h.recordCell1    
            
        resultsDifferentNumberofGJs.index = np.array(h.timeVec)
        

        return resultsDifferentNumberofGJs


    def initHocTwoNeuronsSaudargiene(self):

#        os.chdir("/home/holzbecher/PhD/Gap Junctions/Neuron/SaudargieneEtAl2015")
        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/SaudargieneEtAl2015/basket_cell17S.hoc"
        h.load_file(cellpath)
        h("objref cella, cellb ")
        h("cella = new BasketCell()")
        h("cellb = new BasketCell()")

    def initHocTwoNeuronsSaudargienePassive(self):

#        os.chdir("/home/holzbecher/PhD/Gap Junctions/Neuron/SaudargieneEtAl2015")
        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/SaudargieneEtAl2015/basket_cell17SPassive.hoc"
        h.load_file(cellpath)
        h("objref cella, cellb ")
        h("cella = new BasketCell()")
        h("cellb = new BasketCell()")
        print h.cella, h.cellb      
        
        
    def initHocTwoNeuronsSaudargienePassiveDensActiveSoma(self):

#        os.chdir("/home/holzbecher/PhD/Gap Junctions/Neuron/SaudargieneEtAl2015")
        h.load_file("nrngui.hoc")
        print gt.getSystemPath()
        cellpath = gt.getSystemPath() + "Gap Junctions/Neuron/SaudargieneEtAl2015/basket_cell17SActiveSomaOnly.hoc"
        h.load_file(cellpath)
        h("objref cella, cellb ")
        h("cella = new BasketCell()")
        h("cellb = new BasketCell()")
        print h.cella, h.cellb    

    def testAttenuationProtocoll(self):

        self.initHoc()
        holdingVoltage = -65
        mVstep = 1
        dend = "dend4a[5]"
        denPos = 0.5
        resultsLow = self.generateTracesAttenuation('somaa', 0.5, dend, denPos, holdingVoltage = holdingVoltage)
        resultsLow.plot()
        resultsHigh = self.generateTracesAttenuation('somaa', 0.5, dend, denPos, holdingVoltage = holdingVoltage + mVstep)
        resultsHigh.plot()
        # either make dependent on function or use if clause
        print resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0],  resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]
        delayValue = np.log((resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0])/(resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]))

        return delayValue

    def insertStimulationSaraga(self):
        # insert stimulus into the neuron

        if self.stimulusProtocol == "SEClamp":
            h("objref stim")
            h("stim = new SEClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            # how to set the stimulus in a proper way?
            h("stim.dur1={}".format(self.stimDelay))
            h("stim.dur2=0.1")
            h("stim.dur3=30.0")
            h("stim.amp1=-69.8838")
            h("stim.amp2={}".format(self.SECAmplitude))#-60 -40
            print "amplitude assigned"
            h("stim.amp3=-69.8838")
            h("stim.rs=0.01")

        elif self.stimulusProtocol == "IClampdual":
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=0.1")
            h("stim.amp=1")
            h("stim.del={}".format(self.stimDelay))

            h("objref stim2")
            h("stim2 = new IClamp()")
            h("{} stim2.loc({})".format(*self.stimPos))
            h("stim2.dur=0.6")
            h("stim2.amp=-0.175")
            h("stim2.del={}".format(self.stimDelay + 0.105))


        elif self.stimulusProtocol == "dualAP":
            
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=0.1")
            h("stim.amp={}".format(1.0*self.scaleDualAP))
            h("stim.del={}".format(self.stimDelay))

            h("objref stim2")
            h("stim2 = new IClamp()")
            h("{} stim2.loc({})".format(*self.stimPos))
            h("stim2.dur=0.6")
            h("stim2.amp={}".format(-0.175 * self.scaleDualAP))
            h("stim2.del={}".format(self.stimDelay + self.delayDualAP))

        elif self.stimulusProtocol == "AP":
            print "here we go"
        
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=2.0")
            h("stim.amp=0.3")
            h("stim.del={}".format(self.stimDelay))

    def insertStimulationLee(self):
        # insert stimulus into the neuron

        if self.stimulusProtocol == "SEClamp":
            print "dont need this..."


        elif self.stimulusProtocol == "IClampdual":
            print "dont need this..."

        elif self.stimulusProtocol == "AP":

            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=4.0")
            h("stim.amp=0.95")
            h("stim.del={}".format(self.stimDelay))
            
        elif self.stimulusProtocol == "dualAP":
            
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=0.1")
            h("stim.amp={}".format(1.0*self.scaleDualAP))
            h("stim.del={}".format(self.stimDelay))

            h("objref stim2")
            h("stim2 = new IClamp()")
            h("{} stim2.loc({})".format(*self.stimPos))
            h("stim2.dur=0.6")
            h("stim2.amp={}".format(-0.175 * self.scaleDualAP))
            h("stim2.del={}".format(self.stimDelay + self.delayDualAP))
            print "this works"


    def insertStimulationSaudargiene(self):
        # insert stimulus into the neuron

        if self.stimulusProtocol == "SEClamp":
            print "dont need this..."


        elif self.stimulusProtocol == "IClampdual":
            print "dont need this..."


        elif self.stimulusProtocol == "AP":
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=4.0")
            h("stim.amp=0.5")
            h("stim.del={}".format(self.stimDelay))
            
        elif self.stimulusProtocol == "dualAP":
            
            h("objref stim")
            h("stim = new IClamp()")
            h("{} stim.loc({})".format(*self.stimPos))
            h("stim.dur=0.1")
            h("stim.amp={}".format(1.0*self.scaleDualAP))
            h("stim.del={}".format(self.stimDelay))

            h("objref stim2")
            h("stim2 = new IClamp()")
            h("{} stim2.loc({})".format(*self.stimPos))
            h("stim2.dur=0.6")
            h("stim2.amp={}".format(-0.175 * self.scaleDualAP))
            h("stim2.del={}".format(self.stimDelay + self.delayDualAP))

    @cutFirst
    def generateTraces(self, signalSource, signalPos, signalReceiverName, reiverPos):

        stimPos = (signalSource, signalPos)
        self.stimPos = stimPos
        signalSource = stimPos
        signalReceiver = (signalReceiverName, reiverPos)

        self.insertStimulation()

        h("objref sourceVec, receiverVec, timeVec")

        h("sourceVec = new Vector()")
        h("receiverVec = new Vector()")
        h("timeVec = new Vector()")

        h("sourceVec.record(&{}.v({}),dt)".format(*signalSource))
        h("receiverVec.record(&{}.v({}),dt)".format(*signalReceiver))

        h("timeVec.record(&t,dt)")
        h.tstop = 20
        h.t = 0
        h.dt = 0.001
        h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        data = np.vstack((np.array(h.sourceVec), np.array(h.receiverVec)))

        results = pd.DataFrame(index = np.array(h.timeVec), data = data.T)
        results.columns = [signalSource, signalReceiverName]

        return results


    @cutFirst
    def generateTracesAttenuation(self, signalSource, signalPos, signalReceiverName, reiverPos, holdingVoltage = 1.0):
        # measured from holding potential
    #    voltageRest = -69.874078
        h("objref stim")
        h("stim = new SEClamp()")
        h("{} stim.loc({})".format(signalSource, signalPos))
        h("stim.dur1=100.0")
        h("stim.dur2=0.1")
        h("stim.dur3=50.0")


        h("stim.amp1={}".format(holdingVoltage))
        h("stim.amp2={}".format(holdingVoltage)) #-60 -40
        h("stim.amp3={}".format(holdingVoltage))
        h("stim.rs=0.01")

        h("objref sourceVec, receiverVec, timeVec")

        h("sourceVec = new Vector()")
        h("receiverVec = new Vector()")
        h("timeVec = new Vector()")

        h("sourceVec.record(&{}.v({}),dt)".format(signalSource, signalPos))
        h("receiverVec.record(&{}.v({}),dt)".format(signalReceiverName, reiverPos))

        h("timeVec.record(&t,dt)")
        h.tstop = 50
        h.t = 0
        h.dt = 0.001
        h.v_init = holdingVoltage # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        data = np.vstack((np.array(h.sourceVec), np.array(h.receiverVec)))

        results = pd.DataFrame(index = np.array(h.timeVec), data = data.T)
        results.columns = [signalSource, signalReceiverName]
        results = results
        return results


    @cutFirst
    def generateGJCouplingTraces(self, gapPosPre = ("dend3a[8]", 0.1), gapPosPost = ("somab", 0.5)):

#        self.initHocTwoNeurons()
        #fix protocol. Stimulation site and readoutsite are constant
        stimPos = ("somaa", 0.5)
        self.stimPos = stimPos
        signalSource = stimPos
        signalReceiver = ("somab", 0.5)

        h("objref gapJunction[2]")

        h("for i=0,1 gapJunction[i] = new gap()")
        h("{} gapJunction[0].loc({})".format(*gapPosPre))
        h("{} gapJunction[1].loc({})".format(*gapPosPost))
        h("{} gapJunction[0].del=0".format(gapPosPre[0]))
        h("{} gapJunction[1].del=0".format(gapPosPost[0]))
        h("setpointer gapJunction[0].vgap, {}.v({})".format(*gapPosPost))
        h("setpointer gapJunction[1].vgap, {}.v({})".format(*gapPosPre))


        h("gapJunction[0].r={}".format(self.resistanceGapJunction))
        h("gapJunction[1].r={}".format(self.resistanceGapJunction))

        self.insertStimulation()



        h("objref sourceVec, receiverVec, timeVec")

        h("sourceVec = new Vector()")
        h("receiverVec = new Vector()")
        h("timeVec = new Vector()")

        h("sourceVec.record(&{}.v({}),dt)".format(*signalSource))
        h("receiverVec.record(&{}.v({}),dt)".format(*signalReceiver))

        h("timeVec.record(&t,dt)")
        
        h.tstop = 30
        h.t = 0
        h.dt = 0.001
        h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        data = np.vstack((np.array(h.sourceVec), np.array(h.receiverVec)))

        results = pd.DataFrame(index = np.array(h.timeVec), data = data.T)
        results.columns = [signalSource, signalReceiver]



        return results


    @cutFirst
    def generateGJCouplingTracesSaudargiene(self, gapPosPre = ("dend3a[8]", 0.1), gapPosPost = ("somab", 0.5)):

#        self.initHocTwoNeurons()
        #fix protocol. Stimulation site and readoutsite are constant
        stimPos = ("cella.soma", 0.5)
        self.stimPos = stimPos
        signalSource = stimPos
        signalReceiver = ("cellb.soma", 0.5)

        h("objref gapJunction[2]")

        h("for i=0,1 gapJunction[i] = new gap()")
        h("{} gapJunction[0].loc({})".format(*gapPosPre))
        h("{} gapJunction[1].loc({})".format(*gapPosPost))
        h("{} gapJunction[0].del=0".format(gapPosPre[0]))
        h("{} gapJunction[1].del=0".format(gapPosPost[0]))
        h("setpointer gapJunction[0].vgap, {}.v({})".format(*gapPosPost))
        h("setpointer gapJunction[1].vgap, {}.v({})".format(*gapPosPre))


        h("gapJunction[0].r={}".format(self.resistanceGapJunction))
        h("gapJunction[1].r={}".format(self.resistanceGapJunction))

        self.insertStimulation()



        h("objref sourceVec, receiverVec, timeVec")

        h("sourceVec = new Vector()")
        h("receiverVec = new Vector()")
        h("timeVec = new Vector()")

        h("sourceVec.record(&{}.v({}),dt)".format(*signalSource))
        h("receiverVec.record(&{}.v({}),dt)".format(*signalReceiver))

        h("timeVec.record(&t,dt)")
        h.tstop = 30
        h.t = 0
        h.dt = 0.001
        h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        
        
        print np.array(h.sourceVec), np.array(h.receiverVec)
        
        data = np.vstack((np.array(h.sourceVec), np.array(h.receiverVec)))
        
        print np.array(h.timeVec)
        print data
        results = pd.DataFrame(index = np.array(h.timeVec), data = data.T)
        results.columns = [signalSource, signalReceiver]



        return results

    @cutFirst
    def generateGJCouplingTraces2(self, gapPosPre = ("dend3a[8]", 0.1), gapPosPost = ("somab", 0.5)):

        if self.dendriteType == "Standard":
            self.initHocTwoNeurons()
        if self.dendriteType == "Passive":
            self.initHocTwoNeuronsPassive()


        #fix protocol. Stimulation site and readoutsite are constant
        stimPos = ("somaa", 0.5)
        self.stimPos = stimPos
        signalSource = stimPos
        signalReceiver = ("somab", 0.5)

        self.signalSource = signalSource
        self.signalReceiver = signalReceiver
        self.gapPosPre = gapPosPre
        self.gapPosPost = gapPosPost




        h("objref gapJunction[2]")

        h("for i=0,1 gapJunction[i] = new gap()")
        h("{} gapJunction[0].loc({})".format(*gapPosPre))
        h("{} gapJunction[1].loc({})".format(*gapPosPost))
        h("{} gapJunction[0].del=0".format(gapPosPre[0]))
        h("{} gapJunction[1].del=0".format(gapPosPost[0]))
        h("setpointer gapJunction[0].vgap, {}.v({})".format(*gapPosPost))
        h("setpointer gapJunction[1].vgap, {}.v({})".format(*gapPosPre))


        h("gapJunction[0].r={}".format(self.resistanceGapJunction))
        h("gapJunction[1].r={}".format(self.resistanceGapJunction))
    # current clamp
    #    h("objref stim")
    #    h("stim = new IClamp()")
    #    h("{} stim.loc({})".format(*stimPos))
    #
    #    h("stim.dur=0.1")
    #    h("stim.amp=2")
    #    h("stim.del=5.0")

    # voltage clamp
        self.insertStimulation()
        self.insertRecording()

        h.t = 0
        h.dt = 0.001
        h.v_init = self.vInit # for 0: 69.8838, for 0.1: 69.6594, for 1.0 74.6116, for Hu et al (333), 70.1467
        h("init()")
        h("stdinit()")
        h.run()
        data = np.vstack((np.array(h.sourceVec), np.array(h.receiverVec), np.array(h.gapPosPreVec), np.array(h.gapPosPostVec)))

        results = pd.DataFrame(index = np.array(h.timeVec), data = data.T)
        results.columns = [signalSource, signalReceiver, gapPosPre[0], gapPosPost[0]]



        return results

    def insertRecording(self):

        h("objref sourceVec, receiverVec, gapPosPreVec, gapPosPostVec, timeVec")

        h("sourceVec = new Vector()")
        h("receiverVec = new Vector()")
        h("gapPosPreVec = new Vector()")
        h("gapPosPostVec = new Vector()")
        h("timeVec = new Vector()")

        h("sourceVec.record(&{}.v({}),dt)".format(*self.signalSource))
        h("receiverVec.record(&{}.v({}),dt)".format(*self.signalReceiver))
        h("gapPosPreVec.record(&{}.v({}),dt)".format(*self.gapPosPre))
        h("gapPosPostVec.record(&{}.v({}),dt)".format(*self.gapPosPost))

        h("timeVec.record(&t,dt)")
        h.tstop = self.simulationLength 

    # pass generateTraces function to this function
    def generatePositionDelayData(self, dend, reverse = False):

        self.initHoc()
        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}
        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                print compartment
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend)):#[:-5:-1]:
                    print denPos
                    if not reverse:
                        results = self.generateTraces('somaa', 0.5, dend.format([compartment]), denPos)
                    else:
                        results = self.generateTraces(dend.format([compartment]), denPos, 'somaa', 0.5)

                    # either make dependent on function or use if clause
                    delayValue = self.measureDelayFunction(results)
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    print delayValue
                    self.plotAndClose(results.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue
        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend)):#[:-5:-1]:
                print denPos
                if not reverse:
                    results = self.generateTraces('somaa', 0.5, dend.format(''), denPos)
                else:
                    results = self.generateTraces(dend.format(''), denPos, 'somaa', 0.5)


                delayValue = self.measureDelayFunction(results)
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                print delayValue, distanceSoma

                self.plotAndClose(results.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue

        return resultsDelayVsPosition

    # pass generateTraces function to this function
    def generatePositionDelayDataPar(self, dend, reverse = False):

        self.initHoc()
        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                    print denPos
                    if not reverse:
                        results = self.generateTraces('somaa', 0.5, dend.format([compartment]), denPos)
                    else:
                        results = self.generateTraces(dend.format([compartment]), denPos, 'somaa', 0.5)
                    # either make dependent on function or use if clause
                    delayValue = results.idxmax()[1]-results.idxmax()[0]
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue
        else:
            nsegDend = dendDict[dend].nseg
            for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                print denPos
                if not reverse:
                    results = self.generateTraces('somaa', 0.5, dend.format(''), denPos)
                else:
                    results = self.generateTraces(dend.format(''), denPos, 'somaa', 0.5)


                delayValue = results.idxmax()[1]-results.idxmax()[0]
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue

        return resultsDelayVsPosition

    def generatePositionDelayGJonly(self, dend):

        if self.dendriteType == "Standard":
            self.initHoc()
        if self.dendriteType == "Passive":
            self.initHocTwoNeuronsPassive()


        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                    print denPos
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('a','b'), denPos)
                    data = self.generateGJCouplingTraces2(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    # either make dependent on function or use if clause

                    delayValue = self.measureDelayFunction(data.iloc[:,2::])
                    print delayValue
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue

        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('a','b'), denPos)
                data = self.generateGJCouplingTraces2(gapPosPre = gapPosA, gapPosPost = gapPosB)
                # either make dependent on function or use if clause

                delayValue = self.measureDelayFunction(data.iloc[:,2::])
                print gapPosA, gapPosB
                print delayValue
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue


        return resultsDelayVsPosition

    def generateAPattentuationSaraga(self, dend):


        if self.dendriteType == "Standard":
            self.initHoc()
        if self.dendriteType == "Passive":
            self.initHocTwoNeuronsPassive()

        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                    print denPos
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('a','b'), denPos)
                    data = self.generateGJCouplingTraces2(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    # either make dependent on function or use if clause


                    lowThresholdPostPeak = data.iloc[:,1][data.index < data.iloc[:,1].idxmax()].min()
                    attentuation = (data.iloc[:,1].max() - lowThresholdPostPeak)
                    print lowThresholdPostPeak, data.iloc[:,1].max(), attentuation

                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = attentuation

        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('a','b'), denPos)
                data = self.generateGJCouplingTraces2(gapPosPre = gapPosA, gapPosPost = gapPosB)
                # either make dependent on function or use if clause

                lowThresholdPostPeak = data.iloc[:,1][data.index < data.iloc[:,1].idxmax()].min()
                attentuation = (data.iloc[:,1].max() - lowThresholdPostPeak)
                print lowThresholdPostPeak, data.iloc[:,1].max(), attentuation
                print "Last Values for calibration", data.iloc[0,-1], data.iloc[1,-1]

                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                self.plotAndClose(data.iloc[:,0], self.stimulusProtocol + dend + str(distanceSoma))
#                self.measureAP(data.iloc[:,0])
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = attentuation


        return resultsDelayVsPosition

    def generateAPattentuationSaudargiene(self, dend):
        # calculate the attenuation of the postsynaptic coupling potential
#        self.initHocTwoNeurons()
        self.getDendDic()
        dendDict = self.dendDictA
#        self.testDendDic()
        resultsDelayVsPosition = pd.DataFrame()
        print dend
        print dendDict[dend]
        print np.size(dendDict[dend])
        if np.size(dendDict[dend]) > 1:


            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
#                    print denPos
                    print "first"
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('cella','cellb'), denPos)
#                    print gapPosA, gapPosB
                    data = self.generateGJCouplingTracesSaudargiene(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    # either make dependent on function or use if clause

                    lowThresholdPostPeak = data.iloc[:,1][data.index < data.iloc[:,1].idxmax()].min()

                    attentuation = (data.iloc[:,1].max() - lowThresholdPostPeak)
                    print lowThresholdPostPeak, data.iloc[:,1].max(), attentuation

                    distanceSoma = self.calcDistance(h.cella.soma, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = attentuation

        else:
#            print dendDict[dend]
            nsegDend = dendDict[dend].nseg
#            print nsegDend
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
#                print denPos
                print "second"
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('cella','cellb'), denPos)
#                print gapPosA, gapPosB
                data = self.generateGJCouplingTracesSaudargiene(
                        gapPosPre = gapPosA,
                        gapPosPost = gapPosB)

                # either make dependent on function or use if clause
                lowThresholdPostPeak = data.iloc[:,1][data.index < data.iloc[:,1].idxmax()].min()

                attentuation = (data.iloc[:,1].max() - lowThresholdPostPeak)
                print lowThresholdPostPeak, data.iloc[:,1].max(), attentuation

                print h.cella.soma, dend, dendDict[dend]
                distanceSoma = self.calcDistance(h.cella.soma, 0.5, dendDict[dend], denPos)

                print distanceSoma, attentuation
#                print dend, dendDict[dend]
                self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = attentuation


        if self.calcAPAmp:
            resultsDelayVsPosition.loc[str(0.0), 'APamplitude'] = self.measureAPwithoutPlot(data.iloc[:,0])
            self.calcAPAmp = False

        return resultsDelayVsPosition

    def generatePositionDelayData2NeuronsSaraga(self, dend):


        self.initHoc()
        dendDict = self.dendDict

        resultsDelayVsPosition = pd.DataFrame()
        print "dendrite  " + dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                    print denPos
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('a','b'), denPos)
                    print gapPosA, gapPosA
                    data = self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    # either make dependent on function or use if clause
                    delayValue = self.measureDelayFunction(data)
                    print delayValue
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue

        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('a','b'), denPos)
                data = self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)

                # either make dependent on function or use if clause
                delayValue = self.measureDelayFunction(data)
                print delayValue
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue


        return resultsDelayVsPosition


    def generatePositionDelayData2NeuronsSaudargiene(self, dend):

        print "the right way"

#        self.initHocTwoNeurons()
        self.getDendDic()
        dendDict = self.dendDictA
#        self.testDendDic()
        resultsDelayVsPosition = pd.DataFrame()
        print dend
        print dendDict[dend]
        print np.size(dendDict[dend])
        if np.size(dendDict[dend]) > 1:


            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
#                    print denPos
                    print "first"
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('cella','cellb'), denPos)
#                    print gapPosA, gapPosB
                    data = self.generateGJCouplingTracesSaudargiene(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    # either make dependent on function or use if clause

                    delayValue = self.measureDelayFunction(data)
                    print "delay", delayValue
                    print dendDict[dend][compartment]

#                    self.getDendDic()
#                    self.testDendDic()
                    distanceSoma = self.calcDistance(h.cella.soma, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma), plotMax = True)
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue

        else:
#            print dendDict[dend]
            nsegDend = dendDict[dend].nseg
#            print nsegDend
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
#                print denPos
                print "second"
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('cella','cellb'), denPos)
#                print gapPosA, gapPosB
                data = self.generateGJCouplingTracesSaudargiene(gapPosPre = gapPosA, gapPosPost = gapPosB)

                # either make dependent on function or use if clause

                delayValue = self.measureDelayFunction(data)
#                self.getDendDic()
#                dendDict = self.dendDictA
#                self.testDendDic()
                print h.cella.soma, dend, dendDict[dend]
                distanceSoma = self.calcDistance(h.cella.soma, 0.5, dendDict[dend], denPos)

                print distanceSoma, delayValue
#                print dend, dendDict[dend]
                self.plotAndClose(data.iloc[:,1], self.stimulusProtocol + dend + str(distanceSoma), plotMax = True)
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue


        return resultsDelayVsPosition


    def generatePositionDelayData2NeuronsPar(self, dend):

        self.initHoc()
        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                    print denPos
                    dend1 = dend.format([compartment])
                    gapPosA = (dend1, denPos)
                    gapPosB = (dend1.replace('a','b'), denPos)
                    data = self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)
                    print data.iloc[:,1].idxmax(), ngj.get_maxIdxBlock(data.iloc[:,0])

                    # either make dependent on function or use if clause

                    delayValue = data.iloc[:,1].idxmax()-ngj.get_maxIdxBlock(data.iloc[:,0])
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    self.plotAndClose(data.iloc[:,1], dend + str(distanceSoma))
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue

        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('a','b'), denPos)
                data = self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)

                # either make dependent on function or use if clause
                print data.iloc[:,1].idxmax(), ngj.get_maxIdxBlock(data.iloc[:,0])

                delayValue = data.iloc[:,1].idxmax() - ngj.get_maxIdxBlock(data.iloc[:,0])
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                self.plotAndClose(data.iloc[:,1], dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue


        return resultsDelayVsPosition

    def onlyOneArgument(self, dendInfo):

        gapPosA = (dendInfo[0], dendInfo[1])
        gapPosB = (dendInfo[0].replace('a','b'), dendInfo[1])
        return self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)

    def generatedendInfoList(self, dendDict, dend):
        dendInfo = []
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                    print denPos
                    dend1 = dend.format([compartment])
                    dendInfo.append((dend1, denPos))


        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                dendInfo.append((dend1, denPos))
        return dendInfo


    def generatePositionDelayData2NeuronsParFurther(self, dend):

        self.initHoc()
        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}


        print dend
        if np.size(dendDict[dend]) > 1:

            dendInfoList = self.generatedendInfoList(dendDict, dend)

            data = p.map(self.onlyOneArgument, dendInfoList)
            print data.iloc[:,1].idxmax(), ngj.get_maxIdxBlock(data.iloc[:,0])

            # either make dependent on function or use if clause

            delayValue = data.iloc[:,1].idxmax()-ngj.get_maxIdxBlock(data.iloc[:,0])
            # Write function that takes list as input and has the previous data structure as an output
#            distanceSoma = 0
            self.plotAndClose(data.iloc[:,1], dend + str(distanceSoma))
            resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue

        else:
            nsegDend = dendDict[dend].nseg
            for denPos in (np.arange(nsegDend + 1)/float(nsegDend))[::self.nsegLeaveOut]:
                print denPos
                dend1 = dend.format('')
                gapPosA = (dend1, denPos)
                gapPosB = (dend1.replace('a','b'), denPos)
                data = self.generateGJCouplingTraces(gapPosPre = gapPosA, gapPosPost = gapPosB)

                # either make dependent on function or use if clause
                print data.iloc[:,1].idxmax(), ngj.get_maxIdxBlock(data.iloc[:,0])

                delayValue = data.iloc[:,1].idxmax() - ngj.get_maxIdxBlock(data.iloc[:,0])
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                self.plotAndClose(data.iloc[:,1], dend + str(distanceSoma))
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue


        return resultsDelayVsPosition


    def generatePositionAttenuationData(self, dendDict, dend, reverse = False, mVstep = 1.0):

        holdingVoltage = -65
        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                    print denPos
                    if not reverse:
                        resultsLow = self.generateTracesAttenuation('somaa', 0.5, dend.format([compartment]), denPos, holdingVoltage = holdingVoltage)
                        resultsHigh = self.generateTracesAttenuation('somaa', 0.5, dend.format([compartment]), denPos, holdingVoltage = holdingVoltage + mVstep)
                    else:
                        resultsLow = self.generateTracesAttenuation(dend.format([compartment]), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage)
                        resultsHigh = self.generateTracesAttenuation(dend.format([compartment]), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage + mVstep)
                    # either make dependent on function or use if clause
                    delayValue = np.log((resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0])/(resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]))
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue
        else:
            nsegDend = dendDict[dend].nseg
            for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                print denPos
                if not reverse:
                    resultsLow = self.generateTracesAttenuation('somaa', 0.5, dend.format(''), denPos, holdingVoltage = holdingVoltage)
                    resultsHigh = self.generateTracesAttenuation('somaa', 0.5, dend.format(''), denPos, holdingVoltage = holdingVoltage + mVstep)
                else:
                    resultsLow = self.generateTracesAttenuation(dend.format(''), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage)
                    resultsHigh = self.generateTracesAttenuation(dend.format(''), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage + mVstep)


                delayValue = np.log((resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0])/(resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]))
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue

        return resultsDelayVsPosition

    def generatePositionAttenuationDataPar(self, dend, reverse = False, mVstep = 1.0):

        self.initHoc()
        dendDict = {'dend1a{}': h.dend1a, 'dend2a{}': h.dend2a, 'dend3a{}': h.dend3a, 'dend4a{}': h.dend4a}

        holdingVoltage = -65
        resultsDelayVsPosition = pd.DataFrame()
        print dend
        if np.size(dendDict[dend]) > 1:

            for compartment in range(np.size(dendDict[dend])):
                nsegDend = dendDict[dend][compartment].nseg
                for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                    print denPos
                    if not reverse:
                        resultsLow = self.generateTracesAttenuation('somaa', 0.5, dend.format([compartment]), denPos, holdingVoltage = holdingVoltage)
                        resultsHigh = self.generateTracesAttenuation('somaa', 0.5, dend.format([compartment]), denPos, holdingVoltage = holdingVoltage + mVstep)
                    else:
                        resultsLow = self.generateTracesAttenuation(dend.format([compartment]), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage)
                        resultsHigh = self.generateTracesAttenuation(dend.format([compartment]), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage + mVstep)
                    # either make dependent on function or use if clause
                    delayValue = np.log((resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0])/(resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]))
                    distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend][compartment], denPos)
                    resultsDelayVsPosition.loc[str(distanceSoma), dend.format([compartment])] = delayValue
        else:
            nsegDend = dendDict[dend].nseg
            for denPos in np.arange(nsegDend + 1)/float(nsegDend):
                print denPos
                if not reverse:
                    resultsLow = self.generateTracesAttenuation('somaa', 0.5, dend.format(''), denPos, holdingVoltage = holdingVoltage)
                    resultsHigh = self.generateTracesAttenuation('somaa', 0.5, dend.format(''), denPos, holdingVoltage = holdingVoltage + mVstep)
                else:
                    resultsLow = self.generateTracesAttenuation(dend.format(''), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage)
                    resultsHigh = self.generateTracesAttenuation(dend.format(''), denPos, 'somaa', 0.5, holdingVoltage = holdingVoltage + mVstep)


                delayValue = np.log((resultsHigh.iloc[-1, 0] - resultsLow.iloc[-1, 0])/(resultsHigh.iloc[-1, 1] - resultsLow.iloc[-1, 1]))
                distanceSoma = self.calcDistance(h.somaa, 0.5, dendDict[dend], denPos)
                resultsDelayVsPosition.loc[str(distanceSoma), dend.format('')] = delayValue

        return resultsDelayVsPosition



    def exploreData(self, explorationType = "delay", reverse = False, mVstep = 1.0, saveData = False, ID = ''):

        # init here because we need the dendrites
        
        if self.dendriteType == "Standard":
            self.initHocTwoNeurons()
        elif self.dendriteType == "Passive":
            self.initHocTwoNeuronsPassive()
        elif self.dendriteType == "PassiveDensActiveSoma":
            self.initHocTwoNeuronsPassiveDenActiveSoma()

        self.explorationType = explorationType
        self.getDendDic()
        print self.dendDictA, self.dendDictA.keys()[0]

        dends = self.dendDictA.keys()

        resultsDelayVsPosition = pd.DataFrame()
        resultsDelayVsPositionDend = [0]*len(dends)

        for i, dend in enumerate(dends):
            # modify here to pass on function
            if explorationType == "delay":
                resultsDelayVsPositionDend[i] = self.generatePositionDelayData(dend, reverse = reverse)
            elif explorationType == "attenuation":
                resultsDelayVsPositionDend[i] = self.generatePositionAttenuationData(dend, reverse = reverse, mVstep = mVstep)
            elif explorationType == "2NeuronsDelay":
                resultsDelayVsPositionDend[i] = self.generatePositionDelayData2Neurons(dend)
            elif explorationType == "GJdelay":
                resultsDelayVsPositionDend[i] = self.generatePositionDelayGJonly(dend)
            elif explorationType == "APattenuation":
                resultsDelayVsPositionDend[i] = self.generateAPattentuation(dend)



        resultsTotal = resultsDelayVsPositionDend[0].copy()
        for each in range(len(resultsDelayVsPositionDend)-1):
            resultsTotal = resultsTotal.combineAdd(resultsDelayVsPositionDend[each + 1])

        resultsDelayVsPosition = resultsTotal
        resultsDelayVsPosition.index = map(float, resultsDelayVsPosition.index)
        resultsDelayVsPosition = resultsDelayVsPosition.sort_index()
        figname = explorationType + ID
        if reverse:figname = figname + "reverse"
        if explorationType == "delay":
            ylabel = "Delay (ms)"
        elif explorationType == "attenuation":
            ylabel = "Electrotonic distance"
        #plotDataNew(resultsDelayVsPosition, figname, ylabel)
        
        if saveData == True:
            store = pd.HDFStore(gt.getSystemPath() + 'Gap Junctions/data/pickledData/storeForAllNeuronData.h5')
            store[figname] = resultsDelayVsPosition
            store.close()
            cp.dump(resultsDelayVsPosition, open(gt.getSystemPath() + 'Gap Junctions/data/pickledData/{}.dat'.format(figname), "wb+"))
        return resultsDelayVsPosition
        #resultsDelayVsPosition.plot(style = "o")


    def exploreDataParallel(self, explorationType = "delay", reverse = False, mVstep = 1.0, saveData = False, ID = ''):

        self.initHoc()
#        from multiprocessing import Pool
#        p = Pool(self.Numberprocesses)

        import multiprocessing as mp
        pool = mp.Pool(mp.cpu_count() - 1 )

        dends = ['dend1a{}', 'dend2a{}', 'dend3a{}', 'dend4a{}']
        #dendDict = {'dend1a{}': h.dend1a}
        #dendDict = {'dend4a{}': h.dend4a}

        resultsDelayVsPosition = pd.DataFrame()
        resultsDelayVsPositionDend = [0]*len(dends)


        if explorationType == "delay":
            resultsDelayVsPositionDend = pool.map(partial(self.generatePositionDelayDataPar, reverse = reverse), dends)
        elif explorationType == "attenuation":
            resultsDelayVsPositionDend = pool.map(partial(self.generatePositionAttenuationDataPar, reverse = reverse, mVstep = mVstep), dends)
        elif explorationType == "2NeuronsDelay":
            resultsDelayVsPositionDend = pool.map(self.generatePositionDelayData2NeuronsPar, dends)



        resultsTotal = resultsDelayVsPositionDend[0].copy()
        for each in range(len(resultsDelayVsPositionDend)-1):
            resultsTotal = resultsTotal.combineAdd(resultsDelayVsPositionDend[each + 1])


        resultsDelayVsPosition = resultsTotal
        resultsDelayVsPosition.index = map(float, resultsDelayVsPosition.index)
        resultsDelayVsPosition = resultsDelayVsPosition.sort_index()
        figname = explorationType + ID
        if reverse:figname = figname + "reverse"
        if explorationType == "delay":
            ylabel = "Delay [ms]"
        elif explorationType == "attenuation":
            ylabel = "Electrotonic distance"
        #plotDataNew(resultsDelayVsPosition, figname, ylabel)
        if saveData:
            cp.dump(resultsDelayVsPosition, open('pickeledData/{}.dat'.format(figname), "wb+"))

        return resultsDelayVsPosition
        #resultsDelayVsPosition.plot(style = "o")

    def spreadMaximumPassive(self, x, tauMembrane = 30):
        return tauMembrane/4.0 * (np.sqrt(1 + 4 * x ** 2) - 1)


    def measureAP(self, data, leftMin = False, figName = "AP", limits = [-100, 60]):

        fig = plt.figure()
        ax = fig.add_subplot(111)
        colorsForPlot = ngj.tableau20[:8:2]
        ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])



        ax.set_title('AP soma ' + figName)
        ax.set_ylabel('Membrane potential (mV)')
        ax.set_xlabel('Time (ms)')
#        ax.fill_between(data.index, -100.0, data, facecolor = ngj.get_colorscheme(3, mirror=False)[1] )

        ax.plot(data.index, data, color = ngj.get_colorscheme(3, mirror=False)[0], linewidth = 2.0)

        neuronDataPre = data

        #ax.set_xlim([0,20])
        # only take min vale from the left side of the maximum
#        leftMin = data[data.index < data.idxmax()].min()
        if leftMin == False:
            dataKink = data[data.index < data.idxmax()]
            dataKink = dataKink[dataKink.index > 10.0]
            leftMinTime = self.measureKinkPlot(dataKink)
            leftMin = data[leftMinTime]
        else:
            leftMinTime = abs((data[data.index < data.idxmax()] - leftMin)).idxmin()

        print leftMin, leftMinTime

        amplitude = data.max() - leftMin

        HM = (amplitude/2.0 - data.max(), amplitude/2.0 + data.max())

        HM = data.max() - amplitude/2.0

        leftSideFW = (neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM).idxmin()

        neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()]
        leftSideFW = abs((neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM)).idxmin()
        rightSideFW = abs((neuronDataPre[neuronDataPre.index > neuronDataPre.idxmax()] - HM)).idxmin()

        xx = [leftSideFW, rightSideFW]
        yy = [neuronDataPre[leftSideFW], neuronDataPre[rightSideFW]]
        ax.plot(xx,yy, 'k', lw = 3.0)
        ax.plot([data.index[0], data.index[-1]], [data.max(), data.max()], 'k--')
        ax.plot([data.index[0], data.index[-1]], [leftMin, leftMin], 'k--')
        ax.plot(leftMinTime, leftMin, 'o')

        FWHM = rightSideFW - leftSideFW
        ax.annotate("FWHM {:04.2f} ms\nAmplitude {:04.2f} mV".format(FWHM, amplitude), (xx[1] + 2, yy[1] + 5))
#        figName = 'FWHMspikeSaragaConfigurationHOT'
        ax.set_ylim(limits)
        gt.saveFig(fig, figName)
        gt.saveFig(fig, figName, fileFormat = '.png')

        print "Amplitude {:04.2f}".format(amplitude)
        print "FWHM {:04.2f}".format(FWHM)
        plt.show()

        return amplitude
#        plt.close(fig)

    def main(self):
        return 0

    def measureAPwithoutPlot(self, data, figName = "AP"):

        leftMin = self.leftMin

        neuronDataPre = data

        if leftMin == False:
            dataKink = data[data.index < data.idxmax()]
            dataKink = dataKink[dataKink.index > 10.0]
            leftMinTime = self.measureKinkPlot(dataKink)
            leftMin = data[leftMinTime]
        else:
            leftMinTime = abs((data[data.index < data.idxmax()] - leftMin)).idxmin()

        print leftMin, leftMinTime

        amplitude = data.max() - leftMin

        HM = (amplitude/2.0 - data.max(), amplitude/2.0 + data.max())

        HM = data.max() - amplitude/2.0

        leftSideFW = (neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM).idxmin()

        neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()]
        leftSideFW = abs((neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM)).idxmin()
        rightSideFW = abs((neuronDataPre[neuronDataPre.index > neuronDataPre.idxmax()] - HM)).idxmin()

        FWHM = rightSideFW - leftSideFW

        print "Amplitude = ", amplitude
        print "FWHM = ", FWHM

        return amplitude
#        plt.close(fig)




#%% all the scripts
#from multiprocessing import Pool
if __name__ == "__main__":
    configSimulation = configSimulation()
    configSimulation.main()
    #%% generate and plot delay data, Vout
    SimulationConfig = configSimulation()
    configSimulation.exploreData(explorationType = "delay", reverse = False, mVstep = 1.0)

    #%% generate and plot reverse delay data, Vin
    SimulationConfig = configSimulation()
    configSimulation.exploreData(explorationType = "delay", reverse = True, mVstep = 1.0)

    #%% generate and plot attenuation date according to the new protocoll from
    # Saraga et al.
    configSimulation.exploreData(explorationType = "attenuation", reverse = False, mVstep = 1.0, saveData = True, ID = 'NewAttenuationProtocoll')
    #%% and reverse
    configSimulation.exploreData(explorationType = "attenuation", reverse = True, mVstep = 1.0, saveData = True, ID = 'NewAttenuationProtocoll')

    #%% compare two delays and delays reverse, aka, from soma (Vout) vs. to soma (Vin)
    data = {'DelayPlotreverse': 0, 'DelayPlot': 0 }
    for dataKey in data.keys():
        data[dataKey] = cp.load(open('pickeledData/{}.dat'.format(dataKey), "r"))

    fig = plt.figure(figsize = (7,7))
    ax = fig.add_subplot(111)
    #ax.set_color_cycle(ngj.tableau20)
    for dendrite in data['DelayPlotreverse'].columns:
        delayToSoma = data['DelayPlotreverse'].loc[:,dendrite][data['DelayPlotreverse'].loc[:,dendrite] >=0 ]
        delayFromSoma = data['DelayPlot'].loc[:,dendrite][data['DelayPlot'].loc[:,dendrite] >=0 ]

        ax.plot(np.array(delayToSoma), np.array(delayFromSoma), "-x", color = configSimulation.getColorForDifferentDendrites(dendrite))


    ax.plot([0, 2.5],[0, 2.5], "k", linewidth = 2.0)
    ax.set_ylim([0,2.5])
    ax.set_xlim([0,2.5])
    ax.set_ylabel("Delay from soma (ms)")
    ax.set_xlabel("Delay to soma (ms)")
    ax.legend(loc = 'upper left', fontsize = 12)
    gt.saveFig(fig, "comparisonToSomaVsFromSomaDelay")

    #%% Compare attenuation vs. delay reverse

    if 'data' in locals(): del data
    data = {'attenuationreverse': 0, 'DelayPlotreverse': 0 }
    for dataKey in data.keys():
        data[dataKey] = cp.load(open('pickeledData/{}.dat'.format(dataKey), "r"))

    fig = plt.figure(figsize = (7,7))
    ax = fig.add_subplot(111);
    #ax.set_color_cycle(ngj.tableau20)
    dataXXYY = [0, 0]
    for dendrite in data.values()[0].columns:

        dataXXYY[0] = data.values()[0].loc[:,dendrite][data.values()[0].loc[:,dendrite] >=0 ]
        dataXXYY[1] = data.values()[1].loc[:,dendrite][data.values()[0].loc[:,dendrite] >=0 ]

        ax.plot(np.array(dataXXYY[1]), np.array(dataXXYY[0]), "-x", color = configSimulation.getColorForDifferentDendrites(dendrite))
    xx = np.arange(0,1.2,0.01)
    xxN = np.arange(0,0.4,0.001)
    ax.plot(map(partial(configSimulation.spreadMaximumPassive, tauMembrane = 5), xx), xx, "k", linewidth = 2.0, label = "Maximum spread theory")
    ax.plot(map(partial(configSimulation.spreadMaximumPassive, tauMembrane = 30), xxN), xxN, "--k", linewidth = 2.0, label = "Maximum spread theory")
    #ax.plot([0, 2.5],[0, 2.5], "k", linewidth = 2.0)
#    ax.set_ylim([0,2.5])
#    ax.set_xlim([0,2.5])
    ax.set_ylabel("Electrotonic distance from soma")
    ax.set_xlabel("Delay from soma (ms)")
    ax.legend(loc = 'upper left', fontsize = 12)
    gt.saveFig(fig, "comparisonElectrotonicLenghtVsDelayReverseWithReal")

    #%% Compare attenuation vs. delay

    if 'data' in locals(): del data
    data = {'attenuation': 0, 'DelayPlot': 0 }
    for dataKey in data.keys():
        data[dataKey] = cp.load(open('pickeledData/{}.dat'.format(dataKey), "r"))

    fig = plt.figure(figsize = (7,7))
    ax = fig.add_subplot(111);
    #ax.set_color_cycle(ngj.tableau20)
    dataXXYY = [0, 0]
    for dendrite in data.values()[0].columns:

        dataXXYY[0] = data.values()[0].loc[:,dendrite][data.values()[0].loc[:,dendrite] >=0 ]
        dataXXYY[1] = data.values()[1].loc[:,dendrite][data.values()[0].loc[:,dendrite] >=0 ]

        ax.plot(np.array(dataXXYY[0]), np.array(dataXXYY[1]), "-x", color = configSimulation.getColorForDifferentDendrites(dendrite))

    xx = np.arange(0,0.15,0.001)
    xxN = np.arange(0,0.35,0.001)
    ax.plot(map(partial(configSimulation.spreadMaximumPassive, tauMembrane = 150), xx), xx, "k", linewidth = 2.0, label = "Maximum spread theory")
    ax.plot(map(partial(configSimulation.spreadMaximumPassive, tauMembrane = 30), xxN), xxN, "--k", linewidth = 2.0, label = "Maximum spread theory")
    #ax.plot([0, 2.5],[0, 2.5], )
#    ax.set_ylim([0,2.5])
#    ax.set_xlim([0,2.5])
    ax.set_ylabel("Electrotonic distance to soma")
    ax.set_xlabel("Delay to soma (ms)")
    ax.legend(loc = 'upper left', fontsize = 12)
    gt.saveFig(fig, "comparisonElectrotonicLenghtVsDelayWithReal")

    #%% generate
    configSimulation = configSimulation()
    configSimulation.resistanceGapJunction = 1000
    configSimulation.nsegLeaveOut = 1
    configSimulation.exploreData(explorationType = "2NeuronsDelay2000", saveData = True)


    #%% The total delay of the spiking signal is the sum of the delay from the soma
    # plus the delay to the soma plus the delay that is generated at the gap junction.
    # Now, we can compoare the delay caused by the pure propagation of the signal with the
    # delay that is caused by the gap junction.
    # So, we want to compare the data generated before to the data generated now. Therefore,
    # we have to match the locations and the configurations.

    if 'data' in locals(): del data
    data = {'delayIClampdualreverse': 0, 'delayIClampdual': 0 }
    for dataKey in data.keys():
        data[dataKey] = cp.load(open('pickeledData/{}.dat'.format(dataKey), "r"))

    summedDelay = data['delayIClampdualreverse'] + data['delayIClampdual']
    loadName = '2NeuronsDelayfull'
    data2NeuronDelay = cp.load(open('pickeledData/{}.dat'.format(loadName), "r"))

    configSimulation1 = configSimulation()
    fig, ax = configSimulation1.plotDataNew(summedDelay, 'comparisonSummedDelay', ylabel = "Delay [ms]")
    fig, ax = configSimulation1.plotDataNew(data2NeuronDelay, 'comparisonSummedDelayVsTotalDelay', ylabel = "Delay [ms]", ax = ax, fig = fig)
    fig, ax = configSimulation1.plotDataNew(data2NeuronDelay, 'comparisonTotalDelay', ylabel = "Delay [ms]",  plotstyle = "-x")



    #%% Test the new protocol for the data generation. First test the trace generation
    # and then calculate the new plots from it




