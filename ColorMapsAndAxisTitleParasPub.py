#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 11:42:49 2017

@author: holzbecher

This file just contains some colors and preferences for the plots. 
Also the labels.
"""

#from matplotlib.colors import LinearSegmentedColormap


# colors for gap junctions and without gap junctions

fullPageFigSize = (6.88, 10.14)
halfPageFigSize = (6.88, 5.07)
twoThirdRatio = (6.88, 4.59)
oneTwoRatio = (6.88, 3.44)
flat = (6.88, 2.0)

halfhalfPageFigSize = (3.44, 5.07)


colorWithGJ = '#4EB628'
colorWithOutGJ = '0.6'
colorWithStrongGJ = "#255613"

# Colors for network properties

labelNetworkFrequency = 'Network frequency (Hz)'
labelNetworkFrequencyShort = 'Network\nfrequency (Hz)'
colorNetworkFrequency = '#4073EB'
#cmapNetworkFrequency = LinearSegmentedColormap.from_list('mycmap', [(0, 'white'),
#                                                    (1, colorNetworkFrequency )])
cmapNetworkFrequency = "Blues"


labelSynchrony = 'Synchrony Index'
labelSynchronyShort = 'Synchrony\nIndex'
colorSynchrony = '#4EB628'
#cmapSynchrony = LinearSegmentedColormap.from_list('mycmap', [(0, 'white'),
#                                                    (1, colorSynchrony)])
cmapSynchrony = 'Greens'


labelFiringRate2lines = 'Firing\nrate (1/s)'
labelFiringRate = 'Firing rate (1/s)'
labelFiringRateShort = 'Firing\nrate (1/s)'
colorFiringRate = '#ff7f0e'
#cmapFiringRate = LinearSegmentedColormap.from_list('mycmap', [(0, 'white'),
#                                                    (0.5, colorFiringRate),
#                                                    (1, 'black')])
cmapFiringRate = 'Oranges'

# Network oscillation 

labelOscillationStrength = 'Oscillation strength'
labelOscillationStrengthShort = 'Oscillation\nstrength'
colorOscillationStrength= '#d62728'

#['#4EB628', 'r', '#4073EB', '#F3B841']


# Colors for multicompartment models

# Color for inhibition and excitation

excitatoryColor = '#d62728'
inhibitionColor = '#1f77b4'

# general Labels

labelTimeMs = "Time (ms)"

labelPassive = 'Passive conductance (nS)'
labelActive = 'Active spike (mV)'
labelDelay = 'Delay (ms)'
labelConnectionProbability = 'Connection Probability'
labelConnectionProbabilityShort = 'Con. Prob.'

labelPotential = "Membrane\npotential (mV)"

