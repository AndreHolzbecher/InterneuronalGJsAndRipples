# !/usr/bin/env python
# -*- coding: utf-8 -*-

# =============================================================================
# These are the script that contain 
# most of the brain code and are the core of the simulations
# =============================================================================


import brian as bb
from brian import ms, mV, mA, second, siemens, pF, amp, nA, volt
from matplotlib import pyplot as pp
import brian.library.synapses as syns
import numpy as np
import random 
import gapAnalyticsTempPub as gt
from datetime import date

bb.globalprefs.set_global_preferences(usecodegen=True, usenewpropagate=True,usestdp=True)






class simout:
    
    def __init__(self,fname=[]):
        self.date = []
        self.simtime =[]
        self.com = '...no comments...'
        self.W=[]
        self.delays=[]
        self.Gii = []
        self.Gee =[]
        self.Gei =[]
        self.Gie= []
        self.Git=[]
        self.Iii = []
        self.Iee =[]
        self.Iei =[]
        self.Iie= []
        self.Iit=[]
        self.I0=[]
        self.Vm=[]
        self.v_thres=[]
        self.times_rate=[]
        self.times_cond=[]
        self.rate_poi=[]
        self.rate_burst=[]
        self.rate_inh=[]
        self.rate_bin=[]
        self.Ni=[]
        self.spiketimes=[]
        self.spiketimes2=[]
        self.spiketimes_burst=[]
        self.spiketimes_poi=[]
        if fname:
	  self.load(fname)


    def current_stats(self):
        Gi_mean=np.mean(self.Gii,0)*1E9+self.Git
        Gi_std=np.std(self.Gii,0)*1E9
        Gei_mean=np.mean(self.Gei,0)*1E9
        Gei_std=np.std(self.Gei,0)*1E9
        Ie_mean=np.mean(self.Iei,0)*1E9
        Ie_std=np.std(self.Iei,0)*1E9
        Ii_mean=np.mean(self.Iii,0)*1E9
        Ii_std=np.std(self.Iii,0)*1E9
        I0_mean=np.mean(self.I0,0)*1E9
        Inet=self.Iei+self.Iii # amps
        Inet_mean=np.mean(Inet,0)*1E9
        Inet_std=np.std(Inet,0)*1E9

	return Gi_mean,Gi_std,Gei_mean,Gei_std,Ie_mean,Ie_std,Ii_mean,Ii_std,I0_mean,Inet,Inet_mean,Inet_std

    def smooth_rate(self,sufix='',t_0=0.0,t_1=[],sigma_krnl=0):
        spt_name='spiketimes'+sufix
        if not t_1: t_1=self.simtime
        sptimes=self.__dict__[spt_name][:,0]
        sptimes=sptimes[np.where((sptimes>=t_0)&(sptimes<=t_1))]*100000 #convert time to sample number within range
        sptrain=np.zeros(int((t_1)*100000)) #(int(max(sptimes))+1)
        x=np.array(range(len(sptrain)))*1.0 #make it real
        for i in range(len(sptimes)):
	     if sigma_krnl:
              sptrain=sptrain + gt.gaussfunc(x,1/(np.sqrt(2*np.pi)*sigma_krnl),sptimes[i],sigma_krnl,0)
	     else:
              sptrain[int(sptimes[i])] = sptrain[int(sptimes[i])] + 1.0

        return sptrain[int((t_0)*100000) : int((t_1)*100000+1)] #sptrain[min(sptimes):max(sptimes)]

    def net_freq(self, freq_min=30.0, sigma_krnl=0, scaleFactor = 1):
        # returns: Freq,Sync,
        # Power spectral density
        spk_train=gt.smooth_rate(self.spiketimes, sigma_krnl=sigma_krnl)
        ss=gt.autocorrNorm(scaleFactor*spk_train)
        (frq,Y)=gt.Spectrum(ss,100000.0)
        frqaux=frq[frq>freq_min]#frq[25:125]
        yaux=abs(Y[frq>freq_min]) #changed from freq>10 11.3.14
        fc=frqaux[yaux.argmax()]

        #Coherence
        si2=np.sqrt(yaux.max()/abs(Y[0]))
        return fc,si2,frq,Y

    def spike_stats(self,t_0=0.0,t_1=[]):
	#spiking statistics: fr(array with firing rates),cv(array with coefs of variation),frq array,Fourier Transform
	if not t_1: t_1=self.simtime
	ISI_mean,ISI_std,ISI_acum,FR=[],[],[],[]
	for cell_no in range(self.Ni):
	    spikes_cell=self.spiketimes[np.where(self.spiketimes[:,1]==cell_no)][:,0]
	    spikes_cell=spikes_cell[np.where((spikes_cell>=t_0)&(spikes_cell<=t_1))]
	    isi=np.diff(spikes_cell)
	    ISI_mean=np.concatenate((ISI_mean,np.array([isi.mean()])))
	    ISI_std=np.concatenate((ISI_std,np.array([isi.std()])))
	    ISI_acum=np.concatenate((ISI_acum,isi))
	    FR=np.concatenate((FR,np.array([len(spikes_cell)])))
	FR=FR/(t_1-t_0)
	CV=ISI_std/ISI_mean
	return FR,CV[np.isfinite(CV)],ISI_mean,ISI_std,ISI_acum

class model:
    def __init__(self,pyr=0):
        
        self.sim_id = 0
 # Input
        self.tot_f_poisson = 4000 # Poisson input received by every inhibitory neuron
        self.f_poisson_sigma = 0.0 # Sigma of the average poisson rate of the single neurons in fraction of the average rate
        self.tot_f_poisson_e = 5000
        self.n_synapses= 800 # Gives the number of independent inputs to the inhibitory neurons
        self.n_syn_e = 800
        self.spr_input = 0.0 #0.2 #0.05 # Give the number of how much of this input is shared
        # Injected current
        self.I_mean=0 #10
        self.I_std=0    #2
        # CA3 Burst input
        self.sigma_burst=0.0 #0.006
        #probe=False,sim_time=0.3,rate_bin=1.0,n_rec=50,
        # Spotlight fraction of interneurons receive input
        self.spotLight = 0.0 #Number of neurons receiving excitation
 # Network
        self.Ni=200 # This is the standard size for the network of consideration. It resembles the amout of neurons in a slice.
        self.Ne=12000 # Excitatory neuron population
        
        self.spr_ii = 0.2 # Connectivity for inh.-inh. synapse
        self.spr_ee = 0.01
        self.spr_ei=0.1
        self.spr_ie=0.2

    # n_rec = 50 : number of neurons recorded (both in main pop and probe)
# Synapses
        # GABA
        self.g_ii = 5.0
        self.g_iit = 0.0

        self.tl_ii = 1.0
        self.tr_ii = 0.45
        self.td_ii = 1.2

        self.tl_ie = 1.0
        self.tr_ie = 0.5
        self.td_ie = 5.0

        # AMPA
        self.g_burst = 1.0
        self.g_ee = pyr*0.89#0.19
        self.g_ei = pyr*1.0#0.3
        self.g_ie = pyr*2.5#5.27nS (Bartos, 2002)
        self.g_pe = pyr*0.17#0.25 #1.6*0.25
        self.g_pi = 1.0
        self.tl_ee = 1.0
        self.tr_ee = 0.5
        self.td_ee = 1.8 #2.0
        self.tl_ei = 1.0
        self.tr_ei = 0.5
        self.td_ei = 2.0

        #Electrical
        self.gj_pconnect = 1.0 #0.5
        self.gj_cluster_size = 20
        self.gj_passive = 0.5
        self.gj_spike = 0.1 #19.10.2015 Galarreta and Hestrin (2001)
        self.gj_delay = 0.0 #Delay of gap junctions

        self.gj_passive_Sigma = 0.0 #fraction of gj_passive
        self.gj_spike_Sigma = 0.0 #fraction of gj_spike

# Membrane
        # PV+BC
        self.initRandom = True  #sets the intial voltage to rnd voltages between

                                # threshold and rest

        self.v_rest = -65.0 #-70. -64@(Buhl,1996)   -65@(Wang & Buzsaki,1996)
        self.v_thres = -52.0 #-52 -55@(Buhl, 1996)  -52@(Wang & Buzsaki,1996)
        self.v_reset = -67.0#-59  -69@(Buhl, 1996)  -67@(Wang & Buzsaki,1996)
        self.v_exc = 0.0
        self.v_inh = -75.0  #-70. -75@(Buhl,1995)
        self.g_l = 10.0 #10  #32nS @(Buhl,1996)
        self.C_i = 100.0 #100 pF

        # Pyramids
        self.v_rest_e = -64.0 #-70.
        self.v_thres_e = -52.0 #-51
        self.v_reset_e = -58.0#-59
        self.v_exc_e = 0.0
        self.v_inh_e = -70.0  #-70.
        self.C_e = 200.0 # 360 Estimated from 1 uF/cm-2 and 36000 um2
        self.tau_ref = 2.0 # *0.5 used for inhibitory neurons
        self.g_le =11.0 #10.0
        self.eif_tau=3.0 # Exponential IF
# Output
        self.simtime = []
        self.rate_bin = []
        self.MPe = []
        self.MPi = []
        self.MPp = []
        self.MP_burst = []
        self.rate_inh = []
        self.rate_exc = []
        self.rate_poi = []
        self.rate_burst = []
        self.Mvi = []
        self.Mve = []
        self.Mgi = []
        self.Mge = []
        self.Mgie = []
        self.Mgei = []
        self.lfp = []
        #self.Pi = []
        self.Cii = []
        self.n_rec=[]


        # for passing back excitatory spikeTimes 
        
        self.spiketimes_burst = []
        self.spiketimes_poi = []


    def fi(self):
        
        I=np.arange(0,400,50)
        Tm=(self.C_i/self.g_l)/1000.0
        V= 0.001*(I/self.g_l + self.v_rest)
        fi_i=(Tm*np.log((V - 0.001*self.v_reset)/(V - 0.001*self.v_thres)))**-1
        pp.plot(I,fi_i)
        pp.grid()
        pp.title('Theoretical FI curve')
        pp.ylabel('Firing rate (spikes/s)')
        pp.xlabel('Injected current (pA)')
        pp.show()

# Test the electrical an chemical synapses
    def testsyn2(self,I_inj_e,I_inj_i,pulse_len=5.0,spk_delay=80.0,tlim=[0,300],legend=False, Gii10 = False):
        bb.clock.reinit_default_clock()
        bb.clock.defaultclock.dt=.01*ms
          # Syanpses
        AMPA_pyr=syns.biexp_synapse(input='ge_in',tau1=self.td_ee*ms,tau2=self.tr_ee*ms,unit=siemens,output='ge') #tau1=decay
        AMPA_int=syns.biexp_synapse(input='ge_in',tau1=self.td_ei*ms,tau2=self.tr_ei*ms,unit=siemens,output='ge')

        GABA_pyr=syns.biexp_synapse(input='gi_in',tau1=self.td_ie*ms,tau2=self.tr_ie*ms,unit=siemens,output='gi')
        GABA_int=syns.biexp_synapse(input='gi_in',tau1=self.td_ii*ms,tau2=self.tr_ii*ms,unit=siemens,output='gi')

    # Membrane equations
        g_l=self.g_l*1E-9*siemens
        g_le=self.g_le*1E-9*siemens
        v_rest_e=self.v_rest_e*mV
        v_exc_e=self.v_exc_e*mV
        v_inh_e=self.v_inh_e*mV

        delay_steps=round(spk_delay/pulse_len)
        first_step=2 #round(20.0/pulse_len)
        I_ext_i=bb.TimedArray(I_inj_i*gt.SyncGroupInput(2,1,int(first_step))*nA, dt=pulse_len*ms)
        I_ext_e=bb.TimedArray(I_inj_e*gt.SyncGroupInput(2,1, int(first_step+delay_steps))*nA, dt=pulse_len*ms)
        eqs=bb.MembraneEquation(C=self.C_e*pF,vm='v')+bb.Current('I=g_le*(v_rest_e-v) + ge*(v_exc_e-v) + gi*(v_inh_e-v)  + I_ext_e(t):amp',current_name='I')
        eqs+=AMPA_pyr
        eqs+=GABA_pyr

        #g_l=self.g_l*1E-9*siemens
        v_rest=self.v_rest*mV
        v_exc=self.v_exc*mV
        v_inh=self.v_inh*mV
        #g_gap=gj_passive*1E-9*siemens
        tau_gap=3*ms
        #tau2=1*ms
        #I_ext=bb.TimedArray(GaussMatrix(100,self.Ni,self.I_mean,self.I_std)*nA, dt=0.5*ms)
        #I_ext=bb.TimedArray(I_mean*SyncGroupInput(Ni,70,100)*nA, dt=0.5*ms)
        #v_rest=bb.TimedArray(GaussMatrix(100,Ni,-65,10)*mV, dt=0.5*ms)
        eqs_i=bb.MembraneEquation(C=self.C_i*pF,vm='v')+bb.Current('Ii=g_l*(v_rest-v) + ge*(v_exc-v) + gi*(v_inh-v) +I_ext_i(t):amp',current_name='Ii')
        eqs_i+=bb.Current('I_gap:amp',current_name='I_gap')

        #C=self.C_i*pF
        #eqs_i=bb.Equations('dvi/dt=(g_l*(v_rest-vi) + ge*(v_exc-vi) + gi*(v_inh-vi) +I_ext_i(t))/C:volt')
        # - g_gap*(u-v)
        #eqs_i = '''
        #dv/dt=(g_l*(v_rest-v) + ge*(v_exc-v) + gi*(v_inh-v) + I_gap + I_ext_i(t))/C:volt
        #I_gap:amp
        #I_gap=g_gap*(v-u):amp
        #du/dt=(v_rest-u)/tau_gap : volt
        #x:volt
        #'''
        #eqs_i+=bb.Current('g_gap*(u-vi):amp')
        #eqs_i+=bb.Equations('''du/dt=(v_rest-u)/tau : volt''') # input from other neurons)
        eqs_i+=AMPA_int
        eqs_i+=GABA_int

    # Neuron groups and initialization
        Pe = bb.NeuronGroup(2, eqs, threshold=self.v_thres_e*mV, reset=self.v_reset_e*mV, refractory=self.tau_ref*ms)
        Pi = bb.NeuronGroup(2, eqs_i, threshold=self.v_thres*mV, reset=self.v_reset*mV, refractory=0.5*self.tau_ref*ms)
        #Pe.I_ext=np.array([0,0])*nA
        Pi.I_ext=np.array([0,0])*nA
        Pe.v = v_rest_e*np.array([1,1])
        Pi.v = v_rest*np.array([1,1])
        PeS = Pe.subgroup(1)
        PiS = Pi.subgroup(1)
        PeT = Pe.subgroup(1)
        PiT = Pi.subgroup(1)

        spiketimes=[(0,200*ms)]
        input=bb.SpikeGeneratorGroup(1,spiketimes)

        Cee = bb.Connection(PeS, PeT, 'ge_in', weight=self.g_ee*1E-9*siemens, sparseness=1.0,delay=self.tl_ee*ms)
        Cie = bb.Connection(PeS, PiT, 'ge_in', weight=self.g_ei*1E-9*siemens, sparseness=1.0,delay=self.tl_ei*ms)
        Cei = bb.Connection(PiS, PeT, 'gi_in', weight=self.g_ie*1E-9*siemens, sparseness=1.0,delay=self.tl_ie*ms)
        Cii = bb.Connection(PiS, PiT, 'gi_in', weight=self.g_ii*1E-9*siemens, sparseness=1.0,delay=self.tl_ii*ms)
        Cpi=  bb.Connection(input, PiT, 'ge_in', weight=self.g_pi*1E-9*siemens, sparseness=1.0,delay=self.tl_ei*ms)
        Cpe=  bb.Connection(input, PeT, 'ge_in', weight=self.g_pe*1E-9*siemens, sparseness=1.0,delay=self.tl_ee*ms)

        # Passive conductive connection
        Gii=bb.Synapses(Pi,Pi,model='''g_gap:nS # gap junction conductance
                                         b_gap:mV # spike component
                                         I_gap=g_gap*(v_pre-v_post): amp'''
                                 ,pre='''v_post+=b_gap''')
        Pi.I_gap=Gii.I_gap

        Gii[0,1]= True
        if Gii10:
            Gii[1,0]= True
        Gii.g_gap=self.gj_passive*1E-9*siemens
        Gii.b_gap=self.gj_spike*mV
        Gii.delay = self.gj_delay*ms


        MIi = bb.StateMonitor(Pi,'Ii',record=[0,1])
        MIgap=bb.StateMonitor(Pi,'I_gap',record=[0,1])
        Mve = bb.StateMonitor(Pe, 'v', record=[0,1])
        Mvi = bb.StateMonitor(Pi, 'v', record=[0,1])
        Mgee = bb.StateMonitor(Pe, 'ge', record=[0,1])
        Mgii = bb.StateMonitor(Pi, 'gi', record=[0,1])
        Mgei = bb.StateMonitor(Pi, 'ge', record=[0,1])
        Mgie = bb.StateMonitor(Pe, 'gi', record=[0,1])
        MPeS = bb.SpikeMonitor(PeS)
        MPiS = bb.SpikeMonitor(PiS)
        network = bb.Network(Pe,Pi,Cee, Cie, Cei, Cii, input,Cpi,Cpe, Mve,Mvi,Mgee,Mgii,Mgei,Mgie,MPeS,MPiS,MIgap,MIi, Gii)



        network.run(0.05*second)

        return  (Mvi, Mve, MPiS, MPeS, MIgap, MIi)

    def simulate(self,sim_time,rate_bin=0.5,probe=False,n_rec=50,syn_norm=False, longSim = False):
        self.Cii = []
        bb.clock.reinit_default_clock()
        bb.clock.defaultclock.dt=.01*ms
        sim_time=sim_time*second

        if syn_norm:
            tm_pyr=self.C_e/self.g_le
            tm_int=self.C_i/self.g_l
            s_ee=(tm_pyr/self.td_ee)*(self.tr_ee/self.td_ee)**(self.tr_ee/(self.td_ee-self.tr_ee))
            s_ei=(tm_int/self.td_ei)*(self.tr_ei/self.td_ei)**(self.tr_ei/(self.td_ei-self.tr_ei))
            s_ie=(tm_pyr/self.td_ie)*(self.tr_ie/self.td_ie)**(self.tr_ie/(self.td_ie-self.tr_ie))
            s_ii=(tm_int/self.td_ii)*(self.tr_ii/self.td_ii)**(self.tr_ii/(self.td_ii-self.tr_ii))
            s_pi=(tm_int/self.td_ee)*(self.tr_ee/self.td_ee)**(self.tr_ee/(self.td_ee-self.tr_ee))

            self.g_ee=self.g_ee*s_ee
            self.g_ei=self.g_ei*s_ei
            self.g_ie=self.g_ie*s_ie
            self.g_ii=self.g_ii*s_ii
            self.g_pe=self.g_pe*s_ee
            self.g_pi=self.g_pi*s_pi


        print 'Total input Poisson frequency: ',self.tot_f_poisson
        print '                     Cond.[nS]\tLatency [ms]\tRise [ms]\tDecay [ms]'
        print '  '
        print 'AMPA on pyramids        ',self.g_ee,'\t  ',self.tl_ee,'\t\t',self.tr_ee,'\t\t',self.td_ee
        print 'AMPA on interneurons    ',self.g_ei,'\t  ',self.tl_ei,'\t\t',self.tr_ei,'\t\t',self.td_ei
        print 'GABA on pyramids        ',self.g_ie,'\t  ',self.tl_ie,'\t\t',self.tr_ie,'\t\t',self.td_ie
        print 'GABA on interneurons    ',self.g_ii,'\t  ',self.tl_ii,'\t\t',self.tr_ii,'\t\t',self.td_ii
        print 'External on pyramids    ',self.g_pe,'\t  ',self.tl_ee,'\t\t',self.tr_ee,'\t\t',self.td_ee
        print 'External on intern.     ',self.g_pi,'\t  ',self.tl_ei,'\t\t',self.tr_ei,'\t\t',self.td_ei
        print 'Gap junctions   connect = ',self.gj_pconnect,'\tcluster = ',self.gj_cluster_size,'\tpassive = ',self.gj_passive,'\tspike = ',self.gj_spike

    # Synapses
        AMPA_pyr=syns.biexp_synapse(input='ge_in',tau1=self.td_ee*ms,tau2=self.tr_ee*ms,unit=siemens,output='ge') #tau1=decay
        AMPA_int=syns.biexp_synapse(input='ge_in',tau1=self.td_ei*ms,tau2=self.tr_ei*ms,unit=siemens,output='ge')

        GABA_pyr=syns.biexp_synapse(input='gi_in',tau1=self.td_ie*ms,tau2=self.tr_ie*ms,unit=siemens,output='gi')
        GABA_int=syns.biexp_synapse(input='gi_in',tau1=self.td_ii*ms,tau2=self.tr_ii*ms,unit=siemens,output='gi')

    # Membrane equations
        g_l=self.g_l*1E-9*siemens
        g_le=self.g_le*1E-9*siemens
        gi_tonic=self.g_iit*1E-9*siemens
        v_rest_e=self.v_rest_e*mV
        v_exc_e=self.v_exc_e*mV
        v_inh_e=self.v_inh_e*mV

        eqs=bb.MembraneEquation(C=self.C_e*pF,vm='v')+bb.Current('I=g_le*(v_rest_e-v) + ge*(v_exc_e-v) + gi*(v_inh_e-v):amp',current_name='I')
        eqs+=AMPA_pyr
        eqs+=GABA_pyr

        v_rest=self.v_rest*mV
        v_exc=self.v_exc*mV
        v_inh=self.v_inh*mV
        
        
        I_extInput = bb.TimedArray(gt.GaussMatrix( int(float(sim_time) / 0.005), self.Ni, self.I_mean, self.I_std * self.I_mean)*nA, dt=5.0*ms)
#        I_extInput = np.array([abs(random.gauss(self.I_mean, self.I_mean * self.I_std)) for x in range(int(self.Ni))]) * nA

        eqs_i= bb.MembraneEquation(C=self.C_i*pF,vm='v') + bb.Current('Ii=g_l*(v_rest-v) +  gi_tonic*(v_inh-v):amp',current_name='Ii')
        eqs_i += bb.Current('I_ext = I_extInput(t):amp', current_name='I_ext')
        eqs_i += bb.Current('I_ii = gi*(v_inh-v):amp',current_name='I_ii')
        eqs_i += bb.Current('I_pi = ge*(v_exc-v):amp',current_name='I_pi')
        eqs_i += bb.Current('I_gap:amp',current_name='I_gap')
        eqs_i += AMPA_int
        eqs_i += GABA_int

    # Neuron groups and initialization

        Pe = bb.NeuronGroup(self.Ne, eqs, threshold=self.v_thres_e*mV, reset=self.v_reset_e*mV, refractory=self.tau_ref*ms)
        Pi = bb.NeuronGroup(self.Ni, eqs_i, threshold=self.v_thres*mV, reset=self.v_reset*mV, refractory=0.5*self.tau_ref*ms)

        Pe.v = self.v_rest_e*np.ones(self.Ne)*mV

        if self.initRandom == True:
            Pi.v = np.array([random.randrange(self.v_rest, stop=self.v_thres) for i in range(self.Ni)])*mV
        else:
            Pi.v = self.v_rest*np.ones(self.Ni)*mV

    # Connections: create a basic network with 2 rate monitors
        Cee = bb.Connection(Pe, Pe, 'ge_in', weight=self.g_ee*1E-9*siemens, sparseness=self.spr_ee,delay=self.tl_ee*ms)
        Cie = bb.Connection(Pe, Pi, 'ge_in', weight=self.g_ei*1E-9*siemens, sparseness=self.spr_ei,delay=self.tl_ei*ms)
        Cei = bb.Connection(Pi, Pe, 'gi_in', weight=self.g_ie*1E-9*siemens, sparseness=self.spr_ie,delay=self.tl_ie*ms)
        Cii = [] #Reset Cii before every run...
        #if not self.Cii:
        if type(self.tl_ii) is list:
	    delay_val=(self.tl_ii[0]*ms,self.tl_ii[1]*ms)
        else:
	    delay_val=self.tl_ii*ms

        if self.Cii:
	    Cii = bb.DelayConnection(Pi, Pi, 'gi_in')
	    for j in range(self.Ni):
	        for k in range(self.Ni):
		    if self.Cii[j,k]:
                        Cii[j,k]=self.Cii[j,k]
                        Cii.delay[j,k]=self.Cii.delay[j,k]
        else:
            Cii = bb.Connection(Pi, Pi, 'gi_in', weight=self.g_ii*1E-9*siemens, sparseness=self.spr_ii,delay=delay_val)#self.tl_ii*ms)

        rate_exc = bb.PopulationRateMonitor(Pe,bin=rate_bin*ms)
        rate_inh = bb.PopulationRateMonitor(Pi,bin=rate_bin*ms)
        in_spikes = bb.SpikeCounter(Pi)

    # Electrical synapses
        if self.gj_pconnect:
            Gii=bb.Synapses(Pi, model='''g_gap:nS # gap junction conductance
                                            b_gap:mV # spike component
                                            I_gap=g_gap*(v_pre-v_post): amp'''
                                    ,pre='''v_post+=b_gap''')


            #Connectivity
            for i in range(self.Ni):
                for j in range(i+1,i+1+self.gj_cluster_size):
                    k=j%self.Ni
                    if (random.random()<self.gj_pconnect):
                        Gii[i,k]=True
                        Gii[k,i]=True

                        if self.gj_passive_Sigma:
                            passive_value = abs(random.gauss(self.gj_passive,self.gj_passive*self.gj_passive_Sigma))
                            Gii.g_gap[i,k] = passive_value*1E-9*siemens
                            Gii.g_gap[k,i] = passive_value*1E-9*siemens

                        if self.gj_spike_Sigma:
                            spike_value = abs(random.gauss(self.gj_spike,self.gj_spike*self.gj_spike_Sigma))
                            Gii.b_gap[i,k] = spike_value*mV
                            Gii.b_gap[k,i] = spike_value*mV

            if not self.gj_passive_Sigma:
                Gii.g_gap=self.gj_passive*1E-9*siemens

            if not self.gj_spike_Sigma:
                Gii.b_gap=self.gj_spike*mV

            Gii.delay = self.gj_delay*ms

            # Frist transfer I_gap to the neurongroup...than generate the whole network from it
            Pi.I_gap=Gii.I_gap



        if self.gj_pconnect:
            network = bb.Network(Pe,Pi,Cee, Cie, Cei, Cii, rate_exc, rate_inh, in_spikes, Gii)
        else:
            network = bb.Network(Pe,Pi,Cee, Cie, Cei, Cii, rate_exc, rate_inh, in_spikes)

    # ADDS INPUT
        [MPp,MP_burst,rate_poi,rate_burst]=[[],[],[],[]]

        f_poisson_single = float(self.tot_f_poisson)/float(self.n_synapses)
        f_poisson_e = self.tot_f_poisson_e/self.n_syn_e


        if self.spotLight == 0:
            if self.spr_input == 0:

                if self.f_poisson_sigma == 0.0:

                    f_poisson = f_poisson_single
                    # Not possible to give the Poissoninput multiple firing rates for the Poisson process.
                    # Since we will consider weak correlations not necessary right now.
                    #if self.f_poisson_sigma != 0.0: f_poisson =  np.array([gauss(f_poisson_single,self.f_poisson_sigma) for x in range(int(self.n_synapses))])
                    #else: f_poisson = float(self.tot_f_poisson)/float(self.n_synapses)
                    p_input=bb.PoissonInput(Pi, self.n_synapses, f_poisson, self.g_pi*1E-9*siemens, 'ge_in')
                    p_inpute=bb.PoissonInput(Pe, self.n_syn_e, f_poisson_e, self.g_pe*1E-9*siemens, 'ge_in')
                    network.add(p_input,p_inpute)

                else:

                    f_poisson =  np.array([abs(random.gauss(f_poisson_single,f_poisson_single*self.f_poisson_sigma)) for x in range(int(self.Ni))])
                    p_input = [0]*self.Ni

                    for inhibitoryNeurons in range(self.Ni):

                        p_input[inhibitoryNeurons]=bb.PoissonInput(Pi.subgroup(1), self.n_synapses, f_poisson[inhibitoryNeurons], self.g_pi*1E-9*siemens, 'ge_in')
                        network.add(p_input[inhibitoryNeurons])

                    print self.n_synapses,'Poisson synapses per neuron @',f_poisson,'spikes/sec', 'w/ sigma Gaussian = ',self.f_poisson_sigma*f_poisson_single



            else:

                p_inpute=bb.PoissonInput(Pe, self.n_syn_e, f_poisson_e, self.g_pe*1E-9*siemens, 'ge_in')# added 12.11.2013
                #N_poisson = self.n_synapses/self.spr_input # Number of Poisson units determined by sparseness
                #  /!\ Currently during the burst, the poisson rate is provided via the same number of synapses as the burst /!\

                N_poisson = self.n_synapses/self.spr_input
                # abs of Gaussian distribution...quick and dirty away to get around negative values for the firing rate
                if self.f_poisson_sigma != 0.0: f_poisson =  np.array([abs(random.gauss(f_poisson_single,self.f_poisson_sigma)) for x in range(int(N_poisson))])
                else: f_poisson = f_poisson_single
                print N_poisson, f_poisson
                
                
                if self.spiketimes_poi:
                    print "Using the old times for Poisson excitation"
                    P_poisson =  bb.SpikeGeneratorGroup((self.spiketimes_poi[-1][0] + 1), self.spiketimes_poi)
                else:
                    print "Generating new times for Poisson excitation"
                    P_poisson = bb.PoissonGroup(int(N_poisson), f_poisson)
                
                
                print N_poisson,'Poisson units on average @',f_poisson_single,'spikes/sec', 'w/ sigma Gaussian = ',self.f_poisson_sigma
                #Cep = bb.Connection(P_poisson, Pe,'ge_in', weight=g_pe, sparseness=spr_input)
                #network.add(P_poisson,Cep)
                
                Cip = bb.Connection(P_poisson,Pi,'ge_in',weight=self.g_pi*1E-9*siemens, sparseness=self.spr_input)

                network.add(P_poisson,Cip,p_inpute)#p_input_e added
                MPp = bb.SpikeMonitor(P_poisson)
                network.add(MPp)
                rate_poi = bb.PopulationRateMonitor(P_poisson,bin=rate_bin*ms)
                network.add(rate_poi)


        else:
            f_poisson = f_poisson_single
            PiSW = Pi.subgroup(self.spotLight)
#            p_input=bb.PoissonInput(PiSW, self.n_synapses, f_poisson, self.g_pi*1E-9*siemens, 'ge_in')
            if self.spr_input == 0:
                
                N_poisson = self.n_synapses*self.spotLight
                P_poisson = bb.PoissonGroup(int(N_poisson), f_poisson_single)
                rate_poi = bb.PopulationRateMonitor(P_poisson,bin=rate_bin*ms)
                Cip = [[]]*self.spotLight
                SubGroupsPi = [[]]*self.spotLight
                for i in range(self.spotLight):
                                      
                    SubGroupsPi[i] = PiSW.subgroup(1)                        
                    Cip[i] = bb.Connection(P_poisson.subgroup(self.n_synapses), SubGroupsPi[i], 'ge_in', weight=self.g_pi*1E-9*siemens, sparseness=1.0)
                    
            else:             
                
                
                N_poisson = self.n_synapses/self.spr_input
                
                if self.spiketimes_poi:
                    print "Using the old times for Burst excitation"
                    P_poisson =  bb.SpikeGeneratorGroup((self.spiketimes_poi[-1][0] + 1), self.spiketimes_poi)
                else:
                    print "Generating new times for Poisson excitation"
                    P_poisson = bb.PoissonGroup(int(N_poisson), f_poisson)
                    
                Cip = bb.Connection(P_poisson, PiSW, 'ge_in', weight=self.g_pi*1E-9*siemens, sparseness=self.spr_input)
                
            rate_poi = bb.PopulationRateMonitor(P_poisson,bin=rate_bin*ms)
            MPp = bb.SpikeMonitor(P_poisson)
            network.add(P_poisson, MPp, Cip, rate_poi)


        if self.sigma_burst:
            
            if self.spr_input == 0:
                if self.spotLight == 0:
                    N_gen = self.n_synapses * self.Ni
                else:                    
                    N_gen = self.n_synapses * self.spotLight #No of input generators
                    
                if self.spiketimes_poi:
                    print "Here we go using the old times"
                    P_burst =  bb.SpikeGeneratorGroup((self.spiketimes_burst[-1][0] + 1), self.spiketimes_burst)
                else:
                    print "Here we go... this already works"
                    N_act = int(1*N_gen) # No. of active input neurons
                    burst = [(x,random.gauss(0.15, self.sigma_burst)) for x in range(int(N_act))]
                    fire_times = sorted(burst,key=lambda a: a[1])   
                    P_burst =  bb.SpikeGeneratorGroup(int(N_gen), fire_times)    
                    
                    

                #P_burst =  bb.SpikeGeneratorGroup(int(N_gen), fire_times)
                MP_burst = bb.SpikeMonitor(P_burst)
                rate_burst = bb.PopulationRateMonitor(P_burst,bin=rate_bin*ms)
                print P_burst
                
                if self.spotLight == 0:
                    Cbi = [[]]*self.Ni
                    for each in range(self.Ni):
                        Cbi[each] = bb.Connection(P_burst.subgroup(self.n_synapses), Pi.subgroup(1),'ge_in', weight=self.g_burst*1E-9*siemens, sparseness=1.0)
                else:
                    Cbi = [[]]*self.spotLight
                    for each in range(self.spotLight):
                        print each
                        Cbi[each] = bb.Connection(P_burst.subgroup(self.n_synapses), SubGroupsPi[i],'ge_in', weight=self.g_burst*1E-9*siemens, sparseness=1.0)
                network.add(P_burst,Cbi,MP_burst,rate_burst)
            
            else:
                N_gen = self.n_synapses/self.spr_input #No of input generators
                
                
                if self.spiketimes_poi:
                    print "Here we go using the old times"
                    P_burst =  bb.SpikeGeneratorGroup((self.spiketimes_burst[-1][0] + 1), self.spiketimes_burst)
                else:
                    print "Here we go... this already works"
                    N_act = int(1*N_gen) # No. of active input neurons
                    burst = [(x,random.gauss(0.15, self.sigma_burst)) for x in range(int(N_act))]
                    fire_times = sorted(burst,key=lambda a: a[1])   
                    P_burst =  bb.SpikeGeneratorGroup(int(N_gen), fire_times)
                
                
                

                MP_burst = bb.SpikeMonitor(P_burst)
                rate_burst = bb.PopulationRateMonitor(P_burst,bin=rate_bin*ms)
                
                if self.spotLight == 0:           
                    Cbi = bb.Connection(P_burst, Pi,'ge_in', weight=self.g_burst*1E-9*siemens, sparseness=self.spr_input)
                else:
                    Cbi = bb.Connection(P_burst, PiSW,'ge_in', weight=self.g_burst*1E-9*siemens, sparseness=self.spr_input)
                network.add(P_burst,Cbi,MP_burst,rate_burst)









     # MONITORS

#        if self.gj_pconnect:
#             MIgapSyn=bb.StateMonitor(Gii,'I_gap',record=True)
#             MIgap=bb.StateMonitor(Pi,'I_gap',record= True)

        if longSim:
            MPi = bb.SpikeMonitor(Pi, record=range(n_rec))
            network.add(MPi)
        else:
            MPi = bb.SpikeMonitor(Pi, record=range(n_rec))
            Mvi = bb.StateMonitor(Pi, 'v', record=range(n_rec))
            MIi = bb.StateMonitor(Pi, 'I_ii', record=range(n_rec))
            MIiExt = bb.StateMonitor(Pi, 'I_ext', record=range(n_rec))
            Iei = bb.StateMonitor(Pi, 'I_pi', record=range(n_rec))
            if self.gj_pconnect:
                 MIgap=bb.StateMonitor(Pi,'I_gap',record= True)
                 network.add(MIgap, Mvi, MIi, MPi, Iei, MIiExt)
            else:
                network.add(Mvi, MIi, MPi, Iei, MIiExt)




        network.run(sim_time)
        print 'Done!'


        self.simtime = sim_time
        self.rate_bin = rate_bin
        self.n_rec = n_rec # number of recorded units (from probe or population) to sample Vm, g_i and estimate lfp
        self.rate_inh = rate_inh
        self.rate_exc = rate_exc
        self.rate_poi = rate_poi
        self.rate_burst = rate_burst


        if longSim:
            self.MPi = MPi
            self.Gii = []
            self.MIgap = []
            self.MPp = []
            self.Mvi = [[]]*n_rec
            self.MIi = []


        else:
            self.MPi = MPi
            self.MIi = MIi
            self.MPp = MPp
            self.Mvi = Mvi
            self.MP_burst = MP_burst
            self.Iei = Iei
            self.MIiExt = MIiExt
            
            if self.gj_pconnect:
                self.Gii = Gii
                self.MIgap = MIgap
#                self.MIgapSyn = MIgapSyn

            else:
                self.Gii = []
                self.MIgap = []
#                self.MIgapSyn = []


        return True





    def spiketrain(self,pop_mon='MPi',t_0=0.052,t_win=0.02,sigma_krnl=0): #sigma in samples
        sptimes=np.array([])
        for i in range(len(self.__dict__[pop_mon].spiketimes)):
            sptimes=np.concatenate((sptimes,np.array([self.__dict__[pop_mon].spiketimes[i][j] for j in range(len(self.__dict__[pop_mon].spiketimes[i])) if (self.__dict__[pop_mon].spiketimes[i][j]>=t_0-t_win) & (self.__dict__[pop_mon].spiketimes[i][j]<=t_0+t_win)])))
        sptimes=sptimes*100000 #convert time to sample number
        sptrain=np.zeros(int((t_0+t_win)*100000)) #(int(max(sptimes))+1)
        x=np.array(range(len(sptrain)))*1.0 #make it real
        for i in range(len(sptimes)):
	    if sigma_krnl:
	        sptrain = sptrain + gt.gaussfunc(x,1/(np.sqrt(2*np.pi)*sigma_krnl),sptimes[i],sigma_krnl,0)
	    else:
                sptrain[sptimes[i]]=sptrain[sptimes[i]]+1

        return sptrain[(t_0-t_win)*100000:(t_0+t_win)*100000+1] #sptrain[min(sptimes):max(sptimes)]


    def currents(self,cell_no):  # Should be out
        Ie=self.Mgei[cell_no]*(self.v_exc/1000.0-self.Mvi[cell_no])
        Ii=self.Mgi[cell_no]*(self.v_inh/1000.0-self.Mvi[cell_no])
        Iit=self.g_iit*1E-9*(self.v_inh/1000.0-self.Mvi[cell_no])
        I0=self.g_l*1E-9*(self.v_rest/1000.0-self.Mvi[cell_no]) + Ie + Ii + Iit
        return Ie,Ii+Iit,I0,Iit

    def membrane_stats(self,fc):
        # values returned in volts
        Vm_means=np.array([np.mean(self.Mvi[k][self.Mvi.times>0.010]) for k in range(self.n_rec)])
        Vm_vars=np.array([np.var(self.Mvi[k][self.Mvi.times>0.010]) for k in range(self.n_rec)])
        Sgnl_vars = np.array([np.var(gt.bpfilt(self.Mvi[k][self.Mvi.times>0.010],0.9*fc,1.1*fc,100000.0)) for k in range(self.n_rec)])
        #Noises=np.sqrt(Vm_vars-Sgnl_vars)
        return Vm_means, Vm_vars, Sgnl_vars

    def spiketimes(self,pop_mon): #returns table with spike time || cell no.
        sptimes=[]
        if self.__dict__[pop_mon]:
            sptimes=np.vstack((self.__dict__[pop_mon].spiketimes[0],0*np.ones(len(self.__dict__[pop_mon].spiketimes[0])))).T
            for i in range(1,len(self.__dict__[pop_mon].spiketimes)):
                sptimes=np.vstack((sptimes,np.vstack((self.__dict__[pop_mon].spiketimes[i],i*np.ones(len(self.__dict__[pop_mon].spiketimes[i])))).T))
        return sptimes

    def spiketimes2(self,pop_mon): #returns table with spike time, one entry for one neuron
        sptimes=[]
        if self.__dict__[pop_mon]:
            sptimes=[[]]*len(self.__dict__[pop_mon].spiketimes)
            for i in range(0,len(self.__dict__[pop_mon].spiketimes)):
                sptimes[i]=self.__dict__[pop_mon].spiketimes[i]
        return sptimes

    def out(self,reduced=False):
        out=simout()
        out.date=date.today().isoformat()
        out.simtime=self.simtime.item()
        out.v_thres=self.v_thres
        n_rec=self.n_rec
        out.Ni=self.Ni
        out.spiketimes=self.spiketimes('MPi')
        out.spiketimes2=self.spiketimes2('MPi')

        if not reduced:

            out.Vm=np.array([self.Mvi[cell_no] for cell_no in range(n_rec)])
            out.times_rate=self.rate_inh.times
            out.times_cond=self.Mvi.times
            out.rate_inh=self.rate_inh.rate
            if self.rate_poi: out.rate_poi=self.rate_poi.rate
            if self.rate_burst: out.rate_burst=self.rate_burst.rate
            out.rate_bin=self.rate_bin
            out.spiketimes=self.spiketimes('MPi')         #
            out.spiketimes_poi=self.spiketimes('MPp')
            out.spiketimes_burst=self.spiketimes('MP_burst')
            out.Iii = self.MIi
            out.Iei = self.Iei

        return out




