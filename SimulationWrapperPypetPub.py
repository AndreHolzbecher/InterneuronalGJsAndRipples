# !/usr/bin/env python
# -*- coding: utf-8 -*-

# =============================================================================
# For multiple simulations we use pypet.
# This a python module for doing parameter exploration, 
# which comes along with a native way to store the 
# simulations input and output parameters.
# This file is the wrapper around our simulations,
# which also contain some postprocessing.
# =============================================================================


import matplotlib
matplotlib.use('Agg')

import simtools5Pub as st
import gapAnalyticsTempPub as gt
import numpy as np
from pypet import Environment, Trajectory, cartesian_product
import pandas as pd
import importlib
import sys


def main():
    
    startSimulation()
    
    return True

# This structure is used as the output 
# of one simulation run.
    
class mainOutput:
    def __init__(self,
            description = "",
            frqMax = [],
            averageFiring = [],
            syncIndex = [],
            syncIndexCorr = [],
            meanSpikeDistance = [],
            spectrumFreqs = [],
            spectrumValues = [],
            neuronFiringHist = [],
            spikeTrainsNeurons = [],
            singleNeuronFiring = [],
            averageFiringActive = [],
            qFactor = 0):

        self.description = description
        self.frqMax = frqMax
        self.syncIndex = syncIndex
        self.syncIndexCorr = syncIndexCorr
        self.averageFiring = averageFiring
        self.meanSpikeDistance = meanSpikeDistance
        self.spectrumFreqs = spectrumFreqs
        self.spectrumValues = spectrumValues
        self.neuronFiringHist = neuronFiringHist
        self.spikeTrainsNeurons = spikeTrainsNeurons
        self.singleNeuronFiring = singleNeuronFiring
        self.averageFiringActive = averageFiringActive
        self.qFactor = qFactor


def startSimulation():
    env = Environment(trajectory = trajectoryName,
                    comment = commentTraj,
                    add_time = True, # We don't want to add the current time to the name,
                    log_config = 'DEFAULT',
                    multiproc = True,
                    ncores = CoresToUse, #My laptop has 2 cores ;-)
                    filename = trajectoryPath,
                    git_repository = gitPath,
                    git_message = gitMessage

                    # We only pass a folder here, so the name is chosen
                    # automatically to be the same as the Trajectory
                    )

    traj = env.v_trajectory


    traj.f_add_parameter('neuron.synapses.g_ii', modelParas['g_ii'],
                        comment = 'The initial condition for the '
                                    'interneuron connection strength')
    
    
    traj.f_add_parameter('neuron.synapses.spr_ii', modelParas['spr_ii'],
                        comment = 'The prob of the INT-INT connection')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_pconnect', modelParas['gj_pconnect'],
                        comment = 'Connectio probability of the gap junctions ')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_cluster_size', modelParas['gj_cluster_size'],
                        comment = 'Cluster size in which the gap junctions are connected')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_passive', modelParas['gj_passive'],
                        comment = 'The passive/conductive component '
                                    'of the GJ in the Ostojic model. 19.10.2015 Galarreta and Hestrin (2001)')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_spike', modelParas['gj_spike'],
                        comment = 'The spike/active component of the GJ '
                                    'in the Ostojic model. 19.10.2015 Galarreta and Hestrin (2001)')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_delay', modelParas['gj_delay'],
                        comment = 'Delay of the gap junctions')

    traj.f_add_parameter('neuron.gapJunctions.gj_passive_Sigma', modelParas['gj_passive_Sigma'],
                        comment = 'Sigma of the Gauss of the passive distribution')
    
    traj.f_add_parameter('neuron.gapJunctions.gj_spike_Sigma', modelParas['gj_spike_Sigma'],
                        comment = 'Sigma of the Gauss of the spike distribution')

    traj.f_add_parameter('neuron.excDrive.poissonDrive', modelParas['poissonDrive'],
                        comment = 'Total drive of a neuron.'
                                    'Seperated over multiple synapses')
    
    traj.f_add_parameter('neuron.excDrive.driveSigma', modelParas['driveSigma'],
                        comment = 'Sigma of the Gaussian distribution for one neuron ')

    traj.f_add_parameter('neuron.excDrive.spotLight', modelParas['spotLight'],
                        comment = 'Optics light experiment..only activate a fraction of neurons ')

    traj.f_add_parameter('neuron.excDrive.sigma_burst', modelParas['sigma_burst'],
                        comment = 'Width of the Gaussian for the excitatory sharp wave burst')

    traj.f_add_parameter('neuron.excDrive.n_synapses', modelParas['n_synapses'],
                        comment = 'Number of synapses on each neuron')

    traj.f_add_parameter('neuron.excDrive.spr_input', modelParas['spr_input'],
                        comment = 'Fraction of shared input')
    
    traj.f_add_parameter('neuron.excDrive.I_std', modelParas['I_std'],
                        comment = 'Standard deviation of the mean drive')
    
    traj.f_add_parameter('neuron.excDrive.I_mean', modelParas['I_mean'],
                        comment = 'Mean drive')
    
    traj.f_add_parameter('neuron.synapses.tl_ii', modelParas['tl_ii'],
                        comment = 'Fraction of shared input')
    
    traj.f_add_parameter('neuron.synapses.tr_ii', modelParas['tr_ii'],
                        comment = 'Fraction of shared input')

    traj.f_add_parameter('neuron.synapses.td_ii', modelParas['td_ii'],
                        comment = 'Fraction of shared input')




    # Add parameter to explore, defined in the beginning
    traj.f_explore(forExploration)

    env.f_add_postprocessing(neuron_postproc)

    env.f_run(run_network)
    env.f_disable_logging()
    return traj


def run_network(traj):
    """ Simple wrapper function for compatibility with *pypet*.

    We will call the original simulation functions with data extracted from ``traj``.

    The resulting automaton patterns wil also be stored into the trajectory.

    :param traj: Trajectory container for data

    """
    # Make initial state
    model = st.model()

    # Transfer parameter to model
    model.g_ii = traj.g_ii
    
    model.spr_ii = traj.spr_ii
    
    model.gj_spike =  traj.neuron.gapJunctions.gj_spike
    model.gj_passive =  traj.neuron.gapJunctions.gj_passive
    model.gj_cluster_size =  traj.neuron.gapJunctions.gj_cluster_size
    model.gj_pconnect =  traj.neuron.gapJunctions.gj_pconnect

    model.gj_passive_Sigma = traj.neuron.gapJunctions.gj_passive_Sigma
    model.gj_spike_Sigma = traj.neuron.gapJunctions.gj_spike_Sigma

    model.gj_delay = traj.neuron.gapJunctions.gj_delay

    model.f_poisson_sigma = traj.neuron.excDrive.driveSigma
    model.tot_f_poisson = traj.neuron.excDrive.poissonDrive

    model.spotLight = traj.neuron.excDrive.spotLight

    model.sigma_burst = traj.neuron.excDrive.sigma_burst

    model.n_synapses = traj.neuron.excDrive.n_synapses
    model.spr_input = traj.neuron.excDrive.spr_input
    
    model.I_mean = traj.neuron.excDrive.I_mean
    model.I_std = traj.neuron.excDrive.I_std
    
    
    model.td_ii = traj.td_ii
    model.tr_ii = traj.tr_ii
    model.tl_ii = traj.tl_ii
    

    identifier = traj.v_name + traj.v_crun


    # function that runs simulations and generates output
    results = simulationWrapper('Delay of the excitatory spike component', model, simTime = simulationTime, identify = identifier)
    # put name for figure in call for simulationWrapper...

    #Store results to the trajectory

#    traj.f_add_result('$.description', description = traj.v_idx, comment='Description of this network run')
    traj.f_add_result('$.description', description = results.description, comment='Description of this network run')
    traj.f_add_result('$.derived',
                      frqMax = results.frqMax,
                      syncIndex = results.syncIndex,
                      syncIndexCorr = results.syncIndexCorr,
                      averageFiring = results.averageFiring,
                      meanSpikeDistance = results.meanSpikeDistance,
                      spectrumFreqs = results.spectrumFreqs,
                      spectrumValues = results.spectrumValues,
                      averageFiringActive = results.averageFiringActive,
                      singleNeuronFiring = results.singleNeuronFiring,
                      qFactor = results.qFactor,
                      comment='Derived quantities for this network run')


    traj.f_add_result('$.raw',
                      spikeTrainsNeurons = results.spikeTrainsNeurons,
                      comment='Raw data for this network run')

    return results





def simulationWrapper(
        description, model,
        identify = 'run1',
        simTime = 0.01,
        n_rec = 200):


    """
    model = st.model()
    model.gj_spike = gj_spike
    model.gj_cluster_size = gj_cluster_size
    model.gj_passive = gj_passive
    model.gj_pconnect = gj_pconnect
    model.tot_f_poisson = drive
    model.f_poisson_sigma = driveSigma
    """
    model.simulate(simTime, n_rec, longSim = longSim)
    output = model.out(reduced = True)

    frqMax = output.net_freq()[0]
    spikeTrainsNeurons = output.spiketimes
#    spikeTrainsNeurons2 = np.array(output.spiketimes2)
    #syncIndex = output.net_freq()[1]
    #syncIndex = gt.spikeTrainSynchronyIndexSymmetric(output, 0.0005)
    sTTD = gt.spikeTrainTimeDistances(output)
    averageFiring = 0
    averageFiringActive = 0
    singleNeuronFiring = []
    meanSpikeDistance = 0
    spectrumFreqs = []
    spectrumValues = []
    neuronFiringHist = []
    qFactor = 0.0
    syncIndex = 0.0
    syncIndexCorr = 0.0

    if len(sTTD) != 0:
        meanSpikeDistance = sum(sTTD)/float(len(sTTD))
        singleNeuronFiring = output.spike_stats()[0]
        averageFiringActiveList = [singleNeuronFiring[i] for i in range(len(singleNeuronFiring)) if singleNeuronFiring[i] != 0.0]
        averageFiringActive = np.average(averageFiringActiveList)
        averageFiring = np.average(singleNeuronFiring)
        if withinCycleSynchrony == "MaxWindow":
            syncIndex, syncIndexCorr = gt.SynchronyIndexMaxWindow(output, 0.0005, averageFiring, frqMax)
        elif withinCycleSynchrony == "Standard":
            syncIndex, syncIndexCorr = gt.spikeTrainSynchronyIndexSymmetricCorrected(output, 0.0005, averageFiring)
        elif withinCycleSynchrony == "Brunel":
            syncIndex = gt.synchronyBrunel2003(output, 0.0005)        
        else:
            "Do not understand"
            
            
        out = output.net_freq()
        (frq, frqY) = (out[2], out[3])
        spectrumFreqs = frq[frq < 600]
        spectrumValues = abs(frqY[frq < 600])
        neuronFiringHist = np.histogram(output.spike_stats()[0], bins = 25, range=[0,600])
        if longSim:
            if qFactorPlot:
                (lala, qFactor, figQPlot) = gt.qFactorCalc2(spikeTrainsNeurons, plotGen=qFactorPlot, simTime = simulationTime)
                gt.saveFig(figQPlot, identify + 'qPlot')
                gt.saveFig(figQPlot, identify + 'qPlot', fileFormat = '.png')
            if qFactorCalculation:
                qFactor = gt.qFactorCalc(spikeTrainsNeurons)[1]




    #neuronFiringHist must be replaced

    outputAll = mainOutput(description = description,
            frqMax = frqMax,
            averageFiring = averageFiring,
            syncIndex = syncIndex,
            syncIndexCorr = syncIndexCorr,
            meanSpikeDistance = meanSpikeDistance,
            spectrumFreqs = spectrumFreqs,
            spectrumValues = spectrumValues,
            neuronFiringHist = neuronFiringHist,
            spikeTrainsNeurons = spikeTrainsNeurons,
            singleNeuronFiring = singleNeuronFiring,
            averageFiringActive = averageFiringActive,
            qFactor = qFactor)



    return outputAll

def trajCheck(name, fullName = False):

    if not fullName:
        name = './data/' + name

    print name
    traj = Trajectory(filename = name, add_time=False)
    traj.f_load(index=-1, load_parameters=2, load_results=2, force=True)
    traj.v_auto_load = True
    traj.v_idx = 0

    return traj


def neuron_postproc(traj, result_list):
    """Postprocessing, sorts computed firing rates into a table

    :param traj:

        Container for results and parameters

    :param result_list:

        List of tuples, where first entry is the run index and second is the actual
        result of the corresponding run.

    :return:
    """

    # Let's create a pandas DataFrame to sort the computed firing rate according to the
    # parameters. We could have also used a 2D numpy array.
    # But a pandas DataFrame has the advantage that we can index into directly with
    # the parameter values without translating these into integer indices.

    # arbitray parameters
    # maybe include automatic readout of the explored parameter?


    exploredParameters = traj.f_get_explored_parameters()

    if len(exploredParameters)==1:

        parameter1 = exploredParameters.keys()[0]

        par1_range = traj.f_get(parameter1).f_get_range()

        par1_index = sorted(set(par1_range))

        frqMax_frame = pd.Series(index=par1_index)
        syncIndex_frame = pd.Series(index=par1_index)
        syncIndexCorr_frame = pd.Series(index=par1_index)
        averageFiring_frame = pd.Series(index=par1_index)
        averageFiringActive_frame = pd.Series(index=par1_index)
        meanSpikeDistance_frame = pd.Series(index=par1_index)
        qFactor_frame = pd.Series(index=par1_index)

        for result_tuple in result_list:
            run_idx = result_tuple[0]
            traj.v_idx = run_idx
            totalResults = result_tuple[1]
            par1_val = par1_range[run_idx]


            # data frame
            frqMax_frame.loc[par1_val] = totalResults.frqMax # Put the firing rate into the
            syncIndex_frame.loc[par1_val] = totalResults.syncIndex
            syncIndexCorr_frame.loc[par1_val] = totalResults.syncIndexCorr
            averageFiring_frame.loc[par1_val] = totalResults.averageFiring
            averageFiringActive_frame.loc[par1_val] = totalResults.averageFiringActive
            meanSpikeDistance_frame.loc[par1_val] = totalResults.meanSpikeDistance
            qFactor_frame.loc[par1_val] = totalResults.qFactor


    elif len(exploredParameters)==2:

        parameter1 = exploredParameters.keys()[0]
        parameter2 = exploredParameters.keys()[1]

        par1_range = traj.f_get(parameter1).f_get_range()
        par2_range = traj.f_get(parameter2).f_get_range()

        par1_index = sorted(set(par1_range))
        par2_index = sorted(set(par2_range))

        frqMax_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        syncIndex_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        syncIndexCorr_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        averageFiring_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        averageFiringActive_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        meanSpikeDistance_frame = pd.DataFrame(columns=par2_index, index=par1_index)
        qFactor_frame = pd.DataFrame(columns=par2_index, index=par1_index)


        for result_tuple in result_list:
            run_idx = result_tuple[0]
            traj.v_idx = run_idx
            totalResults = result_tuple[1]
            par1_val = par1_range[run_idx]
            par2_val = par2_range[run_idx]


            # data frame
            frqMax_frame.loc[par1_val, par2_val] = totalResults.frqMax # Put the firing rate into the
            syncIndex_frame.loc[par1_val, par2_val] = totalResults.syncIndex
            syncIndexCorr_frame.loc[par1_val, par2_val] = totalResults.syncIndexCorr
            averageFiring_frame.loc[par1_val, par2_val] = totalResults.averageFiring
            averageFiringActive_frame.loc[par1_val, par2_val] = totalResults.averageFiringActive
            meanSpikeDistance_frame.loc[par1_val, par2_val] = totalResults.meanSpikeDistance
            qFactor_frame.loc[par1_val, par2_val] = totalResults.qFactor

    else:
        raise NameError('What to do with 3 parameters?')




    # select data for plotting in tables




    #spectrumFreqs = results.spectrumFreqs,
    #spectrumValues = results.spectrumValues,
    # This frame is basically a two dimensional table that we can index with our
    # parameters

    # Now iterate over the results. The result list is a list of tuples, with the
    # run index at first position and our result at the second



    # Finally we going to store our new firing rate table into the trajectory
    traj.f_add_result('summary.firingRates', frqMax_frame=frqMax_frame,
                      comment='Contains a pandas data frame with all firing rates.')
    traj.f_add_result('summary.syncIndex', syncIndex_frame=syncIndex_frame,
                      comment='Contains a pandas data frame with all firing rates.')
    traj.f_add_result('summary.syncIndexCorr', syncIndexCorr_frame=syncIndexCorr_frame,
                      comment='Contains a pandas data frame with all firing rates.')
    traj.f_add_result('summary.averageFiring', averageFiring_frame=averageFiring_frame,
                      comment='Contains a pandas data frame with all firing rates.')
    traj.f_add_result('summary.averageFiringActive', averageFiringActive_frame=averageFiringActive_frame,
                      comment='Contains a pandas data frame with all firing rates.')
    traj.f_add_result('summary.meanSpikeDistance', meanSpikeDistance_frame=meanSpikeDistance_frame,
                      comment='Contains a pandas data frame with all firing rates.')

    traj.f_add_result('summary.qFactor', qFactor_frame=qFactor_frame,
                      comment='Contains a pandas data frame the quality factor')
def showResults(traj):
    for run_name in traj.f_get_run_names():
        traj.f_set_crun(run_name)
        #x=traj.gj_pconnect
        #model.g_ii = traj.g_ii
        #model.gj_spike =  traj.neuron.gapJunctions.gj_spike
        #model.gj_passive =  traj.neuron.gapJunctions.gj_passive
        #model.gj_cluster_size =  traj.neuron.gapJunctions.gj_cluster_size
        #model.gj_pconnect =  traj.neuron.gapJunctions.gj_pconnect

        #model.f_poisson_sigma = traj.neuron.excDrive.driveSigma
        #model.tot_f_poisson = traj.neuron.excDrive.poissonDrive

        #y=traj.y
        #z=traj.z
        #print '%s: %f %f %f %f %f %f %f %f %f' %(run_name, traj.gj_pconnect, traj.g_ii, traj.gj_spike, traj.gj_passive, traj.gj_cluster_size, traj.driveSigma, traj.poissonDrive, traj.res.crun.derived.syncIndex, traj.res.crun.derived.frqMax)

        #print '%s: %f %f %f %f %f %f' %(run_name, traj.neuron.gapJunctions.gj_spike,traj.neuron.gapJunctions.gj_passive, traj.neuron.gapJunctions.gj_cluster_size,traj.neuron.gapJunctions.gj_pconnect ,traj.res.crun.derived.syncIndex, traj.res.crun.derived.frqMax)
        print '%s: %f %f %f %f %f' %(run_name,traj.gj_pconnect, traj.neuron.gapJunctions.gj_spike, traj.poissonDrive, traj.neuron.gapJunctions.gj_passive, traj.neuron.excDrive.spotLight)
    # Don't forget to reset your trajectory to the default settings, to release its belief to
    # be the last run:
    traj.f_restore_default()

def newSyncMeasures(traj):



    coherenceIndex = (traj.res.summary.syncIndex.syncIndex_frame).copy()
    traubSyncIndex = (traj.res.summary.syncIndex.syncIndex_frame).copy()


    print 'Total runs %s' %(traj.f_get_run_names()[-1])
    for run_name in traj.f_get_run_names():

        traj.f_as_run(run_name)
        print '%s' %(run_name)

        driveSigma = traj.par.neuron.excDrive.driveSigma
        spiketimes = traj.res.crun.raw.spikeTrainsNeurons
        column = coherenceIndex.columns[0]
        (coherenceValue, TraubIndex) = gt.differentSyncIndex(spiketimes)

        coherenceIndex.loc[driveSigma,column] = coherenceValue
        traubSyncIndex.loc[driveSigma,column] = TraubIndex


        #print coherence


    traj.f_add_result('summary.coherenceIndex', coherenceIndex = coherenceIndex,
                      comment='Defined analogous to Joses work')
    traj.f_add_result('summary.traubSyncIndex', traubSyncIndex=traubSyncIndex,
                      comment='Synchrony parameter as in Traub 2001')


    # Don't forget to reset your trajectory to the default settings, to release its belief to
    # be the last run:

    traj.f_store()
    traj.f_restore_default()

    return coherenceIndex, TraubIndex



def SyncMeasuresCorr(traj):



    syncIndexCorr = (traj.res.summary.syncIndex.syncIndex_frame).copy()



    print 'Total runs %s' %(traj.f_get_run_names()[-1])
    for run_name in traj.f_get_run_names():

        traj.f_as_run(run_name)
        print '%s' %(run_name)

        driveSigma = traj.par.neuron.excDrive.driveSigma
        gj_pconnect = traj.par.neuron.gapJunctions.gj_pconnect
        #spiketimes = traj.res.crun.raw.spikeTrainsNeurons

        column = syncIndexCorr.columns[int(gj_pconnect)]
        syncIndexCorrValue = traj.res.crun.derived.syncIndexCorr

        syncIndexCorr.loc[driveSigma,column] = syncIndexCorrValue


        #print coherence


    traj.f_add_result('summary.syncIndexCorr', syncIndexCorr = syncIndexCorr,
                      comment='Defined analogous to Joses work')



    # Don't forget to reset your trajectory to the default settings, to release its belief to
    # be the last run:

    traj.f_store()
    traj.f_restore_default()

    return syncIndexCorr

def addQuality(traj):


    qFactor = (traj.res.summary.syncIndex.syncIndex_frame).copy()

    print 'Total runs %s' %(traj.f_get_run_names()[-1])

    for run_name in traj.f_get_run_names():

        traj.f_set_crun(run_name)
        print '%s' %(run_name)


        spikes = traj.res.crun.raw.spikeTrainsNeurons
        FWHM, qFactorValue, fig = gt.qFactorCalc3(spikes, plotGen=True, simTime = 10.0, verbose = True)

        activeSpike = traj.par.neuron.gapJunctions.gj_spike
        passiveConductance = traj.par.neuron.gapJunctions.gj_passive


        qFactor.loc[activeSpike, passiveConductance] = qFactorValue

        print qFactor
        gt.saveFig(fig,  'qPlot' + run_name)
        gt.saveFig(fig, 'qPlot' + run_name, fileFormat = '.png')
        #print coherence


    traj.f_add_result('summary.qFactorNew', qFactor = qFactor,
                      comment='FWHM divided by peak frequency')



    # Don't forget to reset your trajectory to the default settings, to release its belief to
    # be the last run:

    traj.f_store()
    traj.f_restore_default()

    return qFactor

if __name__ == '__main__':


    #Set the trajectory as the name of the config file
    trajectoryName =  sys.argv[1]

    loadExplorationParas = 'simulationConfigs.' + trajectoryName

    explorationParameters = importlib.import_module(loadExplorationParas)


    #==============================================================================
    # Set parameters
    #==============================================================================

    #Read from file
    if len(explorationParameters.forExploration) == 1:
        forExploration = explorationParameters.forExploration
    elif len(explorationParameters.forExploration) == 2:
        forExploration = cartesian_product(explorationParameters.forExploration)
    else:
        raise NameError('What to do with 3 parameters?')

    modelParas = explorationParameters.modelParas
    #forExploration = cartesian_product({'gj_cluster_size':[20,40], 'gj_pconnect':[0.0,1.0]})

    #Simulation time
    simulationTime = explorationParameters.simulationTime

    #TrajParameter
    CoresToUse = explorationParameters.CoresToUse
    gitMessage = explorationParameters.gitMessage
    commentTraj = explorationParameters.commentTraj
    trajectoryPath = explorationParameters.trajectoryPath
    gitPath = explorationParameters.gitPath

    longSim = explorationParameters.longSim
    
    if hasattr(explorationParameters, 'withinCycleSynchrony'):
        withinCycleSynchrony = explorationParameters.withinCycleSynchrony
    else:
        withinCycleSynchrony = "Standard"
        
    qFactorPlot = explorationParameters.qFactorPlot
    qFactorCalculation = explorationParameters.qFactorCalculation

    print 'Parameters loaded'

    success = main()
    


