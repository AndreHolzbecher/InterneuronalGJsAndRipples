# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 16:06:13 2016

@author: andre.holzbecher@gmail.com
"""

#==============================================================================
#  In this script all the graphics are generated, 
#  which appear in the Holzbecher and Kempter (2018).
#  For package requirements please read the documentation
#  for the scripts.
#  
#  In this script the data is only plotted (except for Fig. 1c and Fig. 2).
#  Simulations are carried out elsewhere. See the documentation for the scripts.
#  This script is optimized for spyder.
#==============================================================================



#%% imports
from __future__ import unicode_literals
from matplotlib import rcParams, ticker

#rcParams['font.sans-serif'] = "Sans"

import plottingPub as ps
import SimulationWrapperPypetPub as sw
import gapAnalyticsTempPub as gt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import ColorMapsAndAxisTitleParasPub as cmt
import matplotlib.gridspec as gridspec
from scipy.ndimage.filters import gaussian_filter
import cPickle as cp
import pythonInterfaceHocVers2Pub as pih
import simtools5Pub as st
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
#matplotlib.rc('font', serif='Helvetica Neue') 


#%% Fig 1c 

# =============================================================================
# Currents of the different synapses 
# =============================================================================

if len(sys.argv) == 2:
    plotFigures = sys.argv[1]

# create neuron model
model = st.model()

# set input currents 
I_inj_e = 0
I_inj_i = 0.5
pulse_len=3.5
spk_delay=80.0
tlim=[0,300]
legend=False
name = 'no'

# create figure
fig = plt.figure(figsize = (3.0, 3.0))
allPlots = gridspec.GridSpec(4, 1, height_ratios = [2, 1, 1, 1], hspace = .2)
axList = []

for i in [0,1,2,3]:
    
    axList.append(fig.add_subplot(allPlots[i]))
    
# first run 
# only gap junctions
model.v_reset = - 80.0
model.g_ii = 0.0
model.gj_spike = 0.25
model.gj_passive = 1.0

(Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)
axList[0].plot(Mvi.times*1000, Mvi[0]*1000.0, '#1f77b4')
axList[0].plot([MPiS.spiketimes[0]*1000.0,MPiS.spiketimes[0]*1000.0],[model.v_thres,(model.v_thres + 100.0)], '#1f77b4',linestyle = '--')

axList[0].plot([0, 20], [0, 0 ], 'k--', linewidth = 0.5)

axList[0].text(15, 5, "0 mV")


axList[1].plot(Mvi.times*1000, Mvi[1]*1000.0, cmt.colorWithGJ)


# second run 
# only inhibition
model.g_ii = 5.0
model.gj_spike = 0.0
model.gj_passive = 0.0

(Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)


axList[2].plot(Mvi.times*1000, Mvi[1]*1000.0, cmt.colorWithOutGJ)

# third one 
# inhibition and gap junctions

model.g_ii = 5.0
model.gj_spike = 0.25
model.gj_passive = 1.0

(Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)


axList[3].plot(Mvi.times*1000, Mvi[1]*1000.0, cmt.colorWithOutGJ)
axList[3].plot(Mvi.times*1000, Mvi[1]*1000.0, cmt.colorWithGJ, linestyle = ':')
#ax14.text(-3, Mvi[1][0]*1000 + 1.0 * 2.0, "β = " + "mV", fontsize=8)#(, rotation = 'vertical')


# some makeup
for axis in axList:
    axis.set_xlim([0, 20])


for axis in axList[:-1]:
    
    gt.remove_box_cueAll(axis,keepx = False)
    
for axis in axList[1:]:
    axis.set_ylim([-66.0, -64.0])

for axis in axList[-1:]:
    
    gt.remove_box_cueAll(axis,keepx = True)


gt.addScale(axList[0], -0.05, 20, "mV", yPos = -40, distPar = 0.15)
gt.addScale(axList[2], -0.05, 1,"mV", yPos = -65.3, distPar = 0.15)


figname = "ComparisonBetaAndGamma"

if "plotFigures" in locals():
    if plotFigures:
        gt.saveFig(fig, figname)
        gt.saveFig(fig, figname, fileFormat='.png', dpi=300)
else: 
    gt.saveFig(fig, figname)
    gt.saveFig(fig, figname, fileFormat='.png', dpi=300)
#%% Fig 2 transient oscillations

# =============================================================================
# Because these simulations are just one
# run and carried out fast, we just do it 
# right here
# =============================================================================
    
rcParams.update({'font.size': 8})

reload(cmt)
reload(ps)
reload(st)

# for the periodogram
minFreq = 10.0
maxFreq = 500.0
# for the spectrogram 
minMaxFreq = [100.0, 300.0]

# here we basically have a loop
# to generate multiple runs of the same
# randomly initialized simulation.

# First GJs=true then GJs=false.
# Excitation is created only once for
# these two different trails.

# The figure in the manuscript is representative for this
# simulations. Note that that there is quite some variability 
# between the invidividual plots.

for each in range(1):
      
    for withGap in [True, False]:
        
        # create model
        
        model = st.model()
        
        
        # passes on the spike times of excitations for the 
        # run w/o GJs
        
        if not withGap:
            
            spiketimes_poi = out.spiketimes_poi.copy()
            spiketimes_poi_New = [(int(y), x) for (x,y) in spiketimes_poi]            
            spiketimes_burst = out.spiketimes_burst.copy()
            spiketimes_burst_New = [(int(y), x) for (x,y) in spiketimes_burst]
            model.spiketimes_poi = spiketimes_poi_New
            model.spiketimes_burst = spiketimes_burst_New
        
        # set the excitation for the burst
        model.sigma_burst = 0.007
        model.tot_f_poisson = 750
        
        # and other parameters
        model.spr_input = 0.1
        model.n_synapses = 35
        model.gj_spike = 0.25       
        model.rate_bin = 0.2
        
        if withGap:
            model.gj_pconnect = 0.3
            figname = 'Fig01TransientWithGJ' + str(each)
        else:
            model.gj_pconnect = 0.0
            figname = 'Fig01TransientWithoutGJ' + str(each) 
        
        model.initRandom = True
        model.simulate(0.3, n_rec=199)
        

        # extract some data from the output file of the simulation
        out = model.out()        
        spikes = out.spiketimes
        net_freqs = out.net_freq()
        (frq, frqY) = (net_freqs[2], net_freqs[3])
        spectrumFreqs = frq[frq < maxFreq]
        spectrumValues = abs(frqY[frq < maxFreq])
        
        
        print each, withGap
        if ((each == 0) & withGap):
            print "We do this once"
            spectrumArray = spectrumValues.copy()
            spectrumFreqsAr = spectrumFreqs
        else:
            spectrumArray = np.vstack((spectrumArray, spectrumValues))
        
        singleNeuronFiring = out.spike_stats()[0]
        averageFiring = np.average(singleNeuronFiring)
        
        # set title
        title = "Pcon "  + str(model.gj_pconnect) + "," + " Neurons " + str(model.spotLight) + ', '
       
        # this is actually the main function doing all the jest
        # further infos inside
        reload(ps)
        if withGap:
            maxDict = {}
            maxDict = {'excitationActivity': [0, 4000]}
            fig, maxDict = ps.rasterPlotSWR(out, 0.31, number_neuron = 199, 
                               sync=4, figName = figname, 
                               markersize = 1.0, returnFig=True, 
                               showTime = 0.0, bins = 50,
                               spectrumFreqs = spectrumFreqs[spectrumFreqs > minFreq],
                               spectrumValues = spectrumValues[spectrumFreqs > minFreq],
                               title = title, maxDict = maxDict, minMaxFreq = minMaxFreq,
                               leadingFreq = net_freqs[0], averageFiring = averageFiring)
        else:
            fig, maxDict = ps.rasterPlotSWR(out, 0.31, number_neuron = 199, 
                                   sync=4, figName = figname, 
                                   markersize = 1.0, returnFig=True, 
                                   showTime = 0.0, bins = 50,
                                   spectrumFreqs = spectrumFreqs[spectrumFreqs > minFreq],
                                   spectrumValues = spectrumValues[spectrumFreqs > minFreq],
                                   title = title, maxDict = maxDict, minMaxFreq = minMaxFreq,                                
                                   leadingFreq = net_freqs[0], averageFiring = averageFiring)

        
        
        if "plotFigures" in locals():
            if plotFigures:
                gt.saveFig(fig, figname, setPath = 'Manuscript02/Fig01Final2')
                gt.saveFig(fig, figname, fileFormat='.png', dpi=300, setPath = 'Manuscript02/Fig01Final2')
                plt.show()
        else: 
            gt.saveFig(fig, figname, setPath = 'Manuscript02/Fig01Final2')
            gt.saveFig(fig, figname, fileFormat='.png', dpi=300, setPath = 'Manuscript02/Fig01Final2')
            plt.show()

#%% Fig 3 Total


            
reload(ps)
rcParams.update({'font.size': 8})
fig = plt.figure(dpi = 600, figsize = cmt.twoThirdRatio)

#make outer gridspec
gs1 = gridspec.GridSpec(2, 1, height_ratios = [1, 1], hspace = .4) 
#make nested gridspecs
upper = gridspec.GridSpecFromSubplotSpec(1, 2, width_ratios = [1, 2], subplot_spec = gs1[0], wspace = .5)
twoDplots = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec = gs1[1], wspace = 0.65)

rasterPlots = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = upper[0], hspace = 0.5)

rasterPlotsLong = gridspec.GridSpecFromSubplotSpec(3, 1, subplot_spec = rasterPlots[0], hspace = .1)
rasterPlotsShort = gridspec.GridSpecFromSubplotSpec(3, 1, subplot_spec = rasterPlots[1], hspace = .1)

axList = []
    
axList.append(fig.add_subplot(rasterPlotsLong[0]))
axList.append(fig.add_subplot(rasterPlotsLong[1]))
axList.append(fig.add_subplot(rasterPlotsShort[0]))
axList.append(fig.add_subplot(rasterPlotsShort[1]))   
axList.append(fig.add_subplot(upper[1]))

axList.append(fig.add_subplot(twoDplots[0]))
axList.append(fig.add_subplot(twoDplots[1]))   
axList.append(fig.add_subplot(twoDplots[2]))    

axList.append(fig.add_subplot(rasterPlotsLong[2]))
axList.append(fig.add_subplot(rasterPlotsShort[2]))   

# Fill the grid
# Rasterplots 
# load data
traj = sw.trajCheck('Fig03AB.hdf5')

plotList = ['000','015', '030']
time = [0.0315, 0.0652]
timeZoom = [0.043, 0.054]

colors = [cmt.colorWithOutGJ, cmt.colorWithGJ, cmt.colorWithStrongGJ]
spikeTrain = []

for i, each in enumerate(plotList):
    run_name = 'run_00000' + each
    traj.f_set_crun(run_name)
    spikeTrain.append(traj.res['run_0000' + str(each)].raw.spikeTrainsNeurons)

markersize = 1.0
rasterized = True

# Doing the rasterplots 

ps.rasterPlotSingle(spikeTrain[0], time[1], number_neuron = 199, ccode = cmt.colorWithOutGJ, markersize = markersize, showTime = time[0], ax = axList[0], rasterized = rasterized)
axList[0].set_ylabel('W/o GJ', fontsize = 5)

# With gap junctions long trace
ps.rasterPlotSingle(spikeTrain[1], time[1], number_neuron = 199, ccode = cmt.colorWithGJ, markersize = markersize, showTime = time[0], ax = axList[1], rasterized = rasterized)
axList[1].set_ylabel('W/ GJ', fontsize = 5)
#axList[1].set_xlabel(cmt.labelTimeMs)

# With strong gap junctions long trace
# shifting time here to have a nicer overlap of the spiking times
shiftTime2 = - 0.0017
ps.rasterPlotSingle(spikeTrain[2], time[1] - shiftTime2, number_neuron = 199, ccode = cmt.colorWithStrongGJ, markersize = markersize, showTime = time[0] - shiftTime2, ax = axList[-2], rasterized = rasterized)
axList[-2].set_ylabel('W/ GJ', fontsize = 5)
axList[-2].set_xlabel(cmt.labelTimeMs)

# Without gap junctions short trace
ps.rasterPlotSingle(spikeTrain[0], timeZoom[1], number_neuron = 199, ccode = cmt.colorWithOutGJ, markersize = markersize, showTime = timeZoom[0], ax = axList[2], rasterized = rasterized)
axList[2].set_ylabel('W/o GJ', fontsize = 5)

# With gap junctions short trace
ps.rasterPlotSingle(spikeTrain[1], timeZoom[1], number_neuron = 199, ccode = cmt.colorWithGJ, markersize = markersize, showTime = timeZoom[0], ax = axList[3], rasterized = rasterized)
axList[3].set_ylabel('W/ GJ', fontsize = 5)

# With strong gap junctions short trace
ps.rasterPlotSingle(spikeTrain[2], timeZoom[1] - shiftTime2, number_neuron = 199, ccode = cmt.colorWithStrongGJ, markersize = markersize, showTime = timeZoom[0] - shiftTime2, ax = axList[-1], rasterized = rasterized)
axList[-1].set_ylabel('W/ GJ', fontsize = 5)
axList[-1].set_xlabel(cmt.labelTimeMs)

# Plot Connection Probability vs. network statistics

name = 'Fig03AB.hdf5'
traj = sw.trajCheck(name)



transposeAll = False
gaussSmoothing = True
smothingWidth = 10
identifier = ''

  
# get data from traj

if transposeAll == True:

    syncIndex= traj.res.summary.syncIndexCorr.syncIndexCorr_frame.T
    netFreq = traj.res.summary.firingRates.frqMax_frame.T
    averageFiring = traj.res.summary.averageFiringActive.averageFiringActive_frame.T
else:
    syncIndex= traj.res.summary.syncIndexCorr.syncIndexCorr_frame
    netFreq = traj.res.summary.firingRates.frqMax_frame
    averageFiring = traj.res.summary.averageFiringActive.averageFiringActive_frame


axList[4].set_ylabel("Percentual change")
axList[4].set_xlabel('GJ Connection Probability')



# Normalize to percentage
referenceValue = 15
syncIndex = 100.0 * syncIndex / syncIndex.iloc[referenceValue] - 100.0
netFreq = 100.0 * netFreq / netFreq.iloc[referenceValue]- 100.0
averageFiring = 100.0 * averageFiring / averageFiring.iloc[referenceValue]- 100.0




# Recalculate the value for the x axis to go 
# form cluster GJ connectivity to total GJ connectivity

syncIndex.index = syncIndex.index / 5.0
netFreq.index = netFreq.index /  5.0
averageFiring.index = averageFiring.index /  5.0


# And plot it...

axList[4].plot([0, 0.2], [0, 0], '0.7',linestyle = '--', lw = 0.5)

syncIndex.plot(color = cmt.colorSynchrony, subplots=False, ax=axList[4], legend=False)
netFreq.plot(color = cmt.colorNetworkFrequency, subplots=False, ax=axList[4], legend=False)
averageFiring.plot(color = cmt.colorFiringRate, subplots=False, ax=axList[4], legend=False)

# Some make up...

axList[4].locator_params(axis = 'y', nbins = 6)
axList[4].locator_params(axis = 'x', nbins = 3)
axList[4].yaxis.set_tick_params(direction='out')

gt.remove_box_cue(axList[4], keepx=True)

axList[4].legend()
axList[4].legend(axList[4].get_legend_handles_labels()[0], [cmt.labelSynchrony, 'Network frequency', ('Neuron f' + cmt.labelFiringRate[1:-6])], frameon = False)

tickList = axList[4].get_xticks().tolist()
axList[4].set_xticks( tickList + [0.06])


# Plot all 2D the colorplots 
trajPath = 'Fig03CDE.hdf5'
identifier = 'longRangeDrive4000Prob03'

traj = sw.trajCheck(trajPath, fullName = False)


newFreq2 = traj.res.summary.firingRates.frqMax_frame


ps.twoDplotFig2(traj.res.summary.syncIndexCorr.syncIndexCorr_frame,1,1, identifier = identifier, saveDir = 'Manuscript02', linColorCode = False, ax = axList[5])

ps.twoDplotFig2(newFreq2,2,1, identifier = identifier, saveDir = 'Manuscript02', linColorCode = False, ax = axList[6])#, cLimits = [50, 200])

ps.twoDplotFig2(traj.res.summary.averageFiring.averageFiring_frame,3,1, identifier = identifier, saveDir = 'Manuscript02', linColorCode = False, ax = axList[7])#, cLimits = [50, 200])

axList[6].set_xlabel(cmt.labelPassive)
axList[5].set_ylabel(cmt.labelActive) 

for axis in axList[-5:-2]:

    axis.set_xticks([0, 5, 10])
    axis.set_yticks([0, 5, 10])
    
    axis.set_xticklabels(['0', '1', '2'])
    axis.set_yticklabels(['0', '', '0.5'])   
    
    



for axis in [axList[0], axList[1], axList[2], axList[3]]:
    gt.remove_box_cueAll(axis, keepx = False)

for axis in [axList[-1], axList[-2]]:
    gt.remove_box_cueAll(axis, keepx = True, xout = 5)

figName = "Fig02"

if "plotFigures" in locals():
    if plotFigures:
        gt.saveFig(fig, figName, dpi=600)
        gt.saveFig(fig, figName, fileFormat='.png', dpi=600)
else: 
    gt.saveFig(fig, figName, dpi=600)
    gt.saveFig(fig, figName, fileFormat='.png', dpi=600)
#plt.close() 
    
    
#%% Fig 4 Optogenetic experiment 

reload(ps)
rcParams.update({'font.size': 8})
fig = plt.figure(dpi = 600, figsize = cmt.twoThirdRatio)

#make outer gridspec


allPlots = gridspec.GridSpec(1, 3, width_ratios = [1, 2, 1], wspace = .5)

#make nested gridspecs
leftExamples = gridspec.GridSpecFromSubplotSpec(7, 1, subplot_spec = allPlots[0], hspace = .1)
centerPlots = gridspec.GridSpecFromSubplotSpec(4, 1,subplot_spec = allPlots[1], hspace = .5)
rightExamples = gridspec.GridSpecFromSubplotSpec(7, 1,subplot_spec = allPlots[2], hspace = .1)


axList = []
for i in [1,2,4,5]:
    
    if i == 1: axList.append(fig.add_subplot(leftExamples[i]))
    else: axList.append(fig.add_subplot(leftExamples[i], sharex = axList[0]))

for i in [1,2,4,5]:
    axList.append(fig.add_subplot(rightExamples[i], sharex = axList[0]))

for i in [1, 0, 2, 3]:
#    axList.append(fig.add_subplot(centerPlots[i]))
    axList.append(fig.add_subplot(centerPlots[i]))


    
    
    
# load data    
LightsOnGJ = sw.trajCheck('Fig04.hdf5');


minFreq = 10
onsetOscillation = 0.1 #How to define this barrier in a more systematic way?

traj = LightsOnGJ
transposeAll = True
gaussSmoothing = True
smothingWidth = 10

identifier = ''
plotStyle = ['--','-']

# extract data from traj
if transposeAll == True:

    syncIndex= traj.res.summary.syncIndexCorr.syncIndexCorr_frame.T
    netFreq = traj.res.summary.firingRates.frqMax_frame.T
    averageFiring = traj.res.summary.averageFiringActive.averageFiringActive_frame.T
    
    # Last point is at 0 since zero active neurons means all neurons are active...
    syncIndex[200] = syncIndex[0]
    syncIndex = syncIndex.drop([0], axis=1)
    
    netFreq[200] = netFreq[0]
    netFreq = netFreq.drop([0], axis=1)
    
    averageFiring[200] = averageFiring[0]
    averageFiring = averageFiring.drop([0], axis=1)
else:
    syncIndex= traj.res.summary.syncIndex.syncIndex_frame
    netFreq = traj.res.summary.syncIndexCorr.syncIndexCorr_frame
    averageFiring = traj.res.summary.averageFiringActive.averageFiringActive_frame
    
    
    syncIndex[200] = syncIndex[0]
    syncIndex.drop([0], axis=1)
    
    netFreq[200] = syncIndex[0]
    netFreq.drop([0], axis=1)
    
    averageFiring[200] = syncIndex[0]
    averageFiring.drop([0], axis=1)


frameRobust = averageFiring.copy()
frameFrq = averageFiring.copy()

# calculate the oscillation strenght
# since it was not calculated directly in the 
# initial simulations 

for run_name in traj.f_get_run_names():
    print run_name
    traj.f_set_crun(run_name)

    spectrumValues = traj.res.crun.derived.spectrumValues
    spectrumFreqs = traj.res.crun.derived.spectrumFreqs
    maxValue, maxArg = ps.getOscillationStrengthFirstPos(spectrumFreqs[spectrumFreqs > minFreq], spectrumValues[spectrumFreqs > minFreq], windowSize = smothingWidth)
    
    
    if traj.parameters.neuron.excDrive.spotLight == 0:
        frameRobust.loc[traj.parameters.neuron.gapJunctions.gj_pconnect, 200] = maxValue
        frameFrq.loc[traj.parameters.neuron.gapJunctions.gj_pconnect, 200] = maxArg
        
    else: 
        frameRobust.loc[traj.parameters.neuron.gapJunctions.gj_pconnect, traj.parameters.neuron.excDrive.spotLight] = maxValue
        frameFrq.loc[traj.parameters.neuron.gapJunctions.gj_pconnect, traj.parameters.neuron.excDrive.spotLight] = maxArg
#


frameRobust.values[0] = gaussian_filter(frameRobust.values[0].astype('float'), 3)
frameRobust.values[1] = gaussian_filter(frameRobust.values[1].astype('float'), 3)

print "This is it", frameRobust.max().max()

maxValueOnceAndForAll = frameRobust.max().max() 
frameRobust = frameRobust/frameRobust.max().max()


frameFrqOnset = frameFrq[frameRobust > onsetOscillation]
#frameFrqOnset = frameFrq[frameRobust > onsetOscillation]

with pd.option_context('display.max_rows', 999, 'display.max_columns', 3):
    print frameFrqOnset.T

withGapOnset = ps.findOscillationOnset(frameFrqOnset.iloc[1])
withoutGapOnset = ps.findOscillationOnset(frameFrqOnset.iloc[0])

# filter
averageFiring.values[0] = gaussian_filter(averageFiring.values[0].astype('float'), 3)
averageFiring.values[1] = gaussian_filter(averageFiring.values[1].astype('float'), 3)

syncIndex.values[0] = gaussian_filter(syncIndex.values[0].astype('float'), 3)
syncIndex.values[1] = gaussian_filter(syncIndex.values[1].astype('float'), 3)

frameFrq.values[0] = gaussian_filter(frameFrq.values[0].astype('float'), 3)
frameFrq.values[1] = gaussian_filter(frameFrq.values[1].astype('float'), 3)

frameFrq.iloc[1][frameFrq.columns < withGapOnset] = None
frameFrq.iloc[0][frameFrq.columns < withoutGapOnset] = None

freqWithoutGap = gaussian_filter(frameFrqOnset.T.iloc[frameFrqOnset.columns > (withoutGapOnset + 4), 0].values.astype('float'), 3)
freqWithGap = gaussian_filter(frameFrqOnset.T.iloc[frameFrqOnset.columns > (withGapOnset + 4), 1].values.astype('float'), 3)


frameFrqOnset.T.iloc[frameFrqOnset.columns > (withoutGapOnset + 4), 0] = freqWithoutGap
frameFrqOnset.T.iloc[frameFrqOnset.columns > (withGapOnset + 4), 1] = freqWithGap


# uncomment if you wanna the values of the dataframes
#with pd.option_context('display.max_rows', 999, 'display.max_columns', 3):
#    print netFreq.T
#
#with pd.option_context('display.max_rows', 999, 'display.max_columns', 3):
#    print frameFrqOnset.T



axList[-2].set_ylabel(cmt.labelSynchronyShort)

syncIndex.T.plot(c = cmt.colorSynchrony, subplots=False, ax=axList[-2], legend=False, style = plotStyle)


axList[-4].set_ylabel(cmt.labelNetworkFrequencyShort)
frameFrqOnset.T.plot(c = cmt.colorNetworkFrequency, subplots=False, ax=axList[-4], legend=False, style = plotStyle)



axList[-4].plot(frameFrq.T.index[withGapOnset], frameFrq.iloc[1, withGapOnset + 4], '*', markersize=12, c = cmt.colorNetworkFrequency)
axList[-4].plot(frameFrq.T.index[withoutGapOnset], frameFrq.iloc[0, withoutGapOnset + 4], '*', markersize=12, c = cmt.colorNetworkFrequency)


axList[-1].set_ylabel(cmt.labelFiringRateShort)
averageFiring.T.plot(c = cmt.colorFiringRate, subplots=False, ax=axList[-1], legend=False, style = plotStyle)

axList[-3].set_ylabel(cmt.labelOscillationStrengthShort)
frameRobust.T.plot(c = cmt.colorOscillationStrength, subplots=False, ax=axList[-3], legend=False, style = plotStyle)



# some makeup...
for axis in [axList[-4], axList[-3], axList[-2]]:
    gt.remove_box_cue(axis, keepx = False)
    axis.locator_params(axis = 'y', nbins = 3)
    axis.locator_params(axis = 'x', nbins = 3)
    axis.set_xlim([0, 200])


for axis in [axList[-1]]:
    gt.remove_box_cue(axis, keepx = True, xout = 5)
    axis.locator_params(axis = 'y', nbins = 3)
    axis.locator_params(axis = 'x', nbins = 3)
    axis.set_xlim([0, 200])


onsets = [56, 80]
rawList = ['051', '247', '075', '271'] # for 4000 drive an 0.1

# extract spike trains
spikeTrain = []
for i, each in enumerate(rawList):
    run_name = 'run_00000' + each
    traj.f_set_crun(run_name)
    spikeTrain.append(traj.res['run_00000' + str(each)].raw.spikeTrainsNeurons)

# spiking activities
    
markersize = 3.0    
endTime = 0.388
startTime = 0.3525
rasterized = True

ps.rasterPlotSingle(spikeTrain[0], endTime, number_neuron = onsets[0] - 1, ccode = cmt.colorWithOutGJ, markersize = markersize, showTime = startTime, ax = axList[0],rasterized = rasterized)
ps.rasterPlotSingle(spikeTrain[1], endTime, number_neuron = onsets[0] - 1, ccode = cmt.colorWithGJ, markersize = markersize, showTime = startTime, ax = axList[2],rasterized = rasterized)
ps.rasterPlotSingle(spikeTrain[2], endTime, number_neuron = onsets[1] - 1, ccode = cmt.colorWithOutGJ, markersize = markersize, showTime = startTime, ax = axList[4],rasterized = rasterized)
ps.rasterPlotSingle(spikeTrain[3], endTime, number_neuron = onsets[1] - 1, ccode = cmt.colorWithGJ, markersize = markersize, showTime = startTime, ax = axList[6],rasterized = rasterized)



# plot spike network activity

sigma_krnl = 5.0

timesFR, rates = gt.returnTimeResolvedFiringRatesInMsWindows(spikeTrain[0], 1.0, scaleFactor = 0.001, timeFactor = 10000)
axList[1].plot(timesFR * 1000.0, gaussian_filter(rates, sigma_krnl) , c = cmt.colorWithOutGJ)

timesFR, rates = gt.returnTimeResolvedFiringRatesInMsWindows(spikeTrain[1], 1.0, scaleFactor = 0.001, timeFactor = 10000)
axList[3].plot(timesFR * 1000.0, gaussian_filter(rates, sigma_krnl), c = cmt.colorWithGJ)

timesFR, rates = gt.returnTimeResolvedFiringRatesInMsWindows(spikeTrain[2], 1.0, scaleFactor = 0.001, timeFactor = 10000)
axList[5].plot(timesFR * 1000.0, gaussian_filter(rates, sigma_krnl), c = cmt.colorWithOutGJ)

timesFR, rates = gt.returnTimeResolvedFiringRatesInMsWindows(spikeTrain[3], 1.0, scaleFactor = 0.001, timeFactor = 10000)
axList[7].plot(timesFR * 1000.0, gaussian_filter(rates, sigma_krnl), c = cmt.colorWithGJ)


# same makeup... 

axList[3].set_xlabel(cmt.labelTimeMs)
axList[7].set_xlabel(cmt.labelTimeMs)


for axis in [axList[0], axList[2], axList[4], axList[6]]:
    gt.remove_box_cueAll(axis, keepx = False)

for axis in [axList[1], axList[3], axList[5], axList[7]]:
    gt.remove_box_cue(axis, keepx = True, xout = 5)
    axis.locator_params(axis = 'y', nbins = 3)
    axis.set_ylim([-1, 42])
    axis.set_ylabel('FR (10 3 Hz)')
    
axList[-1].set_xlabel('Excited neurons')
axList[-4].set_ylim([150, 300.0])
axList[-2].set_ylim([0.0, 0.6])
axList[-3].set_ylim([-0.05, 1.05])
axList[-1].set_ylim([80, 300.0])
axList[-3].plot([0, 200], [onsetOscillation, onsetOscillation], 'k', linestyle = ':', linewidth = 1.0)


figName = "Fig03"

if "plotFigures" in locals():
    if plotFigures:
        gt.saveFig(fig, figName, dpi=600)
        gt.saveFig(fig, figName, fileFormat='.png', dpi=600)
else: 
    gt.saveFig(fig, figName, dpi=600)
    gt.saveFig(fig, figName, fileFormat='.png', dpi=600)

#%% Fig 5 Multicompartment analysis

#==============================================================================
# Plot and compare data for attenuation and delay
#==============================================================================


reload(ps)
reload(cmt)

# Load model to be able to access the methods of the model...
model = pih.configSimulation(cellModel = "Lee")
rcParams.update({'font.size': 8})
fig = plt.figure(dpi = 600, figsize = (2.2, 5.07))

#make outer gridspec


allPlots = gridspec.GridSpec(4, 1, hspace = .5, wspace = 0.6)
axList = []

for i in [1,2,3]:
    
    if i == 0:
        print "nothing"
    else: axList.append(fig.add_subplot(allPlots[i]))





# Load the data
store = pd.HDFStore("./data/Fig05.h5", mode = "r")

loadName = '2NeuronsDelayLeeDataDelay'
NeuronsDelayLeeDataDelay = store['/{}.dat'.format(loadName)]
NeuronsDelayLeeDataDelay = NeuronsDelayLeeDataDelay.loc[:,NeuronsDelayLeeDataDelay.columns.str.contains("bcdendAP1")]


loadName = '2NeuronsDelaySaudargieneDataDelay'
NeuronsDelaySaudargieneDataDelay = store['/{}.dat'.format(loadName)]
NeuronsDelaySaudargieneDataDelay = NeuronsDelaySaudargieneDataDelay.loc[:,NeuronsDelaySaudargieneDataDelay.columns.str.contains("rad.1|l..1")]


NeuronsDelayLeeDataDelaydualAP = store["/2NeuronsDelayMaxMaxLeeDualAPPassive"]
NeuronsDelayLeeDataDelaydualAP = NeuronsDelayLeeDataDelaydualAP.loc[:,NeuronsDelayLeeDataDelaydualAP.columns.str.contains("bcdendAP1")]


NeuronsDelaySaudargieneDataDelaydualAP = store["/2NeuronsDelayMaxMaxSaudargieneDualAPPassive"]
NeuronsDelaySaudargieneDataDelaydualAP = NeuronsDelaySaudargieneDataDelaydualAP.loc[:,NeuronsDelaySaudargieneDataDelaydualAP.columns.str.contains("rad.1|l..1")]


model.plotDataAll(NeuronsDelayLeeDataDelaydualAP, ylabel = "Delay (ms)", plotstyle = ":", fig = fig, ax = axList[0], color = 4)
model.plotDataAll(NeuronsDelaySaudargieneDataDelaydualAP, ylabel = "Delay\npeaks(ms)", plotstyle = ":", fig = fig, ax = axList[0], color = 2)

model.plotDataAll(NeuronsDelayLeeDataDelay, ax = axList[0], ylabel = "Delay (ms)", plotstyle = "-", color = 4)
model.plotDataAll(NeuronsDelaySaudargieneDataDelay, ylabel = "Delay (ms)", plotstyle = "-", fig = fig, ax = axList[0], color = 2)


loadName = '2NeuronsDelayLeeDataDelayMaxRise'
NeuronsDelayLeeDataDelay = store['/{}.dat'.format(loadName)]
NeuronsDelayLeeDataDelay = NeuronsDelayLeeDataDelay.loc[:,NeuronsDelayLeeDataDelay.columns.str.contains("bcdendAP1")]

loadName = '2NeuronsDelaySaudargieneDataDelayMaxRise'
NeuronsDelaySaudargieneDataDelay = store['/{}.dat'.format(loadName)]
NeuronsDelaySaudargieneDataDelay = NeuronsDelaySaudargieneDataDelay.loc[:,NeuronsDelaySaudargieneDataDelay.columns.str.contains("rad.1|l..1")]

NeuronsDelayLeeDataDelaydualAP = store["/2NeuronsDelayMaxRiseLeeDualAPPassive"]
NeuronsDelayLeeDataDelaydualAP = NeuronsDelayLeeDataDelaydualAP.loc[:,NeuronsDelayLeeDataDelaydualAP.columns.str.contains("bcdendAP1")]


NeuronsDelaySaudargieneDataDelaydualAP = store["/2NeuronsDelayMaxRiseSaudargieneDualAPPassive"]
NeuronsDelaySaudargieneDataDelaydualAP = NeuronsDelaySaudargieneDataDelaydualAP.loc[:,NeuronsDelaySaudargieneDataDelaydualAP.columns.str.contains("rad.1|l..1")]


model.plotDataAll(NeuronsDelayLeeDataDelaydualAP, ylabel = "Delay (ms)", plotstyle = ":", fig = fig, ax = axList[1], color = 4)
model.plotDataAll(NeuronsDelaySaudargieneDataDelaydualAP, ylabel = "Delay\nmax rise(ms)", plotstyle = ":", fig = fig, ax = axList[1], color = 2)

model.plotDataAll(NeuronsDelayLeeDataDelay, ax = axList[1], ylabel = "Delay (ms)", plotstyle = "-", color = 4)
model.plotDataAll(NeuronsDelaySaudargieneDataDelay, ylabel = "Delay (ms)", plotstyle = "-", fig = fig, ax = axList[1], color = 2)


axList[0].set_xlim(xmin = 0.0)
axList[2].set_xlim(xmin = 0.0)



loadName = 'APattenuationLeeDataAttentuation'
APattenuationLeeDataAttentuation = store['/{}.dat'.format(loadName)]


loadName = 'APattenuationSaudargieneDataAttentuation'
APattenuationSaudargieneDataAttentuation = store['/{}.dat'.format(loadName)]



model.plotDataAll(APattenuationLeeDataAttentuation.iloc[:,1:], ylabel = "Fraction AP", plotstyle = "-", ax = axList[2], color = 4)
model.plotDataAll(APattenuationSaudargieneDataAttentuation.iloc[:,1:], ylabel = "Max GJ potential (mV)", plotstyle = "-", fig = fig, ax = axList[2], color = 2)

axList[1].legend()
handles, labels = axList[1].get_legend_handles_labels()

labels = [u'Lee et al.', u'Saudargiene et al']
axList[1].legend([handles[0], handles[1]], labels, frameon = False)
axList[1].autoscale()
axList[2].set_xlim(xmin = 0.0)



axList[0].set_ylim([0, 4.0])



for axis in [axList[0], axList[1]]:
    gt.remove_box_cue(axis, keepx = False)
    axis.set_ylim([0, 4.0])
    axis.locator_params(axis = 'x', nbins = 3)
    axis.locator_params(axis = 'y', nbins = 8)

for axis in [axList[2]]:
    gt.remove_box_cue(axis, keepx = True, xout = 5)
    axis.set_ylim(ymin = 0.0)
    axis.locator_params(axis = 'x', nbins = 3)
    axis.locator_params(axis = 'y', nbins = 6)




figName = "Fig04"

if "plotFigures" in locals():
    if plotFigures:
        gt.saveFig(fig, figName)
        gt.saveFig(fig, figName, fileFormat='.png', dpi=300)
else: 
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat='.png', dpi=300)




#%% Fig 6 Effect of delay on the network


reload(ps)
reload(cmt)

rcParams.update({'font.size': 8})
fig = plt.figure(dpi = 600, figsize = cmt.flat)

#make outer gridspec


allPlots = gridspec.GridSpec(2, 3, height_ratios = [5, 3], hspace = .2, wspace = 0.4)
axList = []

i = 0

for gs in allPlots:
    i+=1 
    axList.append(fig.add_subplot(gs))
        

plotStyle = ['--',':','-']
trajPath = 'Fig06.hdf5'

identifier = ''

traj = sw.trajCheck(trajPath, fullName = False)

newFreq2 = traj.res.summary.firingRates.frqMax_frame

syncIdx = traj.res.summary.syncIndexCorr.syncIndexCorr_frame


# For the single plots...
saveDir = "ProgressReport"
linColorCode = False

# Rescaling of the connection probability to the whole connection probability



syncIdx.index = syncIdx.index / 5.0
newFreq2.index = newFreq2.index / 5.0
traj.res.summary.averageFiring.averageFiring_frame.index = traj.res.summary.averageFiring.averageFiring_frame.index / 5.0


ps.twoDplotFig2(syncIdx,1,5, figLetter = '', identifier = identifier, saveDir = saveDir, linColorCode = linColorCode, ax = axList[0])
ps.twoDplotFig2(newFreq2,2,5, figLetter = '', identifier = identifier, saveDir = saveDir, linColorCode = linColorCode, ax = axList[1])
ps.twoDplotFig2(traj.res.summary.averageFiring.averageFiring_frame,3,5, figLetter = '', identifier = identifier, saveDir = saveDir, linColorCode = linColorCode,  ax = axList[2])


# Plot examples...
exampleIdxList = [0, 6, -1]

syncIdx.iloc[exampleIdxList].T.plot(c = cmt.colorSynchrony, subplots=False, ax=axList[3], legend=False, style = plotStyle)
newFreq2.iloc[exampleIdxList].T.plot(c = cmt.colorNetworkFrequency, subplots=False, ax=axList[4], legend=False, style = plotStyle)
traj.res.summary.averageFiring.averageFiring_frame.iloc[exampleIdxList].T.plot(c = cmt.colorFiringRate, subplots=False, ax=axList[5], legend=False, style = plotStyle)


# makeup

for axis in axList[:3]:
    axis.set_xticks([])
    axis.locator_params(axis = 'y', nbins = 3)
    axis.set_yticks([0, 6, 12])
    axis.set_yticklabels(syncIdx.index[[0,6,12]].tolist())
    axis.plot([0, 24], [0, 0], '0.4', linestyle = plotStyle[0], linewidth = 0.5)
    axis.plot([0, 24], [6, 6], '0.4', linestyle = plotStyle[1], linewidth = 0.5)
    axis.plot([0, 24], [12, 12], '0.4', linestyle = plotStyle[2], linewidth = 0.5)

axList[3].set_ylim([0,1.0])
axList[4].set_ylim([0,250.0])
axList[5].set_ylim([0,250.0])


axList[3].set_ylabel(cmt.labelSynchronyShort)
axList[4].set_ylabel(cmt.labelNetworkFrequencyShort)
axList[5].set_ylabel(cmt.labelFiringRateShort)

axList[4].set_xlabel(cmt.labelDelay)
axList[0].set_ylabel(cmt.labelConnectionProbabilityShort)

for axis in axList[3:]:
    gt.remove_box_cue(axis, keepx = True, xout = 5)
    axis.locator_params(axis = 'x', nbins = 3)
    axis.locator_params(axis = 'y', nbins = 3)



figname = "Fig05"

if "plotFigures" in locals():
    if plotFigures:
        gt.saveFig(fig, figname)
        gt.saveFig(fig, figname, fileFormat='.png', dpi=300)
else: 
    gt.saveFig(fig, figname)
    gt.saveFig(fig, figname, fileFormat='.png', dpi=300)


