# !/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np

forExploration = {'gj_spike': np.arange(0.0, 0.51, 0.05).tolist(), 'gj_passive' : np.arange(0.0, 2.1, 0.2).tolist()}


modelParas = { 'g_ii': 5.0,
	       'spr_ii':0.2,
               'gj_pconnect' : 0.3,
               'gj_cluster_size' : 20,
               'gj_passive' : 0.5,
               'gj_spike' : 0.15,
               'gj_delay' : 0.0,
               'gj_passive_Sigma' : 0.0,
               'gj_spike_Sigma' : 0.0,
               'poissonDrive' : 4000,
               'driveSigma' : 0.0,
               'spotLight' : 0 , 
               'sigma_burst' : 0,
               'n_synapses' : 800,
               'spr_input' : 0.1,
               'tl_ii': 1.0,
               'tr_ii': 0.45,
               'td_ii': 1.2,
	       'I_mean': 0,
	       'I_std':0
               }

simulationTime = 1.0
CoresToUse = 50
gitMessage = 'Gap junction parameters for longer Range'
commentTraj = gitMessage
trajectoryPath = './pypet/'
gitPath = '.'

qFactorPlot = False
longSim = True
qFactorCalculation = False


    #traj.f_explore( { 'neuron.input.Sigma_Gauss': np.arange(0, 1, 0.5).tolist() } )
    #traj.f_explore( { 'poissonDrive': np.arange(1000, 6005, 1000).tolist() } )
    #traj.f_explore( cartesian_product({'driveSigma':np.arange(0.0, 7.6, 0.5).tolist(), 'gj_pconnect':[0.0, 1.0], 'poissonDrive': [6000,2000]}, ('driveSigma', ('gj_pconnect', 'poissonDrive'))))
    #traj.f_explore( cartesian_product({'spotLight':np.arange(5, 20, 5).tolist() , 'gj_pconnect':[0.0, 1.0]}))
    #traj.f_explore( cartesian_product({'gj_cluster_size':[20,40], 'gj_pconnect':[0.0,1.0]}))

    #traj.f_explore({'gj_delay':np.arange(0, 2.0, 0.1).tolist()}) 
