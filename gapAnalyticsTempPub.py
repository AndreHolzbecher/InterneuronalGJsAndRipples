# !/usr/bin/env python
# -*- coding: utf-8 -*-
# This module should be renamed


# =============================================================================
# This file contains most of the analysis functions 
# for the simulations 
# =============================================================================



import numpy as np
from matplotlib import pyplot as pp
import cPickle as cp
import scipy.optimize
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy import asarray as ar,exp
from scipy.signal import spectrogram
from brian import ms, mV, second,siemens,pF,mA,amp,nA,volt
import time
import os
from math import log10, floor, ceil
from scipy.ndimage.filters import gaussian_filter
from scipy.signal import butter, lfilter
import random as rn
from scipy import fft, arange, signal


#Some general parameters

ylabel1 = 'Net. freq. (Hz)'
colNetFreq = '#4073EB'
ylabel2 = 'Coherence'
colSync = '#4EB628'
colSyncStrong = '#247507'
ylabel3 = 'Firing rate (Hz)'

colFiringRateStrong = '#efa30b'
colFiringRate = '#F3B841'



def data4colorplot(data, datalabel):
    out = []
    steps = data[0][2]


    for each in range(steps):
	out.append(data[datalabel][each:501:steps])
    return out



def colorplot(path, datatype, xAr, yAr, cont=0):
    dataRaw = cp.load(open(path, 'rb'))
    if datatype == 0:
	datalabel = 4
	colorBarlabel = 'Network frequency in Hz'
    else:
	datalabel = 5
	colorBarlabel = 'Synchrony index'
    data = data4colorplot(dataRaw, datalabel)
    xMesh, yMesh = np.meshgrid(xAr, yAr)
    fig = pp.figure(facecolor='white')
    pl = fig.add_subplot(111)
    plot = pp.pcolormesh(xMesh,yMesh,np.array(data), cmap='gray')
    colorBar = pp.colorbar(plot)
    pp.ylabel('Connection probability')
    pp.xlabel('Cluster size in #')
    colorBar.set_label(colorBarlabel)

    if cont == 1:
	con = pp.contour(xMesh,yMesh,data, 4, cmap=pp.cm.Greens)

    return data, plot, cont

def multiplot(path,  datatype=0, steps = 51):
    data = cp.load(open(path, 'rb'))
    fig = pp.figure(facecolor='white')
    #xlist = data[0].tolist()

    if datatype == 0:
	datalabel = 4
	colorBarlabel = 'Network frequency in Hz'
    else:
	datalabel = 5
	colorBarlabel = 'Synchrony index'

    N = data[0][1]
    average = np.array([0]*N)
    averageVar = np.array([0]*N)
    pl = fig.add_subplot(111)


    for each in range(data[0][3]):
	print
	pp.plot(data[2][each*steps:(each+1)*steps:1], data[datalabel][each*steps:(each+1)*steps:1], c='#DFDFF2')
	average = average + np.array(data[datalabel][each*steps:(each+1)*steps:1])

    average = average/float(data[0][3])

    for each in range(data[0][3]):
	averageVar = averageVar + (average - np.array(data[datalabel][each*steps:(each+1)*steps:1]))**2


    averageVar = np.sqrt(averageVar)/float(data[0][3])
    pp.plot(data[2][0:steps:1], average, c='#8784F0', lw = 2.0)
    pp.fill_between(data[2][0:steps:1],(average - averageVar),(average + averageVar), alpha = 0.30, facecolor ='#8784F0')
    pp.ylabel(colorBarlabel)
    pp.title(path)
    pp.xlabel('Passive conductance in nS')
    pl.locator_params(axis = 'x', nbins = 6, tight=True)
    pl.locator_params(axis = 'y', nbins = 6, tight=True)
    return None

def colorplot2(path, datatype, xAr, yAr, cont=0):
    dataRaw = cp.load(open(path, 'rb'))
    if datatype == 0:
	datalabel = 4
	colorBarlabel = 'Network frequency in Hz'
    else:
	datalabel = 5
	colorBarlabel = 'Synchrony index'

    datarev = data4colorplot(dataRaw, datalabel)
    data = datarev[::-1]
    xMesh, yMesh = np.meshgrid(xAr, yAr)
    fig = pp.figure(facecolor='white')
    pl = fig.add_subplot(111)
    plot = pp.imshow(np.array(data), cmap='gray', extent=(1.0,49.0,0.05,1.0), aspect = 'auto', interpolation='gaussian')
    colorBar = pp.colorbar(plot)
    pp.ylabel('Connection probability')
    pp.xlabel('Cluster size in #')
    colorBar.set_label(colorBarlabel)

    if cont == 1:
	con = pp.contour(xMesh,yMesh,data, 4, cmap=pp.cm.Greens)

    return data, plot, cont

def gaus(x,a,x0,sigma):
    return a*exp(-(x-x0)**2/(2*sigma**2))


def overview_plot(output, rep=1):

    for each in range(rep):
	#model.simulate(simtime, 200)
	#output=model.out()
	fig = pp.figure(figsize= (9,3))
	#pl1 = fig.add_subplot(311)s
	#pp.title('Network activity')
	#pp.plot(np.array(range(len(output.smooth_rate(sigma_krnl=30))))/100.0,output.smooth_rate(sigma_krnl=30))
	#string = 'Pshare  ' + str(model.spr_input) + '\nGJ spike  ' + str(model.gj_spike) + '\nGJ passive  ' + str(model.gj_passive) + '\nGJ connect  '+ str(model.gj_pconnect)
	#pl1.annotate(string,(0.15,0.2),textcoords='figure fraction')
	#pp.ylabel('Pop. rate in spikes/s')
	#pp.xlabel('Time in ms')
	#pp.subplot(323)
	#pp.hist(output.spike_stats()[0], 25, range=[0,250])
	#pp.ylabel('Number of neurons')
	#pp.xlabel('Frequency in Hz')
	pp.subplot(211)
	(frqMax, syncIndex, frq, frqY) = (output.net_freq()[0],output.net_freq()[1], output.net_freq()[2], output.net_freq()[3])
	frqYPlot = np.real(frqY[0:len(frq[frq < 250]):1])
	frqXPlot = frq[frq < 250]
	pp.plot(frq[frq < 250],np.real(frqY[0:len(frq[frq < 250]):1]))
	syncI = 'SyncIndex = ' + str(syncIndex)
	fmax ='fmax = ' + str(frqMax) + ' Hz'
	startIndex = len(frq)-len(frq[frq>30])
	frqMaxPos = np.real(frqY[startIndex::]).argmax()
	frqMaxY = np.real(frqY[startIndex::][frqMaxPos])
	pp.annotate((syncI + '\n' + fmax),(frqMax,frqMaxY*1.3),textcoords='data',horizontalalignment='right')
	pp.ylabel('Power in a.u.')
	pp.xlabel('Frequency in Hz')

	##fit gaussian
	##define fitfct

	x = frqXPlot[frqMax-5: frqMax+5]
	y = frqYPlot[frqMax-5: frqMax+5]

	n = len(x)                          #the number of data
	mean = sum(x*y)/n                   #note this correction
	sigma = np.sqrt(sum(y*(x-mean)**2)/n)        #note this correction

	popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])

	pp.subplot(212)
	pp.plot(x,y,'b+:',label='data')
	pp.plot(x,gaus(x,*popt),'ro:',label='fit')

    return None


#plot_spikes plots a rasterplot of spike trains. Further, it gives the overall activity and optional the synchrony index of the network.

def plot_spikes(output, simtime, number_neuron = 100.0, sync = 1, ccode = colNetFreq, figName = 'Test'):


    spikes = output.spiketimes #getting the spiketimes



    N1 = np.where(spikes == number_neuron) #gives the number spike count

    N1Max = spikes[-1][1]

    if number_neuron > N1Max:
        print 'Sorry, we do not have so many neurons'
        print 'Using Nmax = %f instead' %(N1Max)

        N = len(spikes)-1


    else: N = int(N1[0][-1])

    x = np.array([0.0]*N)
    y = np.array([0.0]*N)
    each = 0;
    for each in range(N): #extremly costly way to generate the wanted spike train
	x[each] = spikes[each][0]*1000
	y[each] = (spikes[each][1])/5.0
	each+=1

    if sync == 1: #decide for layout of the plot
      #fig = pp.figure(facecolor='white', figsize= (3,6)) #for in an .pdf file
      fig = pp.figure(facecolor='white', figsize= (6,6))
      plotnumber1 = 312
      plotnumber2 = 311
      plotnumber3 = 313
    else:
      #fig = pp.figure(facecolor='white', figsize= (3,4)) #for .pdf
      fig = pp.figure(facecolor='white', figsize= (7,5))
      plotnumber1 = 211
      plotnumber3 = 212

    #Scatter plot for the spikes
    ax = fig.add_subplot(plotnumber1)
    #pp.scatter(x,y, marker='|', c = colNetFreq)
    pp.scatter(x,y, marker = '|', c = ccode, s = 1)
    pp.xlim([0, simtime])
    pp.ylim([max(y),min(y)])
    pp.ylabel('Neurons')
    ax.yaxis.set_ticks([])
    #pp.xlabel('Time in s')
    ax.spines["top"].set_visible(False) #the price of beauty...
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    #pp.axis('off')
    ax.spines["bottom"].set_color('none')
    pp.setp(ax.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'x', nbins = 3)

    #if we want the sync plot...then here it comes
    if sync == 1:

	syncIndex, histoTimes, histoValues, valuesNoZeros, e = spikeTrainSynchrony(output, 0.0005, bins = 1000)
	ax3 = fig.add_subplot(plotnumber2, sharex = ax)
	histoTimes = [x*1000 for x in histoTimes]
	#pp.plot(c,b, '.')
	pp.plot(histoTimes,histoValues,c = '0.5') #bins of fixed neuron number
	t = moving_average(spikeTimesNoZeros,n=300)
	t = [x*1000 for x in t]
	pp.plot(t,moving_average(valuesNoZeros,n=300),ls = '--', c = colSync) #times averages
	pp.xlim([0, simtime])
	pp.ylim([-0.1, 1.2])
	pp.ylabel('Synchrony')
	ax3.yaxis.set_ticks([])
	#pp.xlabel('Time in s')
	ax3.spines["top"].set_visible(False)
	ax3.spines["right"].set_visible(False)
	ax3.spines["left"].set_visible(False)
	ax3.spines["bottom"].set_visible(False)
	ax3.axes.get_xaxis().set_visible(False)
	ax3.xaxis.set_ticks_position('bottom')
	#pp.axis('off')
	ax3.spines["bottom"].set_color('none')
	pp.setp(ax.get_xticklabels(), visible=False)
	print 'Synchrony index = ', syncIndex

    ax2 = fig.add_subplot(plotnumber3, sharex = ax)
    ax2.spines["left"].set_color('none')
    #ax2.tick_params(axis='y', colors='W')
    #ax2.yaxis.set_ticks([0.0, round_to_1(max(output.smooth_rate(sigma_krnl=30)),1)])
    ax2.yaxis.set_ticks([0, 2])
    ax2.spines["left"].set_color('none')
    ax2.yaxis.tick_left()
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    #pp.title('Network activity')
    pp.plot(np.array(range(len(output.smooth_rate(sigma_krnl=30.0))))/100.0,output.smooth_rate(sigma_krnl=30), c = ccode)
    #pp.plot(np.array(range(len(output.smooth_rate(sigma_krnl=0.0))))/100.0,output.smooth_rate(sigma_krnl=0.0))
    pp.ylabel('Activity [a.u.]')
    pp.xlabel('Time [ms]')
    ax2.xaxis.set_ticks_position('bottom')
    ax2.xaxis.set_tick_params(direction='out')
    pp.xlim([0, simtime*1000])
    #pp.ylim(ymin = -0.1)

    pp.ylim([-0.1, 2.3])

    for loc, spine in ax2.spines.items():
	if loc in ['bottom']:
	    spine.set_position(('outward',5)) # outward by 10 points

    fig.tight_layout()
    pp.subplots_adjust(left = 0.12, bottom = 0.13)
    pp.xlabel('Time [ms]')

    ax.get_yaxis().set_label_coords(-0.06,0.5)
    ax2.get_yaxis().set_label_coords(-0.06,0.5)

    pp.rcParams.update({'font.size': 22})
    pp.locator_params(axis = 'x', nbins = 3)
    saveFig(fig, figName)

    #ax.yaxis.set_visible(False)
    #fig.axes.get_yaxis().set_visible(False)
    #pp.axis('off')
    #ax["right"].set_visible(False)
    #ax["top"].set_visible(False)
    #xmin, xmax = ax1.get_xaxis().get_view_interval()
    #ymin, ymax = ax1.get_yaxis().get_view_interval()
    #ax.add_artist(Line2D((xmin, xmax), (ymin, ymin), color='black', linewidth=2))
    #ax.set_frame_on(False)

def testsyn2(model,I_inj_i, I_inj_e = 0, pulse_len=5.0,spk_delay=80.0,tlim=[0,300],legend=False, name = 'no', format = '.pdf'):

    #pp.style.use('dark_background')

    #font = {'size'   : 15}

    #pp.rc('font', **font)

    model.g_ii = 5.0
    model.gj_passive = 0.0
    model.gj_spike = 0.0

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)
    fig  = pp.figure(figsize= (6,4), facecolor='white')
    ax1 = fig.add_subplot(411)
    pp.plot(Mvi.times / ms, Mvi[0] / 1.E-3*volt,'k',lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[0] / 1.E-3*volt,'k',lw = 2.0)
    #pp.plot(Mvi.times / ms, MIi[0] / 1.E-3*volt,'k',lw = 2.0)
    drawspikes(MPiS.spiketimes[0],model.v_thres/1000.0,0.070,'k-')


    pp.ylabel('Presyn. MP [mV]')
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.spines["bottom"].set_visible(False)
    ax1.yaxis.set_ticks_position('left')
    ax1.axes.get_xaxis().set_visible(False)
    pp.setp(ax1.get_xticklabels(), visible=False)

    pp.locator_params(axis = 'y', nbins = 3)




    ax2 = fig.add_subplot(412, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c='#797979',lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[1] / 1.E-3*volt, c='#D2D251',lw = 2.0)
    #pp.plot(Mvi.times / ms, MIi[1] / 1.E-3*volt, c='#D2D251',lw = 2.0)
    ymin1, ymax1 = pp.ylim()
    print ymin1, ymax1
    #pp.text(15,model.v_rest_e+0.1,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')



    pp.xlim(tlim[0],tlim[1])
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    #pp.ylabel('Postsynaptic MP (mV)')
    pp.xlabel('Time (ms)')
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    ax2.spines["bottom"].set_visible(False)
    ax2.yaxis.set_ticks_position('left')
    ax2.axes.get_xaxis().set_visible(False)
    ax2.xaxis.set_tick_params(direction='out')
    ax2.yaxis.set_tick_params(direction='out')
    pp.setp(ax2.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'y', nbins = 4)


    max1 = 1000.0*max(Mvi[1])
    min1 = 1000.0*min(Mvi[1])

    #pp.ylim(ymin1, ymax1)
    model.g_ii = 0.0
    model.gj_passive = 1.0
    model.gj_spike = 0.2

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)

    ax3 = fig.add_subplot(413, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c=colSync,lw = 1.0)


    #pp.text(5,-59.,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')

    pp.xlim(tlim[0],tlim[1])
    #pp.ylim(ymin1, ymax1)
    ax3.spines["top"].set_visible(False)
    ax3.spines["right"].set_visible(False)
    pp.ylabel('Postsyn. MP [mV]')
    pp.xlabel('Time [ms]')
    ax3.spines["top"].set_visible(False)
    ax3.spines["right"].set_visible(False)
    ax3.spines["bottom"].set_visible(False)
    ax3.yaxis.set_ticks_position('left')
    ax3.axes.get_xaxis().set_visible(False)
    pp.setp(ax3.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'y', nbins = 4)



    model.g_ii = 5.0
    model.gj_passive = 1.0
    model.gj_spike = 0.2

    max2 = 1000.0*max(Mvi[1])
    min2 = 1000.0*min(Mvi[1])

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)


    ax4 = fig.add_subplot(414, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt,c=colNetFreq,lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[1] / 1.E-3*volt,c='#4EB628',lw = 2.0)


    #pp.text(15,model.v_rest_e+0.1,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')
    #pp.ylim(ymin1, ymax1)
    pp.xlim(tlim[0],tlim[1])
    ax4.spines["top"].set_visible(False)
    ax4.spines["right"].set_visible(False)
    #pp.ylabel('Postsynaptic MP (mV)')
    #pp.xlabel('Time (ms)')
    ax4.xaxis.set_ticks_position('bottom')
    ax4.yaxis.set_ticks_position('left')
    pp.locator_params(axis = 'y', nbins = 4)
    pp.locator_params(axis = 'x', nbins = 3)

    for loc, spine in ax4.spines.items():
	if loc in ['bottom']:
            spine.set_position(('outward',5)) # outward by 10 points

    pp.xlabel('Time (ms)')
    max3 = 1000.0*max(Mvi[1])
    min3 = 1000.0*min(Mvi[1])

    maxall = max(max1, max2, max3)+0.1
    minall = min(min1, min2, min3)-0.1

    for [axx, text] in [[ax2, 'Chemical syn.'], [ax4,'Chemical +\nElectrical'], [ax3, 'Electrical syn.']]:
	axx.plot([MPiS.spiketimes[0]*1000.0,MPiS.spiketimes[0]*1000.0],[minall,maxall],'k--',linewidth=1.0)
	#axx.text(0.5,-66.0,text)
	axx.yaxis.set_tick_params(direction='out')
    ax2.set_ylim(minall, maxall)
    ax3.set_ylim(minall, maxall)
    ax4.set_ylim(minall, maxall)
    ax1.yaxis.set_tick_params(direction='out')
    ax4.xaxis.set_tick_params(direction='out')


    ax1.get_yaxis().set_label_coords(-0.12,0.5)
    #ax2.get_yaxis().set_label_coords(-0.1,0.5)
    ax3.get_yaxis().set_label_coords(-0.12,0.5)
    #ax4.get_yaxis().set_label_coords(-0.1,0.5)
    #return ax1

    pp.subplots_adjust(left = 0.14)

    if name != 'no':
        saveFig(fig, name)
        saveFig(fig, name, fileFormat = '.png')

    return fig

def testsynCompare(model,I_inj_i, I_inj_e = 0, pulse_len=5.0,spk_delay=80.0,tlim=[0,300],legend=False, name = 'no', format = '.pdf'):

    #pp.style.use('dark_background')

    #font = {'size'   : 15}

    #pp.rc('font', **font)

    model.g_ii = 5.0
    model.gj_passive = 0.0
    model.gj_spike = 0.0

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)
    fig  = pp.figure(figsize= (6,4), facecolor='white')
    ax1 = fig.add_subplot(411)
    pp.plot(Mvi.times / ms, Mvi[0] / 1.E-3*volt,'k',lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[0] / 1.E-3*volt,'k',lw = 2.0)
    #pp.plot(Mvi.times / ms, MIi[0] / 1.E-3*volt,'k',lw = 2.0)
    drawspikes(MPiS.spiketimes[0],model.v_thres/1000.0,0.070,'k-')


    pp.ylabel('Presyn. MP [mV]')
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.spines["bottom"].set_visible(False)
    ax1.yaxis.set_ticks_position('left')
    ax1.axes.get_xaxis().set_visible(False)
    pp.setp(ax1.get_xticklabels(), visible=False)

    pp.locator_params(axis = 'y', nbins = 3)






    ax2 = fig.add_subplot(412, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c='#797979',lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[1] / 1.E-3*volt, c='#D2D251',lw = 2.0)
    #pp.plot(Mvi.times / ms, MIi[1] / 1.E-3*volt, c='#D2D251',lw = 2.0)
    ymin1, ymax1 = pp.ylim()
    print ymin1, ymax1
    #pp.text(15,model.v_rest_e+0.1,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')



    pp.xlim(tlim[0],tlim[1])
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    #pp.ylabel('Postsynaptic MP (mV)')
    pp.xlabel('Time (ms)')
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    ax2.spines["bottom"].set_visible(False)
    ax2.yaxis.set_ticks_position('left')
    ax2.axes.get_xaxis().set_visible(False)
    ax2.xaxis.set_tick_params(direction='out')
    ax2.yaxis.set_tick_params(direction='out')
    pp.setp(ax2.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'y', nbins = 4)


    max1 = 1000.0*max(Mvi[1])
    min1 = 1000.0*min(Mvi[1])


    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend, Gii10 = True)
    ax1.plot(Mvi.times / ms, Mvi[0] / 1.E-3*volt,'r',lw = 1.0)
    ax2.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c='r',lw = 1.0)


    #pp.ylim(ymin1, ymax1)
    model.g_ii = 0.0
    model.gj_passive = 1.0
    model.gj_spike = 0.2



    #pp.plot(Mvi.times / ms, MIgap[0] / 1.E-3*volt,'k',lw = 2.0)
    #pp.plot(Mvi.times / ms, MIi[0] / 1.E-3*volt,'k',lw = 2.0)
    #drawspikes(MPiS.spiketimes[0],model.v_thres/1000.0,0.070,'r-')


    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)

    ax3 = fig.add_subplot(413, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c=colSync,lw = 1.0)


    #pp.text(5,-59.,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')

    pp.xlim(tlim[0],tlim[1])
    #pp.ylim(ymin1, ymax1)
    ax3.spines["top"].set_visible(False)
    ax3.spines["right"].set_visible(False)
    pp.ylabel('Postsyn. MP [mV]')
    pp.xlabel('Time [ms]')
    ax3.spines["top"].set_visible(False)
    ax3.spines["right"].set_visible(False)
    ax3.spines["bottom"].set_visible(False)
    ax3.yaxis.set_ticks_position('left')
    ax3.axes.get_xaxis().set_visible(False)
    pp.setp(ax3.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'y', nbins = 4)

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend, Gii10 = True)
    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt, c = 'r',lw = 1.0)



    model.g_ii = 5.0
    model.gj_passive = 1.0
    model.gj_spike = 0.2

    max2 = 1000.0*max(Mvi[1])
    min2 = 1000.0*min(Mvi[1])

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend)


    ax4 = fig.add_subplot(414, sharex = ax1)#325

    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt,c=colNetFreq,lw = 1.0)
    #pp.plot(Mvi.times / ms, MIgap[1] / 1.E-3*volt,c='#4EB628',lw = 2.0)


    #pp.text(15,model.v_rest_e+0.1,'Pyramid')
    #pp.text(15,model.v_rest+0.1,'Interneuron')
    #pp.ylim(ymin1, ymax1)
    pp.xlim(tlim[0],tlim[1])
    ax4.spines["top"].set_visible(False)
    ax4.spines["right"].set_visible(False)
    #pp.ylabel('Postsynaptic MP (mV)')
    #pp.xlabel('Time (ms)')
    ax4.xaxis.set_ticks_position('bottom')
    ax4.yaxis.set_ticks_position('left')

    (Mvi,Mve, MPiS, MPeS, MIgap, MIi) = model.testsyn2(I_inj_e,I_inj_i,pulse_len=pulse_len,spk_delay=spk_delay,tlim=tlim,legend=legend, Gii10 = True)
    pp.plot(Mvi.times / ms, Mvi[1] / 1.E-3*volt,c = 'r',lw = 1.0)

    pp.locator_params(axis = 'y', nbins = 4)
    pp.locator_params(axis = 'x', nbins = 3)

    for loc, spine in ax4.spines.items():
	if loc in ['bottom']:
            spine.set_position(('outward',5)) # outward by 10 points

    pp.xlabel('Time (ms)')
    max3 = 1000.0*max(Mvi[1])
    min3 = 1000.0*min(Mvi[1])

    maxall = max(max1, max2, max3)+0.1
    minall = min(min1, min2, min3)-0.1

    for [axx, text] in [[ax2, 'Chemical syn.'], [ax4,'Chemical +\nElectrical'], [ax3, 'Electrical syn.']]:
	axx.plot([MPiS.spiketimes[0]*1000.0,MPiS.spiketimes[0]*1000.0],[minall,maxall],'k--',linewidth=1.0)
	#axx.text(0.5,-66.0,text)
	axx.yaxis.set_tick_params(direction='out')
    ax2.set_ylim(minall, maxall)
    ax3.set_ylim(minall, maxall)
    ax4.set_ylim(minall, maxall)
    ax1.yaxis.set_tick_params(direction='out')
    ax4.xaxis.set_tick_params(direction='out')


    ax1.get_yaxis().set_label_coords(-0.12,0.5)
    #ax2.get_yaxis().set_label_coords(-0.1,0.5)
    ax3.get_yaxis().set_label_coords(-0.12,0.5)
    #ax4.get_yaxis().set_label_coords(-0.1,0.5)
    #return ax1

    pp.subplots_adjust(left = 0.14)

    if name != 'no':
        saveFig(fig, name)
        saveFig(fig, name, fileFormat = '.png')

    return fig

def drawspikes(spike_times,thres,ampl,sty,line=1.0):
# arguments must be passed in seconds and volts
    for k in range(len(spike_times)):
        pp.plot([spike_times[k]*1000.0,spike_times[k]*1000.0],[thres*1000.0,(thres+ampl)*1000.0],sty,linewidth=line)

def remove_box_cue(ax,keepx=True, xout = 0):
    for loc, spine in ax.spines.items():
        if loc in ['left']:
            spine.set_position(('outward',0)) # outward by 10 points
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        elif loc in ['bottom']:
            spine.set_position(('outward',xout)) # outward by 10 points
            if not keepx:
                spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    if not keepx: ax.axes.get_xaxis().set_visible(False)
    


def remove_box_cueAll(ax,keepx=True, xout = 0):
    for loc, spine in ax.spines.items():
        if loc in ['right','top', 'left']:
            spine.set_color('none') # don't draw spine
        elif loc in ['bottom']:
            spine.set_position(('outward',xout)) # outward by 10 points
            if not  keepx: spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
#    ax.axes.get_yaxis().set_visible(False)
    ax.yaxis.set_ticks([])
    if not keepx: ax.axes.get_xaxis().set_visible(False)


def addScale(ax, xPos, yLength, unit, yPos = '', distPar = ''):

    from matplotlib.lines import Line2D

    if yPos == '':
        yLow = 0.5 * (ax.get_ylim()[1] - ax.get_ylim()[0])
        print yLow
    else:
        yLow = yPos

    if distPar == '':
        distPar = 0.04

    xPos = xPos * (ax.get_xlim()[1] - ax.get_xlim()[0])


    yHigh = yLength + yLow
    ax.autoscale(enable = False)
    dist = distPar * (ax.get_xlim()[1] - ax.get_xlim()[0])
    ax.text(ax.get_xlim()[0] + xPos - dist, yHigh, str(yHigh - yLow) + ' ' + unit, rotation = 'vertical')

    lineHdl = ax.add_line(Line2D([ax.get_xlim()[0] + xPos, ax.get_xlim()[0] + xPos], [yLow, yHigh], color='k', linewidth = 2.0))
    lineHdl.set_clip_on(False)

def coincidenceMeasure(spikeTime1, spikeTrain2, coincidenceWindow):

    diff = spikeTrain2-spikeTime1
    diffAbs = np.abs(diff)
    if len(diffAbs) != 0:
        if np.min(diffAbs)<= coincidenceWindow:
            return 1, diff[np.argmin(diffAbs)]/2.0 + spikeTime1
    else:       return 0, 0    #if the second spike train is empty there should be no contribution to the synchrony measure. The zero values at zero time are kicked out later in the spikeTrainSynchrony function.

    return 0, diff[np.argmin(diffAbs)]/2.0 + spikeTime1


def coincidenceMeasure2(spikeTime1, spikeTrain2, coincidenceWindow):

    diff = spikeTrain2-spikeTime1
    diffAbs = np.abs(diff)
    if len(diffAbs) != 0:
        if np.min(diffAbs)<= coincidenceWindow:
            return 1, diff[np.argmin(diffAbs)]/2.0 + spikeTime1, 1
    else:       return 0, 0, 0    #if the second spike train is empty there should be no contribution to the synchrony measure. The zero values at zero time are kicked out later in the spikeTrainSynchrony function.

    return 0, diff[np.argmin(diffAbs)]/2.0 + spikeTime1, 1


def coincidenceMeasure3(spikeTime1, spikeTrain2, coincidenceWindow, maxWindow):

    diff = spikeTrain2-spikeTime1
    diffAbs = np.abs(diff)
    
    if len(diffAbs) != 0:
        # contribution to synchrony if there is a spike in the window
        if np.min(diffAbs)<= coincidenceWindow:
            return 1, diff[np.argmin(diffAbs)]/2.0 + spikeTime1, 1
        
        # no contribution to synchrony if there no spike in the whole cycle
        if np.min(diffAbs) > maxWindow:
            return 0, diff[np.argmin(diffAbs)]/2.0 + spikeTime1, 0
        
    # no contribution to synchrony if there is no spike in the spike train
    else:       return 0, 0, 0    #if the second spike train is empty there should be no contribution to the synchrony measure. The zero values at zero time are kicked out later in the spikeTrainSynchrony function.
    
    # contribute zero to the synchrony in all the other cases, i.e. there is a spike within the max window which is ot in the inner coincidence window
    return 0, diff[np.argmin(diffAbs)]/2.0 + spikeTime1, 1


def spikeDistances(spikeTime1, spikeTrain2):

    diff = spikeTrain2-spikeTime1
    diffAbs = np.abs(diff)
    return diffAbs[np.argmin(diffAbs)]


# spikeTrainSynchrony(out, coincidenceWindow, bins = 500) receives input from the model.out() and calculates the synchrony index of the given spike trains.
# Further variables are the cocoincidenceWindow and the in how many bins the histgram show be given out.

def spikeTrainSynchrony(spiketimes1, coincidenceWindow, bins = 500):

    spikeTrains = createSpikeTray2(spiketimes1)
    syncCounter = 0.0
    m = 0
    spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
        for i in range(k+1,len(spikeTrains)):
            for j in range(len(spikeTrains[k])):
                (isSpike, timeSpike) = coincidenceMeasure(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
                syncCounter = syncCounter + isSpike
                m +=1
                spikeTimes[m] = (timeSpike, isSpike)


    spikeTimes.sort() #spikes from all different neurons are sorted
    spikeTimesAr = np.array(spikeTimes)
    [times, values] = np.hsplit(spikeTimesAr, 2)
    valuesNoZeros = values[times>0] # the array is in the beginnig too big, 0's are taken out.
    spikeTimesNoZeros = times[times>0]

    #now the spikes are put together in bins and averages are calculated from those
    ranges = float(max(times)-min(times))
    step = ranges/float(bins)

    histo = [[]]*bins
    histoValues = [0]*bins
    histoTimes = [0]*bins
    counter = 0

    # Create histogram with uniform time windows.That leads to artifacts at the onset of the synchrony since values of 1 are very probable at this border.
    for i in range(bins):
        histo[i]= [x for x in spikeTimesNoZeros if x>=float(i) * step and x<(float(i)+1.0) * step]

        if len(histo[i]) != 0: #avoid dev by 0
           histoValues[i] = np.sum(valuesNoZeros[counter:len(histo[i]) + counter])/float(len(histo[i]))
        else: histoValues[i] = 0
        counter += len(histo[i])
        histoTimes[i] = (i+0.5)*step



    syncIndex = syncCounter/(float(((len(spikeTrains)-1)*np.sum(map(len,spikeTrains))))/2.0)
    return syncIndex, histoTimes, histoValues, valuesNoZeros, spikeTimesNoZeros


def spikeTrainSynchronyIndex(out, coincidenceWindow):

    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
	for i in range(k+1,len(spikeTrains)):
	    for j in range(len(spikeTrains[k])):
		(isSpike, timeSpike) = coincidenceMeasure(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
		syncCounter = syncCounter + isSpike
		m +=1
		spikeTimes[m] = (timeSpike, isSpike)




    syncIndex = syncCounter/(float(((len(spikeTrains)-1)*np.sum(map(len,spikeTrains))))/2.0)
    return syncIndex


def synchronyBrunel2003(output, window):


    allList = [] 
    for each in output.spiketimes2:
    
        allList = allList + list(each)

    allList = np.array(allList)
    
    histo = np.histogram(allList, int(output.simtime / window))[0]

    firingRate = float(len(allList)/(output.simtime)) 
    
    syncIdx = np.correlate(histo, histo, mode = "valid") / firingRate** 2.0
    print firingRate, syncIdx
    return syncIdx[0]  


def spikeTrainSynchronyIndexCorrected(out, coincidenceWindow, averageFiring):

    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0


    for k in range(len(spikeTrains)):
        for i in range(k+1,len(spikeTrains)):
            for j in range(len(spikeTrains[k])):
                (isSpike, timeSpike) = coincidenceMeasure(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
                syncCounter = syncCounter + isSpike
                m +=1





    syncIndex = syncCounter/(float(((len(spikeTrains)-1)*np.sum(map(len,spikeTrains))))/2.0)
    syncIndexTest = syncCounter/m

    syncIndexCorr = syncIndex - coincidenceWindow * averageFiring

    return syncIndex, syncIndexTest, syncIndexCorr


def spikeTrainSynchronyIndexSymmetric(out, coincidenceWindow):


    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    mAdd = 0
    #spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
	for i in range(len(spikeTrains)):
	    for j in range(len(spikeTrains[k])):
		(isSpike, timeSpike, mAdd) = coincidenceMeasure2(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
		syncCounter += isSpike
		m += mAdd
		#spikeTimes[m] = (timeSpike, isSpike)

    # Now we counted the cases as well where k = i, which should !not be included.
    # Compensate for that by substracting the total amout of spikes from syncCounter and m

    A = [len(out.spiketimes2[x]) for x in range(len(out.spiketimes2))]
    allSpikes = np.sum(A)
    syncCounter -= allSpikes
    m -= allSpikes

    if m == 0: syncIndex = 0.0
    else: syncIndex = syncCounter/float(m)
    return syncIndex

def spikeTrainSynchronyIndexSymmetricCorrected(out, coincidenceWindow, averageFiring):


    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    mAdd = 0
    #spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
        for i in range(len(spikeTrains)):
            for j in range(len(spikeTrains[k])):
                (isSpike, timeSpike, mAdd) = coincidenceMeasure2(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
                syncCounter += isSpike
                m += mAdd
                #spikeTimes[m] = (timeSpike, isSpike)

    # Now we counted the cases as well where k = i, which should !not be included.
    # Compensate for that by substracting the total amout of spikes from syncCounter and m

    A = [len(out.spiketimes2[x]) for x in range(len(out.spiketimes2))]
    allSpikes = np.sum(A)
    syncCounter -= allSpikes
    m -= allSpikes

    if m == 0:
        syncIndex = 0.0
        syncIndexCorr = 0.0

    else:
        syncIndex = syncCounter/float(m)
        syncIndexCorr = syncIndex  - 2 * coincidenceWindow * averageFiring #times 2 because of the abs...
    return syncIndex, syncIndexCorr

def SynchronyIndexMaxWindow(out, coincidenceWindow, averageFiring, netFreq):


    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    mAdd = 0
    #spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
        for i in range(len(spikeTrains)):
            for j in range(len(spikeTrains[k])):
                (isSpike, timeSpike, mAdd) = coincidenceMeasure3(spikeTrains[k][j], spikeTrains[i], coincidenceWindow, 0.5 * 1/netFreq)
                syncCounter += isSpike
                m += mAdd
                #spikeTimes[m] = (timeSpike, isSpike)

    # Now we counted the cases as well where k = i, which should !not be included.
    # Compensate for that by substracting the total amout of spikes from syncCounter and m

    A = [len(out.spiketimes2[x]) for x in range(len(out.spiketimes2))]
    allSpikes = np.sum(A)
    syncCounter -= allSpikes
    m -= allSpikes

    if m == 0:
        syncIndex = 0.0
        syncIndexCorr = 0.0

    else:
        syncIndex = syncCounter/float(m)
        syncIndexCorr = syncIndex  - 2 * coincidenceWindow * averageFiring #times 2 because of the abs...
    return syncIndex, syncIndexCorr

def spikeTrainSynchronyIndexSymmetricOld(out, coincidenceWindow):


    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    mAdd = 0
    #spikeTimes = [(0,0)]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):
        for i in range(len(spikeTrains)):
            for j in range(len(spikeTrains[k])):
                (isSpike, timeSpike) = coincidenceMeasure(spikeTrains[k][j], spikeTrains[i], coincidenceWindow)
                syncCounter += isSpike
                m += 1
                #spikeTimes[m] = (timeSpike, isSpike)

    # Now we counted the cases as well where k = i, which should !not be included.
    # Compensate for that by substracting the total amout of spikes from syncCounter and m

    A = [len(out.spiketimes2[x]) for x in range(len(out.spiketimes2))]
    allSpikes = np.sum(A)
    syncCounter -= allSpikes
    m -= allSpikes

    if m == 0: syncIndex = 0.0
    else: syncIndex = syncCounter/float(m)
    return syncIndex




def spikeTrainTimeDistances(out):

    spikeTrains = out.spiketimes2
    syncCounter = 0.0
    m = 0
    spikeTimes = [0]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):

	for i in range(k+1,len(spikeTrains)):

            if len(spikeTrains[i]) != 0:

                for j in range(len(spikeTrains[k])):
                    timeSpike = spikeDistances(spikeTrains[k][j], spikeTrains[i])
                    spikeTimes[m] = timeSpike
                    m +=1


    return spikeTimes[0:m]


def spikeTrainTimeDistancesSingle(out):

    spikeTrains = out.spiketimes2
    m = 0
    spikeTimes = [0]*((max(map(len,spikeTrains))*((len(spikeTrains)))**2)/2)

    for k in range(len(spikeTrains)):


        if len(spikeTrains[k]) != 0:

            for j in range(len(spikeTrains[k])):
                timeSpike = spikeDistances(spikeTrains[k][j], spikeTrains[k])
                spikeTimes[m] = timeSpike
                m +=1


    return spikeTimes[0:m]

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n



#def 2dplot(path, datatype, xAr, yAr, fileNumber=0, cont=0):
def twoDplot(figName, name = 'simulations/20150810/PassiveActiveMapaS=', endName = 'spr0.2.dat', datalabelZ = 8, datalabelX = 1, indexArray = np.arange(0.0,0.21,0.02), cmap='Blues', cLimits = []):
	#indexArray = np.arange(0.0,0.21,0.02)
	#name = 'simulations/20150810/PassiveActiveMapaS='
	#end = 'spr0.2.dat'
	dataOut = []
	#datalabel = 8 # 8 for spike component

	if datalabelZ == 4:
		colorBarlabel = 'Network frequency [Hz]'
		title = 'Network frequency'
	elif datalabelZ == 7:
		colorBarlabel = 'Neuron firing rate [Hz]'
		title = 'Neuron firing rate'
	elif datalabelZ == 8:
		colorBarlabel = 'Synchrony index'
		title = 'Synchrony index'
	else:
		colorBarlabel = 'I really dont know'

	if datalabelX == 2:
		xlabel = 'Passive conductance [nS]'
		ylabel = 'Active spike [mV]'
	elif datalabelX	 == 3:
		xlabel = 'Connection probabilty'
		ylabel = 'Cluster size [#neurons]'
	else:
		xlabel = 'WAAAAAAAA: enter x label here'
		ylabel = 'enter y label here'

	for i in indexArray:

		fullName = name + str(i) + endName
		data = cp.load(open(fullName, 'rb'))

		dataOut.append(data[datalabelZ])
	#xAr = np.arange(0.0,0.23,0.02)
	xAr = list(data[datalabelX])
	xAr.append(data[datalabelX][1] - data[datalabelX][0] + data[datalabelX][-1])
	xAr = np.array(xAr - (data[datalabelX][1] - data[datalabelX][0])/2.0)

	yAr = list(indexArray)
	yAr.append(yAr[1] - yAr[0] + yAr[-1])
	yAr = np.array(yAr - (yAr[1] - yAr[0])/2.0)


	fig = pp.figure()
	ax = fig.add_subplot(1, 1, 1)
	plot = pp.pcolormesh(xAr,yAr,np.array(dataOut), cmap= cmap)
#	ax.pcolormesh(xAr,yAr,np.array(dataOut), cmap= cmap)

	# Now adding the colorbar
	#cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8])
	#print min(dataOut)
	diff = max(max(dataOut)) - min(min(dataOut))
	#mean = (min(min(dataOut)) + max(max(dataOut)))/2
	#diffRound = round_to_1(diff,0)
	#lowTick =
	dataOutAr = np.array(dataOut)
	minData = dataOutAr.min()
	maxData = dataOutAr.max()
	if not cLimits:
	    lowTick = float(magicRound(minData, diff, 0))
	    highTick = float(magicRound(maxData, diff, 1))

	else:
	    [lowTick, highTick] = cLimits
	cb = pp.colorbar(plot, ticks=[lowTick,highTick])
	pp.clim(lowTick,highTick)
	#cb.ax.set_yticklabels([str(lowTick), str(highTick)])
	#colorBar = pp.colorbar(plot)
	cb.set_label(colorBarlabel)

	pp.xlabel(xlabel)
	pp.ylabel(ylabel)

	ax.set_ylim([yAr.min(),yAr.max()])
	ax.set_xlim([xAr.min(),xAr.max()])
	ax.tick_params(direction='out')
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	pp.title(title)
	figName = figName + colorBarlabel

	saveFig(fig, figName)

	return [lowTick, highTick]

def ClusterVsProb(figName, name = 'simulations/20150810/PassiveActiveMapaS=', endName = 'spr0.2.dat', datalabelZ = 8, datalabelX = 3, indexArray = np.arange(0.0,0.21,0.02)):
	#indexArray = np.arange(0.0,0.21,0.02)
	#name = 'simulations/20150810/PassiveActiveMapaS='
	#end = 'spr0.2.dat'
	dataOut = []
	ClusterOut = []
	ProbOut = []

	#datalabel = 8 # 8 for spike component

	if datalabelZ == 4:
		colorBarlabel = 'Network frequency [Hz]'
	elif datalabelZ == 7:
		colorBarlabel = 'Neuron firing rate [Hz]'
	elif datalabelZ == 8:
		colorBarlabel = 'Synchrony index'
	else:
		colorBarlabel = 'I really dont know'

	if datalabelX == 2:
		xlabel = 'Passive conductance [nS]'
		ylabel = 'Active spike [mV]'
	elif datalabelX	 == 3:
		xlabel = 'Connection probabilty'
		ylabel = 'Cluster size [#neurons]'
	else:
		xlabel = 'WAAAAAAAA: enter x label here'
		ylabel = 'enter y label here'

	for i in indexArray:

		fullName = name + str(i) + endName
		data = cp.load(open(fullName, 'rb'))

		dataOut.append(data[datalabelZ])
		ClusterOut.append(data[1])
		ProbOut.append(data[3])






	#dataNew= np.zeros((2,(len(dataOut)*len(dataOut[0]))))
	dataNew= np.zeros((2,len(dataOut),len(dataOut[0])))
	zack = 0


	for each in range(len(dataOut)):
		for element in range(len(dataOut[each])):

			dataNew[0][each][element] = dataOut[each][element]
			dataNew[1][each][element] = ProbOut[each][element]*ClusterOut[each][element]
			#zack+=1
	#return dataNew



	fig = pp.figure()
	ax = fig.add_subplot(1, 1, 1)
	#print dataNew
	for each in range(len(dataOut)):
	    plot = pp.plot(dataNew[1][each], dataNew[0][each],'o')#plot for each clustersize

	pp.show()
	#colorBar = pp.colorbar(plot)
	#colorBar.set_label(colorBarlabel)

	pp.xlabel('Cluster# x Conn. Prob = Av# Neurons')
	pp.ylabel(colorBarlabel)

#	ax.set_ylim([yAr.min(),yAr.max()])
#	ax.set_xlim([xAr.min(),xAr.max()])

	figName = figName + colorBarlabel
	saveFig(fig, figName)

#	if datatype ==0
#	datalabel = 4
#	colorBarlabel = 'Network frequency in Hz'
#	else:
#	datalabel =
#	colorBarlabel = 'Synchrony index'
#	data = data4colorplot(dataRaw, datalabel)
#	xMesh, yMesh = np.meshgrid(xAr, yAr)
#	fig = pp.figure(facecolor='white')
#	pl = fig.add_subplot(111)
#	plot = pp.pcolormesh(xMesh,yMesh,np.array(data), cmap='gray')
##	colorBar = pp.colorbar(plot)
#	pp.ylabel('Connection probability')
#	pp.xlabel('Cluster size in #')
#	colorBar.set_label(colorBarlabel)

#	if cont == 1:
#	con = pp.contour(xMesh,yMesh,data, 4, cmap=pp.cm.Greens)


#	return data, plot, cont
def saveFig(fig, name, fileFormat = '.pdf', dpi = 300, setPath = './figures/', tight = True):
    from os.path import expanduser
    home = expanduser("~")
    if setPath == '':
        today = time.strftime('%Y%m%d')
        if '/' in name:
            dirc2, name  = name.split('/')
            dirc = home + '/PhD/Gap Junctions/figures/' + today + '/' + dirc2 + '/'
        else:
            dirc = home + '/PhD/Gap Junctions/figures/' + today + '/'
        if not os.path.isdir(dirc): os.makedirs(dirc)
        # Change last string to change format of the saved file
        fname1 = dirc + name + fileFormat
        #fnae2 = name + '.svg'
        #fname = 'SweepPConnectMultipleActive' + '.pdf'
    else:
        dirc = setPath
        if not os.path.isdir(dirc): os.makedirs(dirc)

        fname1 = dirc +  name + fileFormat

    if tight == True:
        pp.savefig(fname1, bbox_inches = 'tight', transparent = False, dpi = dpi)
    else:
        pp.savefig(fname1, transparent = False, dpi = dpi)
        #fig.savefig('out.svg', transparent=True
	#pp.savefig(dirc + fname2, bbox_inches='tight', transparent=False, dpi=300)
def round_to_1(x, k):
	xRound = round(x, -int(floor(log10(x))))
	if k == 0:
	    if x-xRound >= 0: return xRound
	    else: return xRound - 10.0**(float(floor(log10(x)))-1.0)
	if k == 1:
	    if x-xRound <= 0: return xRound
	    else: return xRound + 10.0**(float(floor(log10(x)))-1.0)

def magicRound(x, y, k):
	xRound = round(x, -int(floor(log10(y)))+1)
	if k == 0:
	    if x-xRound >= 0: return xRound
	    else: return xRound - 10.0**(float(floor(log10(y)))-1.0)
	if k == 1:
	    if x-xRound <= 0: return xRound
	    else: return xRound + 10.0**(float(floor(log10(y)))-1.0)



def smooth_rate(spiketimes,t_0=0.0,simTime=1.0,sigma_krnl=0, verbose = False):

    t_1 = simTime
    sptimes=spiketimes
    sptimes=sptimes[np.where((sptimes>=t_0)&(sptimes<=t_1))]*100000 #convert time to sample number within range
    sptrain=np.zeros(int((t_1)*100000)+1) #(int(max(sptimes))+1)
    x=np.array(range(len(sptrain)))*1.0 #make it real
    for i in range(len(sptimes)):
        if sigma_krnl:
            sptrain=sptrain+gaussfunc(x,1/(np.sqrt(2*np.pi)*sigma_krnl),sptimes[i],sigma_krnl,0)
        else:
            sptrain[int(sptimes[i])] = sptrain[int(sptimes[i])] + 1

    return sptrain[int((t_0)*100000):int((t_1)*100000+1)] #sptrain[min(sptimes):max(sptimes)]

def smooth_ratePerMs(spiketimes,t_0=0.0,simTime=1.0,sigma_krnl = 25):

    
    # measure that gives us the firing rate for approximately a ms window
    
    SR = smooth_rate(spiketimes[:,0],t_0=0.0,simTime=simTime, sigma_krnl = 0)

    sSR = gaussian_filter(SR, sigma_krnl)
    
    
    return sSR

def net_freq(spiketimes,freq_min=30.0,sigma_krnl=30, scaleTest = 1.0):
    # returns: Freq,Sync,
    # Power spectral density
    spk_train=smooth_rate(spiketimes, sigma_krnl=sigma_krnl)
    ss=autocorrNorm(scaleTest*spk_train)
    (frq,Y)=Spectrum(ss,100000.0)
    frqaux=frq[frq>freq_min]#frq[25:125]
    yaux=abs(Y[frq>freq_min]) #changed from freq>10 11.3.14
    fc=frqaux[yaux.argmax()]

    #Coherence
    si2=np.sqrt(yaux.max()/abs(Y[0]))
    return fc, si2, frq, Y, ss




def net_freqExtended(spiketimes,freq_min=30.0,sigma_krnl=0.0, simTime = 1.0, verbose = False):
    # returns: Freq,Sync,
    # Power spectral density
    spk_train=smooth_rate(spiketimes, simTime = simTime, sigma_krnl=sigma_krnl, verbose = verbose)
    if verbose:
        print 'Smooth rate done'
    ss=autocorrNorm(spk_train)
    if verbose:
        print 'Autocorr done'
    (frq,Y)=Spectrum(ss,100000.0)
    if verbose:
        print 'FFT done'

    frqaux=frq[frq>freq_min]#frq[25:125]
    yaux=abs(Y[frq>freq_min]) #changed from freq>10 11.3.14
    maxIndex = yaux.argmax()
    fc = frqaux[maxIndex]

    #Coherence
    si2 = np.sqrt( yaux.max() / abs(Y[0]) )


    return fc, si2, frq, Y, ss, frqaux, yaux, maxIndex









def differentSyncIndex(spiketimes,freq_min=30.0,sigma_krnl=30, time_min = 3*100, max_length = 10000):

    # returns: CoherenceIndex, syncIndexTraub
    spk_train = smooth_rate(spiketimes, sigma_krnl=sigma_krnl)

    aCorr = autocorrNorm(spk_train)
    aCorrAux = aCorr[time_min:]
    aCorrMaxY = aCorrAux.max()
    aCorrMaxX = aCorrAux.argmax()
    synIndexTraub = aCorrMaxY/(aCorr.mean())**2

    (frq, Y) = Spectrum(aCorr, 100000.0)
    frqaux = frq[frq > freq_min]#frq[25:125]
    yaux = abs(Y[frq > freq_min]) #changed from freq>10 11.3.14
    fc = frqaux[yaux.argmax()]

    #Coherence
    coherence = np.sqrt(yaux.max()/abs(Y[0]))

    return coherence, synIndexTraub




def rasterPlot(spikes, simtime, number_neuron = 199.0, sync = 1, ccode = colNetFreq, figName = 'Test', markersize = 0.1, returnFig = True, showTime = 0):


    N1 = np.where(spikes == number_neuron) #gives the number spike count

    if len(N1[0]) == 0 : return 'Sorry, we do not have so many neurons'
    else: N = N1[0][-1]

    x = np.array([0.0]*N)
    y = np.array([0.0]*N)
    each = 0;
    for each in range(N): #extremly costly way to generate the wanted spike train
        x[each] = spikes[each][0]*1000
        y[each] = spikes[each][1]
        each+=1

    if sync == 1: #decide for layout of the plot
      #fig = pp.figure(facecolor='white', figsize= (3,6)) #for in an .pdf file
      fig = pp.figure(facecolor='white', figsize= (6,6))
      plotnumber1 = 312
      plotnumber2 = 311
      plotnumber3 = 313
    elif sync == 0:
      #fig = pp.figure(facecolor='white', figsize= (3,4)) #for .pdf
      fig = pp.figure(facecolor='white', figsize= (7,5))
      plotnumber1 = 211
      plotnumber3 = 212
    else:
      #fig = pp.figure(facecolor='white', figsize= (3,4)) #for .pdf
      fig = pp.figure(facecolor='white', figsize= (3.5,5))
      plotnumber1 = 211
      plotnumber3 = 212
    pp.rcParams.update({'font.size': 20})
    #Scatter plot for the spikes
    ax = fig.add_subplot(plotnumber1)
    #pp.scatter(x,y, marker='|', c = colNetFreq)
    pp.scatter(x,y, marker=',', s = markersize, c = ccode)
    pp.xlim([showTime, simtime])
    pp.ylim([(spikes[0][1] - 2),spikes[N][1]])
    pp.ylabel('Neurons')
    ax.yaxis.set_ticks([])
    #pp.xlabel('Time in s')
    ax.spines["top"].set_visible(False) #the price of beauty...
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    #pp.axis('off')
    ax.spines["bottom"].set_color('none')
    pp.setp(ax.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'x', nbins = 3)

    #if we want the sync plot...then here it comes
    if sync == 1:

        syncIndex, histoTimes, histoValues, valuesNoZeros, spikeTimesNoZeros = spikeTrainSynchrony(output, 0.0005, bins = 1000)
        ax3 = fig.add_subplot(plotnumber2, sharex = ax)
        histoTimes = [x*1000 for x in histoTimes]
        #pp.plot(c,b, '.')
        pp.plot(histoTimes,histoValues,c = '0.5') #bins of fixed neuron number
        t = moving_average(spikeTimesNoZeros,n=300)
        t = [x*1000 for x in t]
        pp.plot(t,moving_average(valuesNoZeros,n=300),ls = '--', c = colSync) #times averages
        pp.xlim([0, simtime])
        pp.ylim([-0.1, 1.2])
        pp.ylabel('Synchrony')
        ax3.yaxis.set_ticks([])
        #pp.xlabel('Time in s')
        ax3.spines["top"].set_visible(False)
        ax3.spines["right"].set_visible(False)
        ax3.spines["left"].set_visible(False)
        ax3.spines["bottom"].set_visible(False)
        ax3.axes.get_xaxis().set_visible(False)
        ax3.xaxis.set_ticks_position('bottom')
        #pp.axis('off')
        ax3.spines["bottom"].set_color('none')
        pp.setp(ax.get_xticklabels(), visible=False)
        print 'Synchrony index = ', syncIndex

    ax2 = fig.add_subplot(plotnumber3, sharex = ax)
    ax2.spines["left"].set_color('none')
    #ax2.tick_params(axis='y', colors='W')
    #ax2.yaxis.set_ticks([0.0, round_to_1(max(output.smooth_rate(sigma_krnl=30)),1)])
    #ax2.yaxis.set_ticks([0, 1])
    ax2.spines["left"].set_color('none')
    ax2.yaxis.tick_left()
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    #pp.title('Network activity')
    pp.plot(np.array(range(len(smooth_rate(spikes, sigma_krnl=30.0))))/100.0, smooth_rate(spikes, sigma_krnl=30), c = ccode)
    #pp.plot(np.array(range(len(output.smooth_rate(sigma_krnl=0.0))))/100.0,output.smooth_rate(sigma_krnl=0.0))
    pp.ylabel('Activity (a.u.)')
    pp.xlabel('Time (ms)')
    ax2.xaxis.set_ticks_position('bottom')
    ax2.xaxis.set_tick_params(direction='out')
    pp.xlim([showTime*1000, simtime*1000])
    #pp.ylim(ymin = -0.1)

    pp.ylim([-0.1, 1.5])
    ax2.yaxis.set_ticks([0, 1.0])


    for loc, spine in ax2.spines.items():
        if loc in ['bottom']:
            spine.set_position(('outward',5)) # outward by 10 points

    fig.tight_layout()
    pp.subplots_adjust(left = 0.12, bottom = 0.13)
    pp.xlabel('Time (ms)')

    ax.get_yaxis().set_label_coords(-0.3, 0.5)
    ax2.get_yaxis().set_label_coords(-0.3, 0.5)

    pp.rcParams.update({'font.size': 16})
    pp.locator_params(axis = 'x', nbins = 3)

    saveFig(fig, figName)
    saveFig(fig, figName, fileFormat='.png' , dpi=300)
    #pp.close()
    if returnFig == True: return fig
    else: pp.close()


def rasterPlotSWR(spikes, simtime, number_neuron = 199.0,
                  sync = 1, ccode = colNetFreq, figName = 'Test',
                  markersize = 0.1, returnFig = True,
                  showTime = 0, bins = 500, title = '',
                  spectrumFreqs = [], spectrumValues = []):

#    spikes = output.spiketimes
    pp.rcParams.update({'font.size': 12})
    N1 = np.where(spikes == number_neuron) #gives the number spike count

#    if len(N1[0]) == 0 : return 'Sorry, we do not have so many neurons'
#    else: N = N1[0][-1]


#    N = N1[0][-1]
    
    N = len(spikes)
    
    x = np.array([0.0]*N)
    y = np.array([0.0]*N)
    each = 0;
    for each in range(N): #extremly costly way to generate the wanted spike train
        x[each] = spikes[each][0]*1000
        y[each] = spikes[each][1] * 5.0
        each+=1

    if sync == 1: #decide for layout of the plot
      #fig = pp.figure(facecolor='white', figsize= (3,6)) #for in an .pdf file
      fig = pp.figure(facecolor='white', figsize= (6,6))
      plotnumber1 = 312
      plotnumber2 = 311
      plotnumber3 = 313
    elif sync == 0:
      #fig = pp.figure(facecolor='white', figsize= (3,4)) #for .pdf
      fig = pp.figure(facecolor='white', figsize= (7,5))
      plotnumber1 = 211
      plotnumber3 = 212
    elif sync == 3:
      #fig = pp.figure(facecolor='white', figsize= (3,4)) #for .pdf
      fig = pp.figure(facecolor='white', figsize= (3.5,5))
      plotnumber1 = 211
      plotnumber3 = 212
    else:
      fig = pp.figure(facecolor='white', figsize= (6,8))
      plotnumber1 = 512
      plotnumber2 = 511
      plotnumber3 = 513 
      plotnumber4 = 514
      plotnumber5 = 515
        
    pp.rcParams.update({'font.size': 10})
    #Scatter plot for the spikes
    ax = fig.add_subplot(plotnumber1)
    #pp.scatter(x,y, marker='|', c = colNetFreq)
    pp.scatter(x,y, marker=',', s = markersize, c = ccode)
    pp.xlim([showTime, simtime])
    pp.ylim([(spikes[0][1] - 2),spikes[N-1][1] * 5.0])
    pp.ylabel('Neurons')
    ax.yaxis.set_ticks([])
    #pp.xlabel('Time in s')
    ax.spines["top"].set_visible(False) #the price of beauty...
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    #pp.axis('off')
    ax.spines["bottom"].set_color('none')
    pp.setp(ax.get_xticklabels(), visible=False)
    pp.locator_params(axis = 'x', nbins = 3)

    #if we want the sync plot...then here it comes
    if (sync == 1) | (sync == 4):

        syncIndex, histoTimes, histoValues, valuesNoZeros, spikeTimesNoZeros = spikeTrainSynchrony(spikes, 0.0005, bins = bins)
        ax3 = fig.add_subplot(plotnumber2, sharex = ax)
        histoTimes = [k*1000 for k in histoTimes]
        #pp.plot(c,b, '.')
        pp.plot(histoTimes,histoValues,c = '0.5') #bins of fixed neuron number
        t = moving_average(spikeTimesNoZeros,n=300)
        t = [k*1000 for k in t]
#        pp.plot(t,moving_average(valuesNoZeros,n=300),ls = '--', c = colSync) #times averages
        pp.xlim([0, simtime])
        pp.ylim([-0.1, 1.2])
        pp.ylabel('Synchrony')
        pp.locator_params(axis = 'x', nbins = 3)
#        ax3.yaxis.set_ticks([])
        #pp.xlabel('Time in s')
        ax3.spines["top"].set_visible(False)
        ax3.spines["right"].set_visible(False)
        ax3.spines["left"].set_visible(False)
        ax3.spines["bottom"].set_visible(False)
        ax3.axes.get_xaxis().set_visible(False)
        ax3.xaxis.set_ticks_position('bottom')
        #pp.axis('off')
        ax3.spines["bottom"].set_color('none')
        pp.setp(ax.get_xticklabels(), visible=False)
        print 'Synchrony index = ', syncIndex
        fig.suptitle(title + 'Sync. ind. = ' +  str(syncIndex)[:4], fontsize = 10 )
        
    ax2 = fig.add_subplot(plotnumber3, sharex = ax)
    ax2.spines["left"].set_color('none')
    #ax2.tick_params(axis='y', colors='W')
    #ax2.yaxis.set_ticks([0.0, round_to_1(max(output.smooth_rate(sigma_krnl=30)),1)])
    #ax2.yaxis.set_ticks([0, 1])
    ax2.spines["left"].set_color('none')
    ax2.yaxis.tick_left()
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(False)
    #pp.title('Network activity')
    networkActivity = smooth_rate(spikes, sigma_krnl=30, simTime = 0.3)
    pp.plot(np.array(range(len(networkActivity)))/100.0, networkActivity, c = ccode)
    #pp.plot(np.array(range(len(output.smooth_rate(sigma_krnl=0.0))))/100.0,output.smooth_rate(sigma_krnl=0.0))
    pp.ylabel('Activity (a.u.)')
    pp.xlabel('Time (ms)')
    ax2.xaxis.set_ticks_position('bottom')
    ax2.xaxis.set_tick_params(direction='out')
    pp.xlim([showTime*1000, simtime*1000])
    #pp.ylim(ymin = -0.1)

#    pp.ylim([-0.1, 0.7])
#    ax2.yaxis.set_ticks([0, 0.5])

    if sync == 4:
        ax4 = fig.add_subplot(plotnumber4)
        
        freq, times, Sxx = spectrogram(networkActivity.flatten(), fs = 100000, noverlap = 4096-100, nperseg = 4096)

        #freq, times, Sxx = spectrogram(networkActivity.flatten(), fs = 100000, noverlap = 1023, nperseg = 1024)
        Sxx = Sxx[(freq > 50.0) & (freq < 500.0), :]
        freq = freq[(freq > 50.0) & (freq < 500.0)]
        ax4.pcolormesh(times, freq, Sxx)
        ax4.set_xlabel('Time (ms)')
        gaussianWidth = 5
        ax5 = fig.add_subplot(plotnumber5)
        ax5.plot(spectrumFreqs, spectrumValues, label = 'Raw')
        ax5.plot(spectrumFreqs,gaussian_filter(spectrumValues,gaussianWidth), label = 'Filtered ')
        #plt.rcParams.update({'font.size': 22})
        ax5.set_xlabel('Frequency (Hz)')
        ax5.set_ylabel('Power (a.u)')
        ax5.set_xlim(xmin = 100)
#        ax5.autoscale(enable=True, axis='y', tight=True)
    
    for loc, spine in ax2.spines.items():
        if loc in ['bottom']:
            spine.set_position(('outward',5)) # outward by 10 points

    fig.tight_layout()
    pp.subplots_adjust(left = 0.12, bottom = 0.13)
#    

    ax.get_yaxis().set_label_coords(-0.1, 0.5)
    ax2.get_yaxis().set_label_coords(-0.1, 0.5)
    ax3.get_yaxis().set_label_coords(-0.1, 0.5)
    ax4.get_yaxis().set_label_coords(-0.1, 0.5)
    ax5.get_yaxis().set_label_coords(-0.1, 0.5)
    
    pp.locator_params(axis = 'x', nbins = 3)
    
    saveFig(fig, figName)
    saveFig(fig, figName, fileFormat='.png' , dpi=300)
    #pp.close()
    if returnFig == True: return fig
    else: pp.close()


    
def qFactorCalc(spiketimes, interpolationRangeFreq = 20, Upsampling = 50, plotGen = False, simTime = 1.0, verbose = False):

    fc, si2, frq, Y, ss, frqaux, yaux, maxIndex = net_freqExtended(spiketimes, simTime = simTime, verbose = verbose)
    if verbose:
        print 'NetFreq done'

    halfPowerValue = yaux.max()/2.0
    # Define the interpolation range

    interpolationRange = ceil(interpolationRangeFreq/(frqaux[1]-frqaux[0]))
    x = frqaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]
    y = yaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]

    # Generate the interpolating function and generate new data
    numberOfPoints = interpolationRange*Upsampling
    interpolateForPower = interp1d(x, y, kind='linear')
    if verbose:
        print 'Interpolation done'
    fInterpo = np.linspace(frqaux[maxIndex - (interpolationRange - 1)], frqaux[maxIndex + (interpolationRange-1)], num = numberOfPoints, endpoint=False)
    YInterpo = interpolateForPower(fInterpo)

    maxIndexNew = numberOfPoints/2.0

    halfPowerIndexLow = (abs(YInterpo[:maxIndexNew] - halfPowerValue)).argmin()
    halfPowerIndexUp = (abs(YInterpo[maxIndexNew:] - halfPowerValue)).argmin()

    HalfPowerFLow = fInterpo[:maxIndexNew][halfPowerIndexLow]
    HalfPowerFUp = fInterpo[maxIndexNew:][halfPowerIndexUp]

    if plotGen:
        fig = pp.figure()
        pp.plot(fInterpo, YInterpo, ',')
        pp.plot(fInterpo[maxIndexNew],YInterpo[maxIndexNew], 'o')
        pp.plot(HalfPowerFLow, YInterpo[halfPowerIndexLow], 'o', c = 'red')
        pp.plot(HalfPowerFUp, YInterpo[maxIndexNew:][halfPowerIndexUp], 'o', c = 'red')
        pp.plot(frqaux[frq < 300], yaux[frq < 300], '.')
        pp.xlim([180,220])
        #pp.show()

    #Calculate the FWHM
    FWHM = HalfPowerFUp - HalfPowerFLow
    qFactor = FWHM/fc
    print FWHM

    if plotGen: return FWHM, qFactor, fig
    else: return FWHM, qFactor

def lorentzian(x, a, x0, sigma):
    numerator =  (sigma**2 )
    denominator = np.pi*sigma*(( x - x0 )**2 + sigma**2)
    y = a*(numerator/denominator)
    return y


def qFactorCalc2(spiketimes, interpolationRangeFreq = 20, Upsampling = 50, plotGen = False, simTime = 10.0, verbose = False):

    fc, si2, frq, Y, ss, frqaux, yaux, maxIndex = net_freqExtended(spiketimes, simTime = simTime, verbose = verbose)
    if verbose:
        print 'Net Freq done'

    interpolationRange = ceil(interpolationRangeFreq/(frqaux[1]-frqaux[0]))

    x = frqaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]
    y = yaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]



    n = len(x)                          #the number of data
    mean = sum(x)/n                   #note this correction
    sigma = np.sqrt(sum((x-mean)**2)/n)        #note this correction

    print mean

    #popt, pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    poptLo, pcovLo = curve_fit(lorentzian,x,y, p0=[1,mean,sigma])
    #Calculate the FWHM
    FWHM = poptLo[2]*2
    qFactor = FWHM/fc

    if plotGen:


        # Generate the interpolating function and generate new data
        numberOfPoints = interpolationRange*Upsampling

        if verbose:
            print 'Fitting done'


        fFit = np.linspace(frqaux[maxIndex - (interpolationRange - 1)], frqaux[maxIndex + (interpolationRange-1)], num = numberOfPoints, endpoint=False)
        YFit = lorentzian(fFit,*poptLo)

        halfPowerValue = YFit.max()/2.0

        maxIndexNew = numberOfPoints/2.0



        halfPowerIndexLow = (abs(YFit[:maxIndexNew] - halfPowerValue)).argmin()
        halfPowerIndexUp = (abs(YFit[maxIndexNew:] - halfPowerValue)).argmin()

        HalfPowerFLow = fFit[:maxIndexNew][halfPowerIndexLow]
        HalfPowerFUp = fFit[maxIndexNew:][halfPowerIndexUp]


        fig = pp.figure()
        pp.plot(fFit, YFit, ',')
        pp.plot(fFit[maxIndexNew],YFit[maxIndexNew], 'o')
        pp.plot(HalfPowerFLow, YFit[halfPowerIndexLow], 'o', c = 'red')
        pp.plot(HalfPowerFUp, YFit[maxIndexNew:][halfPowerIndexUp], 'o', c = 'red')
        pp.plot(frqaux[frq < 300], yaux[frq < 300], '.')
        pp.xlim([180,220])
        #pp.show()
        return FWHM, qFactor, fig



    else: return FWHM, qFactor


def qFactorCalc3(spiketimes, interpolationRangeFreq = 20, windowSize = 3, plotGen = False, simTime = 10.0, verbose = False):

    fc, si2, frq, Y, ss, frqaux, yaux, maxIndex = net_freqExtended(spiketimes, simTime = simTime, verbose = verbose)
    if verbose:
        print 'Net Freq done'

    interpolationRange = ceil(interpolationRangeFreq/(frqaux[1]-frqaux[0]))

    xRange = frqaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]
    yRange = yaux[int(maxIndex - interpolationRange) : int(maxIndex + interpolationRange)]

    #x = gt.moving_average(xRange, windowSize)
    #y = gt.moving_average(yRange, windowSize)
    x = xRange
    y = gaussian_filter(yRange, windowSize)

    halfPowerValue = y.max()/2.0
    maxIndexNew = y.argmax()
    halfPowerIndexLow = (abs(y[:maxIndexNew] - halfPowerValue)).argmin()
    halfPowerIndexUp = (abs(y[maxIndexNew:] - halfPowerValue)).argmin()
    HalfPowerFLow = x[:maxIndexNew][halfPowerIndexLow]
    HalfPowerFUp = x[maxIndexNew:][halfPowerIndexUp]

    FWHM = HalfPowerFUp - HalfPowerFLow
    qFactor = fc/FWHM

    if plotGen:

        fig = pp.figure()

        pp.plot(frqaux[frq < 300], yaux[frq < 300], '.', c = '0.5')
        pp.plot(x, y, '-', c = 'blue')

        pp.plot(x[maxIndexNew],y[maxIndexNew], 'o', c = 'red')
        pp.plot(HalfPowerFLow, y[halfPowerIndexLow], 'o', c = 'red')
        pp.plot(HalfPowerFUp, y[maxIndexNew:][halfPowerIndexUp], 'o', c = 'red')
        pp.xlim([180,220])

        return FWHM, qFactor, fig


    else: return FWHM, qFactor


def getSystemPath(location = 'documentationProject'):

    import getpass
    if location == 'documentationProject':
        if getpass.getuser() == 'andre':
            return "/home/andre/Uni/documentationProject/"
        elif getpass.getuser() == 'holzbecher':
            return "/home/holzbecher/PhD/"
        else:
            raise NameError('I dont know where we are...I deeply regret')

    elif location == 'dropbox':
        if getpass.getuser() == 'andre':
            return "/home/andre/Dropbox/André_2004-data/"
        elif getpass.getuser() == 'holzbecher':
            return "/home/holzbecher/Dropbox/André_2004-data/"
        else:
            raise NameError('I dont know where we are...I deeply regret')
            
            
    elif location == 'anoukProject':
        if getpass.getuser() == 'andre':
            return "/home/andre/Dropbox/Anouk Manuscript/"
        elif getpass.getuser() == 'holzbecher':
            return "/home/holzbecher/Dropbox/Anouk Manuscript/"
        else:
            raise NameError('I dont know where we are...I deeply regret')

    elif location == 'gapjunctions':
        if getpass.getuser() == 'andre':
            return "/home/andre/Uni/GapjunctionProject/"
        elif getpass.getuser() == 'holzbecher':
            return "/home/holzbecher/PhD/Gap Junctions/Python/"
        else:
            raise NameError('I dont know where we are...I deeply regret')

    else:
        raise NameError('Where the hack is {}'.format(location))

def createSpikeTray2(spiketimes):

    outspikeTimes = []
    for neuron in range(int(spiketimes[-1,1])  + 1):
        pos, posBool = np.where(spiketimes == neuron)
        spikeArray = spiketimes[pos][:,0].flatten()
        outspikeTimes.append(spikeArray.copy())

    return outspikeTimes



def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    
    return y


def butter_lowpass(highcut, fs, order=5):
    nyq = 0.5 * fs
    highcut = highcut / nyq
    b, a = butter(order, highcut, btype='low')
    return b, a


def butter_lowpass_filter(data, highcut, fs, order=5):
    b, a = butter_lowpass(highcut, fs, order=order)
    y = lfilter(b, a, data)
    
    return y

#imports from former nlstools
    

def acf(x, length=20):
    return np.array([1]+[np.corrcoef(x[:-i], x[i:])[0,1] \
        for i in range(1, length)])

def autocorr(x):
    result = np.correlate(x, x, mode='full')
    return result[result.size/2:]

def autocorrNorm(x):
    result = np.correlate(x, x, mode='full')/np.var(x)
    return result[result.size/2:]


def gaussfunc(x,a,b,c,d):
    return d + a*np.exp(-((x-b)**2)/(2*c**2))


def gaussfuncNorm(x,a,b,c,d):
    return d + a * 1.0/ ( c * np.sqrt(2.0 * np.pi)) * np.exp(-((x-b)**2)/(2*c**2))

def Spectrum(y,Fs,plot=1):
    """
    Plots a Single-Sided Amplitude Spectrum of y(t)
    """
    n = len(y) # length of the signal
    k = arange(n)
    T = float(n)/Fs #duration of the signal
    #assert T > 0
    frq = k/T # two sides frequency range
    frq = frq[range(n/2)] # one side frequency range
    Y = fft(y) / float(n) # fft computing and normalization
    Y = Y[range(n/2)]
    return frq, Y

def realSpectrum(y,Fs,plot=1):
    """
    Plots a Single-Sided Amplitude Spectrum of y(t)
    """
    n = len(y) # length of the signal
    k = arange(n)
    T = n/Fs #duration of the signal
    #assert T > 0
    frq = k/T # two sides frequency range
    frq = frq[range(n/2)] # one side frequency range
    Y = np.fft.rfft(y)/n # fft computing and normalization
    Y = Y[range(n/2)]
    return frq, Y

def ac_dc(signal,SR):
    (frq,Y)=Spectrum(signal,SR)
    Y=abs(Y)
    dc=Y[0]
    ac=Y[frq>10].max()
    return ac,dc

def sync_idx(rate_inh,fc,FR,plots=False):#,smooth=False):
    #if smooth:
    #ss=rate_inh.smooth_rate(width=1*ms,filter='gaussian')[rate_inh.times>0.01]
    #else:
    ss=rate_inh
    acs=autocorr(ss)
    (frq2,Y2)=Spectrum(acs,FR)
    idx=np.array(range(0,len(frq2)))
    fc_idx0=idx[frq2>0.9*fc][0]
    fc_idx1=idx[frq2<1.1*fc][-1]
    Y_chunk=abs(Y2)[fc_idx0:fc_idx1]
    frq_chunk=frq2[fc_idx0:fc_idx1]
    if Y_chunk.size==0: #if Y_chunk empty then no peak
        Y_chunk=np.array([1,0])
    if Y_chunk.argmax()==0:
        print 'no peak in fft(acf)!'
        ffac=0
    else:
        ffac=Y_chunk.max()
        #fc_idx=numpy.argmin(abs(frq2-popt[1])) #find index of closest freq to the fft peak
        #ffac=abs(Y2)[fc_idx] #first fourier component
    if plots:
        pyplot.figure()
        pyplot.subplot(211)
        pyplot.plot(acs)
        pyplot.title('Autocorrelation')
        pyplot.subplot(212)
        pyplot.plot(frq2,abs(Y2))
        pyplot.title('PSD')
        print 'Frequency (ACF)   : ', frq_chunk[Y_chunk.argmax()]#frq2[fc_idx]
    return np.sqrt(ffac/abs(Y2[0]))

def bpfilt(x,lowcut,highcut,fs):
    order=2
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    (b, a) = signal.butter(order, [low, high], btype='band')
    #print b,a
    y = signal.lfilter(b, a, x)
    return y

def GaussMatrix(n_row,n_col,mu,sigma):
    out=[]
    for i in range(n_row):
        row=[]
        for j in range(n_col):
            row.append(rn.gauss(mu,sigma))
        out.append(row)
    return np.array(out)

def SyncGroupInput(n_units,n_act_units,at_time_bin):
    flat_line=np.zeros((at_time_bin-1,n_units))
    sync_act=np.append(np.ones((1,n_act_units)),np.zeros((1,n_units-n_act_units)),axis=1)
    stimulus=np.append(flat_line,sync_act,axis=0)
    stimulus=np.append(stimulus,np.zeros((1,n_units)),axis=0)
    return stimulus

def rm_refractory(S,spiketimes,ref=100):
    So=S.copy()
    for k in range(np.shape(spiketimes)[0]):
        So[spiketimes[k,1],spiketimes[k,0]*100000:spiketimes[k,0]*100000+ref]=np.nan
    return So

def time_hist(S,hrange,nbins=20):
    sample=S[:,0]
    S_hist=np.histogram(sample[np.isfinite(sample)],range=(hrange[0],hrange[1]),bins=nbins)[0]
    for k in range(1,S.shape[1]):
        sample=S[:,k]
        S_hist=np.vstack((S_hist,np.histogram(sample[np.isfinite(sample)],range=(hrange[0],hrange[1]),bins=nbins)[0]))
    #pyplot.imshow(S_hist.T,interpolation='none',origin='lower',aspect='auto',extent=ext,cmap=cmap)
    #cbar=pyplot.colorbar()
        #pp.plot(range(tlim[0],tlim[1]),self.v_thres*np.ones(len(range(tlim[0],tlim[1]))),'k--')
        #pp.xlim(tlim[0],tlim[1])
    #cbar.set_label('Proportion of units (%)', rotation=270)
    return S_hist

def spikespercycle(nspikes): #arg needs to be array
    #Takes vector with population firing rate in units of No. of spikes
    out=[]
    cycle_size=4# half size (number in bins)

    for k in range(cycle_size,nspikes.size-cycle_size):
        chunk=nspikes[k-cycle_size:k+cycle_size+1]
        if chunk.argmax()==cycle_size: # center of the chunk
            out.append(chunk.sum())
    return np.array(out)

def find_extrema(signal,peak=False,winlen=201):
    # find location of throughs by default if peak=True then find peaks
    through_idx=[]
    for k in range(len(signal)-winlen):
        chunk=signal[k:k+winlen]
        extrema=chunk.argmin()
        if peak: extrema=chunk.argmax()
        if extrema==winlen/2:
	    through_idx.append(extrema+k)
    return through_idx

def charge(I):
    currents=I.copy()
    for row in range(np.shape(I)[0]):
        currents[row,:]=currents[row,:]-currents[row,0:100].min()
    Q=np.cumsum(currents,axis=1)/100000 #*1/sampling rate
    return Q,currents

def charge_delivery(I,trigg_list):
    max_len=np.diff(trigg_list).min() #length of shortest cycle
    n_cycles=len(np.diff(trigg_list))
    Q_cycle=np.arange(max_len)
    for k in range(len(trigg_list)-1):
        Q,currents=charge(I[:,trigg_list[k]:trigg_list[k+1]])
        total_charge=Q.sum(axis=0)
        Q_cycle=np.vstack((Q_cycle,total_charge[0:max_len]))
    Q_cycle=Q_cycle[1:n_cycles,:] #remove stem vector
    return Q_cycle
# Wavelet analysis
def wavelet(freq,SR,sigma=0.5,plots=False,norm=False):
    #sigma: proportion of t_span 1

    n_cycles=4
    t_span=0.5*n_cycles/freq #1/2 total t_span
    t=np.array(range(int(round(2*t_span*SR)+1)))
    t=t/float(SR)-t_span
    kernel_r=np.exp(-(t**2)/(2*(sigma*t_span)**2))*np.cos(2*np.pi*freq*t)
    kernel_i=np.exp(-(t**2)/(2*(sigma*t_span)**2))*np.sin(2*np.pi*freq*t)
    if norm:
        kernel_r=0.71*kernel_r/sum(kernel_r**2)
        kernel_i=0.71*kernel_i/sum(kernel_i**2)
    if plots:
        pyplot.plot(t,kernel_r)
        pyplot.plot(t,kernel_i)
    return kernel_r,kernel_i

def conwvlt(signal,freq,SR,sigma=0.3,plots=False,norm=False,zscore=False):#zscore specifies number of samples to use for baseline estimation
    [wvltr,wvlti]=wavelet(freq,SR,sigma,norm=norm)
    convr=np.convolve(signal,wvltr,'same')
    convi=np.convolve(signal,wvlti,'same')
    conv=convr**2+convi**2 #np.sqrt removed
    if zscore:
        bl_mean=conv[0:zscore].mean()
        bl_std=1.0#conv[0:zscore].std()
        conv=(conv-bl_mean)/bl_std
    if plots:
        tk=1000.0*np.array(range(len(wvltr)))/SR
        tk=tk-tk[-1]/2.0
        pyplot.figure()
        pyplot.plot(tk,wvltr)
        pyplot.plot(tk,wvlti,'r')
        t=1000.0*np.array(range(len(signal)))/SR
        pyplot.figure()
        pyplot.subplot(311)
        pyplot.plot(t,signal)
        pyplot.subplot(312)
        pyplot.plot(t,convr)
        pyplot.plot(t,convi,'r')
        pyplot.subplot(313)
        pyplot.plot(t,conv)
    return conv

def spectrogram(signal,freqlist,SR,sigma=0.3,ext=[],plots=True,spectrum=[],norm=False,zscore=False,fmax=1000):#spectrum: value of spectrum extension
    # fmax is the maximum frequency considered to estimate the instantaneous freq curve
    # norm: use normalized wavelet
    if not ext:
        ext=[0,1000*len(signal)/float(SR),freqlist[0],freqlist[-1]]
    stack=conwvlt(signal,freqlist[0],SR,sigma,norm=norm,zscore=zscore)
    for k in range(1,len(freqlist)):
        stack=np.vstack((stack,conwvlt(signal,freqlist[k],SR,sigma,norm=norm,zscore=zscore)))
    freqlist=np.array(freqlist)
    freqs=freqlist[stack[freqlist<=fmax,:].argmax(axis=0)] # Instantaneous frequency trace
    t=1000.0*np.array(range(len(signal)))/SR
    if plots:
        #print stack.shape
    #pyplot.figure()
        pyplot.imshow(stack,interpolation='none',origin='lower',aspect='auto',extent=ext,cmap='jet')
    #pyplot.plot(t[::100],freqs[::100],'k.-')
        if spectrum:
	    spec=np.mean(stack,1)
            spec=spec/spec.max()
            pyplot.hlines(freqlist,ext[0]*np.ones(len(range(110,300))),spectrum*(spec-spec.min()),color='gray')
    # Estimation of frequency and duration
    pow_t=np.mean(stack,0)
    pow_t=pow_t-pow_t[0]
    pow_t=pow_t/pow_t.max()
    duration=t[pow_t>=0.5]
    duration=duration[-1]-duration[0]
    pow_f=np.mean(stack,1)
    f_peak=freqlist[pow_f.argmax()]
    return t,freqs,stack,f_peak,duration #returns frequency trace

def spgm(signal,freqlist,SR,sigma=0.5):
    stack=conwvlt(signal,freqlist[0],SR,sigma)
    for k in range(1,len(freqlist)):
        stack=np.vstack((stack,conwvlt(signal,freqlist[k],SR,sigma)))
    return stack



# New function for the firing rates
    

def returnTimeResolvedFiringRatesInMsWindows(spiketimes, simulationLength, scaleFactor = 0.1, timeFactor = 1000):
    
    bins = int(simulationLength * timeFactor)
    spikeHistogram = np.histogram(spiketimes[:,0], bins)

    # int_spr is 0.1, so * 10 
    # and 300 bins gives us ms, so * 1000.0
    firingRates = spikeHistogram[0] * scaleFactor * timeFactor

    times = spikeHistogram[1][1:] + (spikeHistogram[1][1] - spikeHistogram[1][0]) / 2.0
    
    return times, firingRates

    