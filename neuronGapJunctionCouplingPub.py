# -*- coding: utf-8 -*-
"""
Created on Mon Aug  1 15:11:10 2016

@author: holzbecher
"""

# =============================================================================
# This contains some functions that are used to 
# support the multicompartment analysis.
# =============================================================================

#%%
import pandas as pd
import matplotlib.pyplot as plt
import gapAnalyticsTempPub as gt
import numpy as np


#some nice colors
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]


for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r / 255., g / 255., b / 255.)

def get_colorscheme(numberOfPlots, mirror = True):
    if mirror == True:
        colorsForPlot = tableau20[:numberOfPlots - 1:2]

        if numberOfPlots % 2 == 0:
            return colorsForPlot + colorsForPlot[::-1]
        else:
            return colorsForPlot + colorsForPlot[-2::-1]
    else:
        return tableau20[:2*numberOfPlots :2]

def findFractionMaxium(dataset, fractionOfMax = 0.95):
    #Assumption:    monotonic growths of function until maximum...
    #Assumption:    if we cannot determine a fractionOfMaxValue... we just
    #               take the maximum value

    dataMax = dataset.max()
    dataMin = dataset.min()
    dataLowerLimit = dataMax - (dataMax - dataMin) * (1.0 - fractionOfMax)
    dataMaxIdx = dataset.idxmax()
    logical = np.logical_and(dataset.index < dataMaxIdx, dataset > dataLowerLimit)
    datasetRise = dataset[logical]
    if len(datasetRise) == 0: return dataMaxIdx
    return abs((datasetRise - dataLowerLimit)).idxmin()

def get_seperator(identifier):

    if identifier == '' or identifier == 'HOT':
        return ' '
    else: return ','

def get_maxIdxBlock(data, maxPosition = 'right'):

    dataAmp = data.max() - data.min()

    dataAboveThreshold = data[data > (data.max() - dataAmp/2.0)]

    
    if maxPosition == 'middle':
        maxIdx = (dataAboveThreshold.index.max() + dataAboveThreshold.index.min())/2.0
        return maxIdx

    elif maxPosition == 'right':
        return dataAboveThreshold.index.max()

    elif maxPosition == 'left':
        return dataAboveThreshold.index.min()
        

def analyzeNeuronData(dendrites, identifier, nameLocation, fractionOfMax, maxPosition = 'right'):

    #loadname = '/home/holzbecher/PhD/Gap Junctions/Neuron/Basketcell/cell' + nameLocation + dendrites + '.dat'
    loadname = '/home/holzbecher/PhD/Gap Junctions/Neuron/Basketcell/data/' + identifier + dendrites + 'cell' + nameLocation  + '.dat'
    #loadname = '/home/holzbecher/PhD/Gap Junctions/Neuron/Basketcell/data/' +'VCLAMP0.1cellproxisoma.dat'


    #loadname = '~/Uni/documentationProject/Gap Junctions/Neuron/Basketcell/data/' + identifier + dendrites + 'cell' + nameLocation  + '.dat'

    if dendrites == '1': vRest = 74.6116
    elif dendrites == '333': vRest = 70.1467
    elif dendrites == '0': vRest = 69.8838
    else: vRest = 69.6594
    seperator = get_seperator(identifier)
    neuronData = pd.read_csv(loadname, sep = seperator, index_col = False)#, header = True)
    neuronData.index = neuronData['time(ms)']
    neuronData = neuronData.drop('time(ms)', 1)
    #plotlist[i] = neuronData.iloc[:,1:].plot()



    resistanceValues = neuronData['gapR(pS)'].unique()

    maxListDF = pd.Panel(items = neuronData.columns, major_axis = ['Maximum (mV)', 'Time@Max (ms)', 'Relative time (ms)', 'Time@FractionOfMax (ms)', 'Delay fraction (ms)'], minor_axis = resistanceValues)


    for resistanceValue in resistanceValues:

        tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]
        forTimeRange = tempData['somaA(mV)'].idxmax()
        tempData = tempData[(tempData.index > forTimeRange - 0.5) & (tempData.index <= forTimeRange + 10)]

        for name in neuronData.columns:

            if (name == 'somaA(mV)') & ('VCLAMP' in identifier):
                maxIdx = get_maxIdxBlock(tempData[name], maxPosition = maxPosition)
                maxIdxFraction = maxIdx
            else:
                maxIdx = tempData[name].idxmax()
                maxIdxFraction = findFractionMaxium(tempData[name], fractionOfMax = fractionOfMax)

            if (maxIdx > 3.0) & (maxIdx != forTimeRange + 10):
                #print tempData[name].max()
                maxListDF.loc[name,'Maximum (mV)', resistanceValue] = tempData[name].max() + vRest #70.1476 #75.0137 , 69.8741 #resting potential
                maxListDF.loc[name,'Time@Max (ms)', resistanceValue] = maxIdx
                maxListDF.loc[name,'Time@FractionOfMax (ms)', resistanceValue] = maxIdxFraction
                maxListDF.loc[name,'Relative time (ms)',:] = maxListDF.loc[name, 'Time@Max (ms)', :] -  maxListDF.loc['somaA(mV)', 'Time@Max (ms)', :]
                maxListDF.loc[name,'Delay fraction (ms)',:] = maxListDF.loc[name, 'Time@FractionOfMax (ms)', :] -  maxListDF.loc['somaA(mV)', 'Time@Max (ms)', :]

    return neuronData, maxListDF

#%%
#import numpy as np


#dendrites = '0.1'#'Active'

#['proxisoma']#

def figGen(dendrites, locationList = ['distal', 'mid', 'proxi', 'proxisoma', '20muSoma', 'SomaSoma'],  potentialPlot = False, delayPlot = False,
           delayPlotComparison = False,  amplitudePlot = False, couplingPotentialShape = False, maxDelay = 20, potentialPlotScaled = False,
           potentialPlotScaledAll = False, identifier = '', closeFig = True, fractionOfMax = 0.95,
           delayPlotComparisonFraction = False, delayPlotFraction = False, maxPosition = 'right'):

    maxListDF = [0]*len(locationList)


    for i, nameLocation in enumerate(locationList):#'distal', 'mid', 'proxi']):


        neuronData, maxListDF[i] = analyzeNeuronData(dendrites, identifier, nameLocation, fractionOfMax, maxPosition = maxPosition)
        resistanceValues = neuronData['gapR(pS)'].unique()

        if potentialPlot == True:
            for resistanceValue in resistanceValues:

                fig = plt.figure()
                ax = fig.add_subplot(111)
                colorsForPlot = tableau20[:8:2]
                ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])
                tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]

                ax.set_title('Membrane pot ' + dendrites + ' den. and GJ @' + nameLocation + '\n conductance ' + str(resistanceValue) + ' (pS)')
                ax.set_ylabel('Membrane potential (mV)')

                tempData.iloc[:,1:].plot(ax = ax, style = ['--']*4 + ['-']*4)
                figName = 'vm' + dendrites + nameLocation + str(resistanceValue) + identifier
                ax.set_xlim([4.8, 5.2])
                gt.saveFig(fig, figName)
                gt.saveFig(fig, figName, fileFormat = '.png')

                plt.show()
                #plt.close(fig)

        if potentialPlotScaled == True:
            for k, resistanceValue in enumerate(resistanceValues):

                fig = plt.figure()
                ax = fig.add_subplot(111)
                tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]

                ax.set_color_cycle(get_colorscheme(3))
                ax.set_title('Membrane pot ' + dendrites + ' den. and GJ @' + nameLocation + '\n conductance ' + str(resistanceValue) + ' (pS)')
                ax.set_ylabel('Membrane potential (mV)')
                #shift = tempData.iloc[:,[1,-1]] - tempData.iloc[:,[1,-1]].min()
                shift = tempData.iloc[:,1::] - tempData.iloc[:,1::].min()
                shiftNormal = shift/shift.max()
                ax.fill_between(shiftNormal.index, 0, shiftNormal.iloc[:,0], facecolor = get_colorscheme(3, mirror=False)[1] )
                shiftNormal.iloc[:,1::].plot(ax = ax, style = '-')
                ax.set_ylim([0, 1.2])
                ylims = ax.get_ylim()
                ax.plot([maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue], maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue]], ylims, ':', c = 'red', scalex = False, scaley = False)
                ax.legend()
                #ax.set_ylim([0, 1.2])
                #ax.set_xlim([10, 30])
                figName = 'ScaledPotential' + dendrites + nameLocation + str(resistanceValue) + identifier

                figName = 'potentialPlotScaled/' + figName
                gt.saveFig(fig, figName)
                gt.saveFig(fig, figName, fileFormat = '.png')
                plt.show()
                #plt.close(fig)

        if potentialPlotScaledAll == True:
            for k, resistanceValue in enumerate(resistanceValues):

                fig = plt.figure()
                ax = fig.add_subplot(111)
                tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]
                ax.set_color_cycle(get_colorscheme(len(tempData.iloc[:,1::])))

                ax.set_title('Membrane pot ' + dendrites + ' den. and GJ @' + nameLocation + '\n conductance ' + str(resistanceValue) + ' (pS)')
                ax.set_ylabel('Membrane potential (mV)')
                #shift = tempData.iloc[:,[1,-1]] - tempData.iloc[:,[1,-1]].min()
                shift = tempData.iloc[:,1::] - tempData.iloc[:,1::].min()
                shiftNormal = shift/shift.max()
                ax.fill_between(shiftNormal.index, 0, shiftNormal.iloc[:,0], facecolor = get_colorscheme(3, mirror=False)[1] )
                shiftNormal.iloc[:,1::].plot(ax = ax, style = '-')
                ax.set_ylim([0, 1.2])
                ylims = ax.get_ylim()
                ax.plot([maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue], maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue]], ylims, ':', c = 'red', scalex = False, scaley = False)
                legend = plt.legend(frameon = 1, loc = 'upper left')
                frame = legend.get_frame()
                frame.set_facecolor('white')


                #ax.set_ylim([0, 1.2])
                ax.set_xlim([4, 6])
                figName = 'ScaledPotential' + dendrites + nameLocation + str(resistanceValue) + identifier

                figName = 'potentialPlotScaledAllZoom/' + figName
                gt.saveFig(fig, figName)
                gt.saveFig(fig, figName, fileFormat = '.png')
                plt.show()
                #plt.close(fig)




        if couplingPotentialShape == True:

            for l, xRange in enumerate(['', 'zoom in']):

                fig = plt.figure(figsize = (8,5))
                ax = fig.add_subplot(111)
                colorsForPlot = tableau20[:8:2]
                ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])


                if l == 1:

                    for k, resistanceValue in enumerate(resistanceValues):
                        tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]
                        forTimeRange = tempData['somaA(mV)'].idxmax()
                        tempData = tempData[(tempData.index > forTimeRange - 10) & (tempData.index <= forTimeRange + 10)]
                        tempData = tempData.add_prefix(str(resistanceValue) + ' ps ')
                        ax.set_ylabel('Membrane potential (mV)')
                        tempData.iloc[:,-1].plot(ax = ax, style = '-')

                    colorsForPlot = get_colorscheme(len(resistanceValues), mirror = False)


                    for k, resistanceValue in enumerate(resistanceValues):

                        ylims = ax.get_ylim()
                        ax.plot([maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue], maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', resistanceValue]], ylims, '--', c = colorsForPlot[k], scalex = False, scaley = False)


                    ax.set_xlim([maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', :].min() - 1, maxListDF[i].loc['somaA(mV)', 'Time@Max (ms)', :].max() + 1])

                    ax.set_title('SomaBpotential ' + dendrites + ' den. and GJ @' + nameLocation + '\n' + xRange )
                    figName = 'SomaBpotential' + dendrites + nameLocation + xRange + identifier
                    ax.legend(loc = 'upper left')

                    figName = 'couplingPotentialShape/' + figName
                    gt.saveFig(fig, figName)
                    gt.saveFig(fig, figName, fileFormat = '.png')
                    plt.show()
                    plt.close(fig)

                else:
                    for k, resistanceValue in enumerate(resistanceValues):
                        tempData = neuronData[neuronData['gapR(pS)'] == resistanceValue]
                        tempData = tempData.add_prefix(str(resistanceValue) + ' ps ')
                        ax.set_ylabel('Membrane potential (mV)')
                        tempData.iloc[:,-1].plot(ax = ax, style = '-')

                    ax.set_title('SomaBpotential ' + dendrites + ' den. and GJ @' + nameLocation + '\n')
                    figName = 'SomaBpotential' + dendrites + nameLocation + identifier
                    ax.legend()
                    figName = 'couplingPotentialShape/' + figName
                    gt.saveFig(fig, figName)
                    gt.saveFig(fig, figName, fileFormat = '.png')
                    plt.show()
                    plt.close(fig)





    if delayPlot == True:

        for i, nameLocation in enumerate(locationList):

            fig = plt.figure()
            ax = fig.add_subplot(111)
            colorsForPlot = tableau20[:8:2]
            ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])

            #yMax = max([maxListDF[k].iloc[1:,2,:].max().max() for k in range(len(maxListDF))])
            yMax = maxListDF[i].iloc[1:,2,:].max().max()

            maxListDF[i].iloc[1:,2,:].T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)

            ax.set_title('Propagation of maximum /w % act. den.' + dendrites + ' GJ @ ' + nameLocation )
            ax.set_ylabel('Time delay (ms)')
            #barlist[0].set_color('r')
            figName = 'delayOfMaximum' + dendrites + nameLocation + identifier
            ax.set_ylim([0, yMax])
            figName = 'delayPlot/' + figName
            gt.saveFig(fig, figName)
            gt.saveFig(fig, figName, fileFormat = '.png')
            plt.show()
            if closeFig == True: plt.close(fig)

    if delayPlotFraction == True:

        for i, nameLocation in enumerate(locationList):

            fig = plt.figure()
            ax = fig.add_subplot(111)
            colorsForPlot = tableau20[:8:2]
            ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])

            #yMax = max([maxListDF[k].iloc[1:,2,:].max().max() for k in range(len(maxListDF))])
            yMax = maxListDF[i].iloc[1:,4,:].max().max()

            maxListDF[i].iloc[1:,4,:].T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)

            ax.set_title('Propagation of maximum /w % act. den.' + dendrites + ' GJ @ ' + nameLocation )
            ax.set_ylabel('Time delay (ms)')
            #barlist[0].set_color('r')
            figName = 'delayOfMaximum' + dendrites + nameLocation + identifier
            ax.set_ylim([0, yMax])
            figName = 'delayPlotFraction/' + figName
            gt.saveFig(fig, figName)
            gt.saveFig(fig, figName, fileFormat = '.png')
            plt.show()
            if closeFig == True: plt.close(fig)

    if delayPlotComparison == True:

        fig = plt.figure()
        ax = fig.add_subplot(111)
        colorsForPlot = tableau20[:8:2]
        ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])


        ax.set_title('Propagation of maximum /w % act. den. ' + dendrites + ' comparison' + identifier)
        ax.set_ylabel('time delay (ms)')
            #barlist[0].set_color('r')
        figName = 'delayOfMaximumComparison' + dendrites + identifier

        for i, nameLocation in enumerate(locationList):

            if i==0:
                maxListDFTemp = maxListDF[i].iloc[-1,2,:]
                maxListDFTemp.add_prefix(nameLocation + ' ps ')
                maxListDFTemp = maxListDFTemp.to_frame()
            else:
                maxListDFTemp[nameLocation] = maxListDF[i].iloc[-1,2,:]


        maxListDFTemp.columns = locationList
        maxListDFTemp
        maxListDFTemp.T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)
        ax.set_ylim([0, 6])
        figName = 'delayPlotComparison/' + figName
        gt.saveFig(fig, figName)
        gt.saveFig(fig, figName, fileFormat = '.png')
        ax.set_ylim([0, 2])
        gt.saveFig(fig, figName + 'ZOOM')
        gt.saveFig(fig, figName + 'ZOOM', fileFormat = '.png')
        plt.show()
        if closeFig == True: plt.close(fig)

    if delayPlotComparisonFraction == True:

        fig = plt.figure()
        ax = fig.add_subplot(111)
        colorsForPlot = tableau20[:8:2]
        ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])


        ax.set_title('Propagation of fraction ' + str(fractionOfMax) + 'maximum /w % act. den. ' + dendrites + ' comparison' + identifier)
        ax.set_ylabel('time delay (ms)')
            #barlist[0].set_color('r')
        figName = 'delayOfMaximumComparisonFraction' + dendrites + identifier

        for i, nameLocation in enumerate(locationList):

            if i==0:
                maxListDFTemp = maxListDF[i].iloc[-1,4,:]
                maxListDFTemp.add_prefix(nameLocation + ' ps ')
                maxListDFTemp = maxListDFTemp.to_frame()
            else:
                maxListDFTemp[nameLocation] = maxListDF[i].iloc[-1,4,:]


        maxListDFTemp.columns = locationList
        maxListDFTemp
        maxListDFTemp.T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)
        ax.set_ylim([0, 5])
        figName = 'delayPlotComparisonFraction/' + figName
        gt.saveFig(fig, figName)
        gt.saveFig(fig, figName, fileFormat = '.png')
#        ax.set_ylim([0, 2])
#        gt.saveFig(fig, figName + 'ZOOM')
#        gt.saveFig(fig, figName + 'ZOOM', fileFormat = '.png')
        plt.show()
        if closeFig == True: plt.close(fig)




            #barlist[0].set_color('r')

            #ax.set_ylim([0,10])



    if amplitudePlot == True:
        for i, nameLocation in enumerate(locationList):

            fig = plt.figure()
            ax = fig.add_subplot(111)
            colorsForPlot = tableau20[:8:2]
            ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])

            maxListDF[i].iloc[1:,0,:].T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)
            ax.set_title('Amplitude of maximum /w % act. den.' + dendrites + ' GJ @ ' + nameLocation )
            ax.set_ylabel('Amplitude (mV)')
            #barlist[0].set_color('r')
            figName = 'Amplitudeofmaximum ' + dendrites + nameLocation + identifier
            #ax.set_ylim([0,10])
            figName = 'amplitudePlot/' + figName
            gt.saveFig(fig, figName)
            gt.saveFig(fig, figName, fileFormat = '.png')
            plt.show()
            plt.close(fig)

    if amplitudePlot == True:

        fig = plt.figure()
        ax = fig.add_subplot(111)
        colorsForPlot = tableau20[:8:2]
        ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])
        ax.set_title('Amplitude of maximum /w % act. den.' + dendrites )
        ax.set_ylabel('Amplitude (mV)')

        for i, nameLocation in enumerate(locationList):

            if i==0:
                maxListDFTemp = maxListDF[i].iloc[-1,0,:]
                maxListDFTemp.add_prefix(nameLocation + ' ps ')
                maxListDFTemp = maxListDFTemp.to_frame()
            else:
                maxListDFTemp[nameLocation] = maxListDF[i].iloc[-1,0,:]

            #barlist[0].set_color('r')

            #ax.set_ylim([0,10])
        maxListDFTemp.columns = locationList
        maxListDFTemp
        maxListDFTemp.T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)

        figName = 'AmplitudeofmaximumSoma ' + dendrites + identifier
        figName = 'amplitudePlot/' + figName
        gt.saveFig(fig, figName)
        gt.saveFig(fig, figName, fileFormat = '.png')
        plt.show()
        plt.close(fig)
    #print maxListDF

    return maxListDF, neuronData
#Very high delays to soma...I guess the reason for this is the passive dendrites
#We have to consider active dendrites in order to get reasonable delays for the
#coupling potentials.

#Next...add active dendrites.

#%%

if __name__ == '__main__':

    for i in range(3):
        maxListDF[i].T['Relative time (ms)'].plot()
    #%%
    for i in range(3):
        plotlist[i].set_xlim([0,20])

        plotlist[i].get_figure()
        fig



    #plotlist[2].set_xlim([0,20])
    #plotlist[2].get_figure()

    #%%
    neuronData[neuronData['gapR(pS)'] == 30.030030]
    neuronData

    neuronData['gapR(pS)'].unique()

    for i in range(1,9):
    #%%
        print maxListDF[0].iloc[i]
    for i in range(1,9):
        print maxListDF[1].iloc[i]
    for i in range(1,9):
        print maxListDF[2].iloc[i]
    #%%
    whatToPlot = 2
    locationList = ['proxi', 'proxisoma', '20muSoma', 'SomaSoma']#['proxi', 'proxisoma', '20muSoma', 'SomaSoma']#['distal', 'mid', 'proxi', 'proxisoma', '20muSoma', 'SomaSoma']
    chanConfig= ['333','0','0.1','1']#['333','0','0.1','1']#['333','1']#


    for i, dend in enumerate(chanConfig):

       zack, bang = figGen(dend, locationList = locationList, potentialPlot = False, delayPlot = False, delayPlotComparison = False,  amplitudePlot = False, couplingPotentialShape = False, identifier = 'Detail')
       if i == 0:
           parameterCompare = pd.Panel(items = locationList, major_axis = chanConfig, minor_axis = zack[0].minor_axis)
       for k, location in enumerate(locationList):
            parameterCompare.iloc[k,i,:] = zack[k].iloc[-1,whatToPlot]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    colorsForPlot = tableau20[:16:2]
    ax.set_color_cycle(colorsForPlot[:len(chanConfig)])
    styleList = ['+','x',',','v']
    styleList = ['-+','--x','-.,',':.','-o','--o','-.o',':o']

    for j, gapPosition in enumerate(parameterCompare.items):
        if j < len(parameterCompare.items):
            parameterCompare[gapPosition].T.plot(style = styleList[j], ax = ax, legend = None)
            #ax.legend()
        else:
            parameterCompare[gapPosition].T.plot(style = styleList[j], ax = ax, legend = None)

    handles, labels = ax.get_legend_handles_labels()



    ax.set_xlabel('Conductance (pS)')
    if whatToPlot == 0:
        ax.set_ylabel('Max. amplitude (mV)')
        ax.set_title('Passive conductance vs. max. of amplitude')
        figName = 'RatioActivePassiveDetail'
        ax.set_ylim([0,4])
    elif whatToPlot == 2:
        ax.set_ylabel('Time delay (ms)')
        ax.set_title('Passive conductance vs. Time delay')
        figName = 'TimeDelayVsPassiveDetail'
        ax.set_ylim([0,3])
    ax.set_xlim([0,1100])
    leg1 = ax.legend(handles[:len(chanConfig)], labels[:len(chanConfig)], loc ='upper center')
    ax.legend(handles[::len(chanConfig)],locationList, loc = 'upper left')

    plt.gca().add_artist(leg1)

    #figName = 'RatioActivePassiveDetailReal'
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')
    #%%
    locationList = ['distal', 'mid', 'proxi', 'proxisoma', '20muSoma', 'SomaSoma']# ['proxi']#[, 'proxisoma']#]#['proxisoma', 'proxisoma', '20muSoma', 'SomaSoma']#
    chanConfig= ['0']#,'0','0.1','1']


    for i, dend in enumerate(chanConfig):


       maxListDF, neuronData = figGen(dend, locationList = locationList,
                                      potentialPlot = 0,
                                      delayPlot = 0,
                                      delayPlotComparison = 0,
                                      delayPlotComparisonFraction = 0,
                                      amplitudePlot = 0,
                                      couplingPotentialShape = 0,
                                      potentialPlotScaled = 0,
                                      potentialPlotScaledAll = 1,
                                      identifier = 'VCLAMP',
                                      closeFig = False,
                                      delayPlotFraction = 0,
                                      fractionOfMax = 0.95)

    #%%
    locationList = ['distal', 'mid', 'proxi', 'proxisoma', '20muSoma', 'SomaSoma']# ['proxi']#[, 'proxisoma']#]#['proxisoma', 'proxisoma', '20muSoma', 'SomaSoma']#
    chanConfig= ['0']#,'0','0.1','1']
    dendrites = '0'

    for nameLocation in enumerate(locationList):

       maxListDF, neuronData =  analyzeNeuronData(dendrites, identifier, nameLocation, fractionOfMax)
    #%%
    test = bang[bang['gapR(pS)'] == 1000.0]
    test.iloc[:,[1]].plot(style = 'o')
    print test['somaA(mV)'].idxmax()
    print test['somaA(mV)'].max()
    plt.xlim([4.9,5.5])

    #%%
    chanConfig= ['0','333','0.1','1']


    for i, dend in enumerate(chanConfig):
        locationList = ['20muSoma']
        zack, bang = figGen('333', locationList = locationList, potentialPlot = False, delayPlot = False, delayPlotComparison = False,  amplitudePlot = False, couplingPotentialShape = False,  potentialPlotScaled = True, identifier = '')
        test = bang[bang['gapR(pS)'] == 1000.0]

    #%%
    #    if delayPlotComparison == True:
    #
    fig = plt.figure()
    ax = fig.add_subplot(111)
    colorsForPlot = tableau20[:8:2]
    ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])


    ax.set_title('Propagation of 90% maximum w/ passive dendrites') #+ dendrites + ' comparison' + identifier)
    ax.set_ylabel('time delay (ms)')
    #barlist[0].set_color('r')
    figName = 'delayOfMaximumMeasureComparison90%'

    for i, nameLocation in enumerate(locationList):

        if i==0:
            maxListDFTemp = maxListDF[i].iloc[-1,[2,4],1]
            maxListDFTemp.add_prefix(nameLocation + ' ps ')
            maxListDFTemp = maxListDFTemp.to_frame()
        else:
            maxListDFTemp[nameLocation] = maxListDF[i].iloc[-1,[2,4],1]


    maxListDFTemp.columns = locationList
    maxListDFTemp.T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)
    #ax.set_ylim([0, 6])
    figName = 'delayPlotMeasureComparison/' + figName
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')
    #ax.set_ylim([0, 2])
    #gt.saveFig(fig, figName + 'ZOOM')
    #gt.saveFig(fig, figName + 'ZOOM', fileFormat = '.png')
    #plt.show()
    #if closeFig == True: plt.close(fig)

    #%%
    maxListDFTemp0 = maxListDFTemp.T.copy()
    #%%

    maxListDFTempShort = maxListDFTemp.T.copy()
    maxListDFTempLong = maxListDFTemp.T.copy()
    #%%
    fig = plt.figure()
    ax = fig.add_subplot(111)
    colorsForPlot = tableau20[:8:2]
    ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])
    longData = maxListDFTempLong.add_prefix('long')
    shortData = maxListDFTempShort.add_prefix('short')

    results = longData.T.append(shortData.T)

    results.iloc[[1,3],:].T.plot(kind = 'bar', ax =ax, figsize = (6,3), color = colorsForPlot)

    ax.set_ylim([0,5])
    figName = 'comparisonLongVShortPulse95Percent'
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')

    #%% Generate figure for measuring the FWHM of the spike peak

    fig = plt.figure()
    ax = fig.add_subplot(111)
    colorsForPlot = tableau20[:8:2]
    ax.set_color_cycle(colorsForPlot + colorsForPlot[::-1])

    tempData = neuronData[neuronData['gapR(pS)'] == 1000.0]

    ax.set_title('FWHM spike soma')
    ax.set_ylabel('Membrane potential (mV)')
    ax.fill_between(tempData.index, -80, tempData.iloc[:,1], facecolor = get_colorscheme(3, mirror=False)[1] )

    neuronDataPre = tempData.iloc[:,1]

    #ax.set_xlim([0,20])
    amplitude = neuronData.iloc[:,1].max()-neuronData.iloc[:,1].min()

    HM = (amplitude/2.0-neuronData.iloc[:,1].max(),amplitude/2.0+neuronData.iloc[:,1].max())

    HM = neuronData.iloc[:,1].max()-amplitude/2.0

    leftSideFW = (neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM).idxmin()

    neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()]
    leftSideFW = abs((neuronDataPre[neuronDataPre.index < neuronDataPre.idxmax()] - HM)).idxmin()
    rightSideFW = abs((neuronDataPre[neuronDataPre.index > neuronDataPre.idxmax()] - HM)).idxmin()

    xx = [leftSideFW, rightSideFW]
    yy = [neuronDataPre[leftSideFW], neuronDataPre[rightSideFW]]
    ax.plot(xx,yy, 'k', lw = 3.0)

    FWHM = rightSideFW - leftSideFW
    ax.annotate('FWHM = '+ str(FWHM) + 'ms', (xx[1], yy[1] + 5))
    figName = 'FWHMspikeSaragaConfigurationHOT'
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')
    #
    #plt.show()

    #%%
    locationList = ['distal', 'mid', 'proxi', 'proxisoma', '20muSoma', 'SomaSoma']# ['proxi']#[, 'proxisoma']#]#['proxisoma', 'proxisoma', '20muSoma', 'SomaSoma']#
    chanConfig= ['0']#,'0','0.1','1']

    idList = ['', 'VCLAMP_FWHM','VCLAMP0.4', 'VCLAMP0.1', 'VCLAMP']#, 'VCLAMP']
    for k, identifier in enumerate(idList):

        measureToDisplay = np.arange(0,2*len(idList),2)

        for i, dend in enumerate(chanConfig):

            # get data
            maxListDF, neuronData = figGen(dend, locationList = locationList,
                                          potentialPlot = 0,
                                          delayPlot = 0,
                                          delayPlotComparison = 0,
                                          delayPlotComparisonFraction = 0,
                                          amplitudePlot = 0,
                                          couplingPotentialShape = 0,
                                          potentialPlotScaled = 0,
                                          potentialPlotScaledAll = 0,
                                          identifier = identifier,
                                          closeFig = True,
                                          delayPlotFraction = 0,
                                          fractionOfMax = 0.95,
                                          maxPosition = 'right')

        # harvest data from full dataset
            for i, nameLocation in enumerate(locationList):

                if i==0:
                    maxListDFTemp = maxListDF[i].iloc[-1,[2,4],1]
                    maxListDFTemp.add_prefix(nameLocation + ' ps ')
                    maxListDFTemp = maxListDFTemp.to_frame()
                else:
                    maxListDFTemp[nameLocation] = maxListDF[i].iloc[-1,[2,4],1]
            maxListDFTemp.index = identifier + ' ' + maxListDFTemp.index
            maxListDFTemp.columns = locationList
            if k == 0: results = maxListDFTemp.copy()
            else:
                results = pd.concat([results, maxListDFTemp])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    colorsForPlot = get_colorscheme(len(idList), mirror = False)
    ax.set_color_cycle(colorsForPlot)

    results.index = ['Saraga Max', 'Saraga 95% Max',
                       'Block 1.7ms Max', 'Block 1.7ms 95% Max',
                       'Block 0.4ms Max', 'Block 0.4ms 95% Max',
                       'Block 0.1ms Max', 'Block 0.1ms 95% Max',
                       'Block 0.01ms Max', 'Block 0.01ms 95% Max']

    results.iloc[measureToDisplay,:].T.plot(kind = 'bar', ax =ax, figsize = (12,6), color = colorsForPlot)
    figName = 'realSpikeVsBlockDelayAllMaxRight'
    #figName = 'realSpikeVsBlockDelayAllFractionMax'
    ax.set_title('Delay of postsynaptic potential')
    ax.set_ylabel('Delay (ms)')

    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')

    figName = figName + 'CLOSEUP'

    ax.set_ylim([0,1.5])
    gt.saveFig(fig, figName)
    gt.saveFig(fig, figName, fileFormat = '.png')


